\smallsection{COMMA Evaluation}\label{sec:evaluation}
We conducted several experiments to evaluate the approach proposed in the paper.
In order to investigate the \textit{effectiveness} of COMMA we evaluated the
accuracy of the ranks returned by the system comparing COMMA with a baseline,
namely a syntactic approach based on the well-known TF-IDF weighting
scheme\cite{SaltonTfIdf88}, and analyzing the effect of several parameters on
the results. In order to evaluate the \textit{efficiency} of COMMA, we
investigate whether the AS satisfies the strict time constraints required by the
domain, and can scale up to handle a significant amount of data. Finally, we
conducted experiments in a real world scenario, deploying COMMA on an Italian
e-marketplace and measuring the impact on the \textit{conversion rate}, i.e.,
the ratio of users that complete a purchase over the users that visit the
e-marketplace.

\begin{plot}
\includegraphics[width=1\columnwidth]{images/architecture.pdf}
	\caption{COMMA evaluation deployment scenario.} \label{fig:architecture}
\end{plot}

COMMA has been implemented using the Java programming language exploiting
several functions of the Lucene search engine
library\footnote{\url{http://lucene.apache.org/}}; further
details on COMMA's implementation are out of the scope of this paper. We only clarify that, as
shown in Figure~\ref{fig:architecture}, the system accepts the AJAX calls from
the client web page and supplies results of the matching operation on offers,
exploiting category and facet information, by means of JSON format. This
deployment allowed for the provisioning of the autocompletion functionality as a
remote service instead of requiring a deployment on the e-marketplace
server. It must be noted that due to UI constrains, the list of results of the
autocompletion operation is necessarily small: the size of this list was set to
$7$, a good trade-off between compactness and coverage of the suggested results\cite{Saaty03}.

% Moreover, it is important to notice that the top $7$ offers
% according to the COMMA global score are not ordered according to the score, but
% they are rather grouped according to their category to reduce the cognitive load
% of the user when interpreting the results of the autocompletion
% query~\cite{GROUPING-STRATEGIES}.
% Although we will evaluate our approach considering the final rank returned by
% the algorithm defined in Section \ref{sec:algorithm}, we will show that the
% categorized presentation of the autocompletion results does not significantly
% affect the \as effectiveness.

The experiments where run in May 2012 on an Intel Core2 2.54GHz processor
laptop, 4GB RAM, under the Xubuntu Linux Desktop 12.04 32bit operating system.
For the real-world experiments only, COMMA has been deployed to an Ubuntu Linux
Server 10.04 LTS 64bit, 2GB RAM, 2GB storage, virtualized machine.

\smallsubsection{Effectiveness}
In order to evaluate the effectiveness of our approach we evaluate the accuracy
of the results ranked by COMMA with respect to an ideal rank, adopting the
Normalized Discounted Cumulative Gain (nDCG) measure~\cite{DCG}. Discounted
Cumulative Gain (DCG)~\cite{DCG-EARLY} is a well-known measure of effectiveness
for ranking algorithms adopted in Information Retrieval.
% the effectiveness of ranked results in terms of cumulative gain of documents
% returned as results to a query considering their position in the list of
% results, discounting the contribution of each document to the overall score as
% rank increases.
The rationale of the measurement is that highly relevant documents should appear
in the top positions of the list of results and, more generally, that the list
should be ordered according to the ideal relevance of the documents to a query.
The nDCG measure is obtained by normalizing the DCG measure according to the
ideal DCG measure of the result list.

Adopting the notation used in the paper (documents are represented by offers),
DCG can be defined as follows. Given a ranked set of offers $O$, and an ideal
ordering of the same set $I$, the DCG at a particular rank $r$ is defined as

\begin{formula}
DCG(O, r) = \sum\limits_{i=1}^r \cfrac{2^{r(i)} - 1}{log(1+i)},
\end{formula}

where $r(i)$ is the assessed graded relevance of the offer $i$ (adopting the scale 
0=Bad, 1=Fair, 2=Good, 3=Excellent relevance). Consequently, the nDCG of $O$ at
a particular rank $r$ is defined as
\begin{formula}
nDCG(O, r) = \cfrac{DCG(O, r)}{DCG(I, r)}
\end{formula}
Intuitively, a higher nDCG value corresponds to a better agreement between the list 
proposed by the system and the human judgements (the ideal ordering).

\subsubsection{Experimental Setup}
To the best of our knowledge there is no available benchmark and dataset defined
to evaluate a result-oriented \as, and in particular considering the types of
queries addressed in this paper. Therefore, we used a dataset from a real
e-marketplace, which contains 5594 offers belonging to 92 different categories
and annotated with 463 different facets.
The ideal ranks for 30 keyword-based queries (13 \taq, 13 \exq and 4 \hyq
queries) have been defined by collecting graded relevance judgements from 10
average users of e-marketplaces and aggregating the individual judgements into
mean graded relevance scores. All the queries have been manually constructed,
considering frequent types of queries made by Italian users of the
e-marketplace. For example, the query \quoted{nakon coolpix} (notice the
misspelling) is classified as \taq, the query \quoted{nokia wifi} is classified
as \exq, and the query \quoted{lcd samsung} is classified as \hyq because the
all the keywords can refer both to the name and the facets of an offer. For each
query the user was asked to fill in a questionnaire assigning a graded relevance
judgement (adopting the previously introduced scale) to a set of distinct 20
offers from the dataset. This set contained 15 offers selected by a pool of
experts and was enriched with the top 5 ranked offers according to the TF-IDF
weighting scheme\footnote{An archive containing the dataset used for our
experiments, including the questionnaire and the user judgements can be
downloaded from
\url{http://www.lintar.disco.unimib.it/COMMA2012/query_set_extended.zip}}.
 
%Every other offer from the dataset was considered to be irrelevant and thus the
%value 0 was assigned to it. 
%COMMA results to the above queries were
%then retrieved in different configurations of the matching algorithm; the
%evaluation of these tests will now be given.

\subsubsection{Experiments}

\begin{plot}
	\begin{subfigure}[b]{\columnwidth}
		\includegraphics[width=.9\columnwidth]{plots/ndcg-overall.pdf}
		\caption{All query types.}
		\label{fig:ndcg-overall}
	\end{subfigure}
	\begin{subfigure}[b]{\columnwidth}
		\includegraphics[width=.9\columnwidth]{plots/explorative-queries-all.pdf}
		\caption{Only \exq queries.}
		\label{fig:explorative-queries-all}
	\end{subfigure}
	\caption{Normalized DCG for the 3 different configurations of COMMA compared to
	the baseline at different rank thresholds.}
\end{plot}

In the first experiment we consider as a baseline the naive solution to the
problem of responding to both \exq and \taq queries: titles, categories and
facets are indexed and the offers that have at least on term in common with the
query (considering also mispellings) are selected and ranked according to the
TF-IDF weighting scheme\footnote{We also run experiments using a BM25 weighting
scheme~\cite{Bm25Jones00}, which achieved worse performance than TF-IDF reported
in the paper.}. The baseline is compared to three different configurations of
COMMA: simple scoring, that is, without the methods for ranking improvement
(i.e., the \textit{intersection boost} and \textit{facet salience}), with
\textit{intersection boost} and without \textit{facet salience} (COMMA-B), and
with both \textit{intersection boost} and \textit{facet salience} (COMMA-BFS).
Figure~\ref{fig:ndcg-overall} shows that all the configurations of COMMA that
include at least one method for ranking improvement perform better than the
baseline, at all the considered rank thresholds, while the TF-IDF-based approach
outperforms the basic COMMA algorithm.

Using both \textit{intersection boost} and \textit{facet salience} in the global
ranking function leads to significant improvements, especially if we consider
the results for \exq queries, plotted in
Figure~\ref{fig:explorative-queries-all}. The accuracy in ranking results in the
top positions, which is noticeable for any type of query, is even more
remarkable for \exq queries; this behavior is particularly beneficial to an \as,
which presents a limited number of results to the user. Rewarding the offers
selected by more than one filters (\textit{Intersection boost}) and particularly
relevant because of the salience of their facets to the query (\textit{facet
salience}) enables COMMA to be enough selective to perform better for higher
rank thresholds.
% This indicates that the main difficulty in responding ambiguous queries is to
% decide the offers that are not the most relevant but still relevant to the
% query.

\begin{plot}
	\begin{subfigure}[b]{\columnwidth}
		\includegraphics[width=.9\columnwidth]{plots/ndcg-boost-varying.pdf}
		\caption{$w_{boost}$ varying.}
		\label{fig:ndcg-boost-varying}
	\end{subfigure}
	\begin{subfigure}[b]{\columnwidth}
		\includegraphics[width=.9\columnwidth]{plots/ndcg-localscores-varying.pdf}
		\caption{Local scores for varying weights.}
		\label{fig:ndcg-localscores-varying}
	\end{subfigure}
	\caption{Normalized DCG for different configurations of
	COMMA with different weights at fixed rank values.}
\end{plot} 

In the second experiment, the effect of the weights $w_{boost}$, $w_{pop}$,
$w_{syn}$ and $w_{sem}$ on the overall effectiveness of COMMA is evaluated.
Figure~\ref{fig:ndcg-boost-varying} presents the nDCG values obtained at fixed
ranks (1, 10 and 20 respectively) for different configurations of COMMA, each
one with a different assignment to $w_{boost}$ (ranging from $0.1$ to $1$), and
with an uniform distribution of the other weights  according to the formula
$w_{pop} = w_{syn} = w_{sem} = \frac{1 - w_{boost}}{3}$. This experiment shows
that the best results are obtained when $w_{boost}$ is assigned values between
$0.6$ and $0.9$. In other words, the higher the weight of the
\textit{intersection boost} factor is, the better COMMA ranks according to human
judgement, with the exception of $w_{boost}=1$. Moreover, the best performing
configuration of COMMA is the one that emphasizes the syntactic score, as
depicted in the Figure~\ref{fig:ndcg-localscores-varying}.
Although several criteria have to be introduced to refine the overall ranking
scores (especially for offers selected by semantic filters, which extend the
functionality of the \as) the syntactic score still represents the most
important ranking factor.
% \begin{figure}[t] \vspace{-2pt}
% \includegraphics[width=.9\columnwidth]{plots/ndcg-localscores-varying.pdf}
% \caption{Normalized DCG for different 4 configurations of COMMA at different
% rank thresholds.} \label{fig:ndcg-localscores-varying} \vspace{-2pt}
% \end{figure}

%In the third experiment the effects of different combinations of weights
%$w_{pop}$, $w_{SYN}$ and $w_{sem}$ on the efficiency of COMMA are
%studied. The Figure~\ref{fig:ndcg-localscores-varying} depicts the nDCG
%values for 4 different configurations of COMMA at fixed rank thresholds. For
%all the configurations the value of $w_{boost}$ is fixed to 0.6, while the values
%of the remaining weights are combined in order to favor a different local score for
%every different configuration. All those configurations are compared to the one
%with all constant and fixed weights $w_{pop}$, $w_{SYN}$ and $w_{sem}$, that
%is, the one in which all the local scores contribute equally to the rank, a
%configuration that can be considered as a baseline. Both the weight configuration
%that privilege semantic and popularity scores perform similarly or worse than the
%baseline, while the configuration that emphasizes syntactic local score performs
%better. The results of this experiment indicate that syntactic criteria are
%still the most valuable ranking factor, thus demonstrating the value of the
%proposed approach of combining this criteria with semantic ones that instead
%allow extending the set of offers potentially considered thanks to the semantic
%filters.

%\begin{figure}[t]
%\vspace{-2pt}
%\includegraphics[width=.9\columnwidth]{plots/ndcg-pure-vs-category.pdf}
%\caption{Normalized DCG at different rank thresholds for COMMA using
%two different ranking functions.}
%	\label{fig:ndcg-pure-vs-category}
%\vspace{-2pt}
%\end{figure}

% In the last experiment the effects of the aggregated category ranking function
% on the overall COMMA effectiveness are analyzed; more precisely two configurations
% of the ranking functions are compared. The first one employs the function $score$
% while the second adoptes the $cat\_rank$ function. The achieve nDCG curves
% are substantially the same, to the point that we decided not to report the related figures
% for sake of space. The experiment, however, has shown that a categorized display of
% the outcomes of the autocompletion process does not decrease the efficiency of COMMA.
% Usability tests would be necessary to understand if the category ranking is
% actually preferred by users in the real world setting.

\smallsubsection{Efficiency}
% The efficiency of an \as is actually a necessary condition for its
% effectiveness, as discussed in the introduction of the paper. In particular
We remind that the autocompletion operation must take place in a relatively
short time span (100 ms) in order to be perceived as instant by the user.
% Therefore we measured the efficiency of the proposed approach, to show that it
% is viable and scalable.

We assess the efficiency of COMMA \wrt time elapsed for the computation of an
autocompletion operation. For these experiments we use a dataset of 30725
offers, belonging to 206 different categories, annotated with 936 different
facets, and collected from a real e-marketplaces (consider that this is one of
the larger e-marketplace considered by a price comparison engine like
ShoppyDoo). In order to analyze the scalability of the COMMA approach, the
execution times for completing a query are evaluated with sets of offers of
different size.
Test queries for this experiment are taken from a set of 200 queries of variable
length (chosen among the most common queries actually submitted by users of
ShoppyDoo). All the results of the single query computation are merged to a mean
elapsed time measure. Results of this test are shown in
Figure~\ref{fig:efficiency}: COMMA is compared to the approach based on TF-IDF
described in the previous experiment, and to other configurations of COMMA and a
TF-IDF that do not include any approximate string matching algorithm (referred
to as \quoted{not robust} in the plot).

\begin{plot}
	\includegraphics[width=.9\columnwidth]{plots/efficiency.pdf}
	\caption{Average execution times with set of offers of different size.} \label{fig:efficiency}
\end{plot}

The results of this experiment show that, when dealing with a set of offers of
this size, COMMA is able to respect the strict requirements of an \as,
completing the autocompletion operation in less than 25 ms leaving a
reasonable time for network overhead. A second kind of analysis is related to
the impact of the semantic filters and ranking scores adopted by COMMA on the
overall execution times, which can be estimated by considering the difference
between COMMA and the TF-IDF weighting approach. Within the experimented field,
this difference remains quite limited and it does not grow significantly with
the growth of the size of the dataset. Finally, the computational costs of the
approximate string matching algorithms dominate the overall costs of
computation.
% :
% when COMMA was configured not to employ approximate matching the computation
% costs significantly decreased to less than 5 ms
This problem is due to a suboptimal implementation of the approximate string
matching algorithm in the Lucene framework used in the experiments; however,
it has been announced that an improved implementation  will be included in
Lucene soon, the overall performance of COMMA are expected to significantly
improve.

\smallsubsection{Real World Experimentation}
The experiments in real world scenario were run to evaluate COMMA from a
commercial perspective. Two different experiments were run considering two
different configurations of COMMA deployed to an Italian e-marketplace dealing
with consumer electronics and photography related goods. The results reported in
this section are not as extensive as the ones previously described due to
confidentiality agreements with the e-marketplace company.

The goal of the first experiment was to evaluate the impact of the addition of
the autocompletion function to the e-marketplace web site, with particular
reference to the effects on usability of the web site search function. The
autocompletion function is a form of site adaptation and we were concerned that
it could reduce overall site usability. Therefore, we used Google
Analytics\footnote{\url{http://www.google.com/analytics/}} to gather data about
the usage of the autocompletion function, adopting a purely syntactic
configuration of COMMA (we ran the test in the early phases of the work):
results showed that during the test period (50 days) users increasingly employed
the autocompletion function (considering the frequency of activations of the
autocompletion function on the overall number of page views) and they also
growingly selected offers returned by the \as.

In a later phase, we evaluated the impact of the full autocompletion function on
the (previously introduced) conversion rate by means of an A/B
test~\footnote{The A/B test was carried out using Visual Website Optimizer A/B
testing tool. More information can be found at
\url{http://visualwebsiteoptimizer.com/}} during a shorter time period (5 days,
for a total of 7,339 visitors): the introduction of this function led to an
increase of $6.67\%$ of the conversion rate.