\smallsection{The matching algorithm}\label{sec:algorithm}

The COMMA approach consists of three main phases:
\begin{itemize}
\item \textbf{indexing (off-line)}: offers descriptions are indexed along three
different dimensions: their title, category and the facets they are annotated
with;
\item \textbf{filtering (runtime)}: given a query fragment $q$ submitted by the
user, the matching algorithm applies three filters, one for each descriptive
dimension; one filter selects offers whose \emph{titles} match with the query
(syntactic matching); one filter selects offers whose \emph{categories} match
with the query (semantic matching); one filter selects offers whose
\emph{facets} match with the query (semantic matching); the results of the
three filters are combined by means of a set union returning an overall set of
matching offers;
\item \textbf{ranking (runtime)}: several scoring methods are applied to rank
the offers selected by the filters; local scoring methods evaluate the relevance
of each offer to the query, by considering different criteria such as their
popularity, the strength of the syntactic match, the semantic relevance; the
local scores are combined in a final relevance score, which is used to rank the
offers and define the list of top-k relevant offers.
\end{itemize}

Ranking is applied after filtering because most of the scores applied in ranking
reuse the results of matches discovered during filtering; moreover, this
approach allows to limit the number of items to which scoring functions are
applied, achieving a better efficiency.

%\begin{figure}[t]
%	\includegraphics[width=1\columnwidth]{images/overall_process.pdf}
%	\caption{Overview of the proposed approach} \label{fig:two-phases}
%\end{figure}

\smallsubsection{Indexing}

In order to index the titles, categories and facets associated with each offer,
three inverted indexes are built: $I^{T}$, $I^{C}$ and $I^{F}$ respectively
denote the title, category and facet indexes. In the facet index $I^{F}$ only
facet values are indexed under the assumption that the attribute names of facets
are not very significant for searches (e.g. a user is more likely to search for
\quoted{phone Samsung android}, instead of \quoted{phone brand Samsung operating system
Android}).

All the indexes are built by extracting every term occurring in the indexed
string (respectively the name, the category identifier and the facets' values);
we tokenize the strings representing code (e.g. from \quoted{Nokia c5-03} we
extract the three terms \quoted{nokia}, \quoted{c5} and \quoted{03}); terms are
stemmed and stop-words are removed.
% ; these techniques are applied to those indexes because the identifiers of
% categories and facets' values can be represented by natural language
% expressions (e.g. \quoted{printer for photos}) and the matching process should be
% transparent \wrt singular and plural forms
We will call title terms, category terms and facet terms the elements
belonging respectively to the sets $I^{T}$,  $I^{C}$ and $I^{F}$.

\smallsubsection{Syntactic and Semantic Filtering}
Let $O^q_{SYN}$, $O^q_{CAT}$, and $O^q_{FAC}$ be the set of offers whose textual descriptions (titles in our case),
categories and facets respectively match a query $q$. The set $O^q$ of
offers matching a query $q$ can be defined as follows:
\begin{formula}
	O^q = O^q_{SYN} \cup O^q_{CAT} \cup O^q_{FAC}\label{def:total}
\end{formula}

In other words, all the offers that match the input query along at least one
dimension are selected in the filtering phase.
% We call syntactic match when the name is considered, and semantic match when
% the category or the facet are considered.
The next sections explain how each matching set is obtained.

\subsubsection{Syntactic Filter}

The set $O^q_{SYN}$ of the offers that syntactically match against a query
fragment is computed by performing a set union of the results of two filters: a prefix-based string matcher, and an approximate string matcher (asm) based on normalized edit distance (nedit) and a filtering threshold $th^{asm}$ \cite{NavarroStringMatching2002}.

A \textit{title term} $t\in I^{T}$ \textit{pref-matches} an input keyword
fragment $k$, iff $k$ is a prefix of $t$;  a title
term $t\in I^{T}$ \textit{asm-matches} an input keyword fragment $k$,
iff $nedit(k,t)\leq th^{asm}$. An
\textit{offer} $o$ \textit{pref-matches} (asm-matches, respectively) an \textit{input keyword} fragment $k$, iff
there is at least a title term $t\in o$ such that $t$ pref-matches (asm-matches) $k$.
An \textit{offer} $o$ pref-matches (asm-matches, respectively) a \textit{query}
$q=\{k_{1},...,k_{n}\}$ if and only if $o$ pref-matches (asm-matches) 
\textit{every} keyword $k_{i}\in q$, with $1 \leq i\leq n$. The set $O^q_{SYN}$
of offers syntactically matching an input query $q$ is defined as the union of
the set of all the offers pref-matching $q$ and the the set of all the offers
asm-matching $q$.

As an example, suppose to have a query $q$=\quoted{nakia c}, and two offers
named \quoted{Nokia c5-03} and \quoted{Nokia 5250 Blue}; \quoted{Nokia c5-03}
syntactically match $q$ because \quoted{nakia} asm-matches the keyword
\quoted{Nokia} and \quoted{c} asm-matches the keyword fragment \quoted{c}
(instead, observe that this is not a pref-match because
\quoted{nakia} does not pref-match \quoted{Nokia}); instead \quoted{Nokia 5250 Blue}
does not syntactically match  $q$ because there is no term in the offer title
pref-matching or asm-matching the keyword fragment \quoted{c}.

% ------ OLD EXTENDED VERSION
%%The set $O^q_{SYN}$ of the offers that syntactically match against a query
%%fragment is computed by performing a set union of the results of two filters,
%%respectively based on a prefix matching and approximate string matching
%%algorithms.
%%
%%A title term $t\in I^{T}$ has a \textit{pref-match} with an input keyword
%%fragment $k$, iff $k$ is a prefix of $t$ \cite{NavarroStringMatching2002}; an
%%offer $o$ has a \textit{pref-match} with an input keyword fragment $k$, iff
%%there is at least a title term $t\in o$ such that $t$ pref-matches $k$.
%%
%%The approximate string matching method we use is based on the normalized edit
%%distance (nedit) similarity metric~\cite{NavarroStringMatching2002}. A title
%%term $t\in I^{T}$ has an \textit{asm-match} with an input keyword fragment $k$,
%%iff the normalized edit distance between the two strings  $nedit(k,t)\leq
%%th^{asm}$, where $th^{asm}$ is a similarity threshold; an offer $o$ has an
%%\textit{asm-match} with an input keyword fragment $k$, iff there is at least a
%%title term $t\in o$ such that $t$ asm-matches $k$.
%%
%%An offer $o$ pref-matches (asm-matches, respectively) a query
%%$q=\{k_{1},...,k_{n}\}$ if and only if $o$ pref-matches (asm-matches) with
%%\textit{every} keyword $k_{i}\in q$, with $1 \leq i\leq n$. The set $O^q_{SYN}$
%%of offers syntactically matching an input query $q$ is defined as the union of
%%the set of all the offers pref-matching $q$ and the the set of all the offers
%%asm-matching $q$.
%%
%%As an example, suppose to have a query $q$=``nakia c", and two offers named
%%``Nokia c5-03" and ``Nokia 5250 Blue"; ``Nokia c5-03" syntactically match $q$
%%because ``nakia" asm-matches the keyword ``Nokia" and ``c" asm-matches the
%%keyword fragment ``c" (instead, observe that this is not a pref-match because
%%"nakia" does not pref-match ``Nokia"); instead ``Nokia 5250 Blue" does not
%%syntactically match  $q$ because there is no term in the offer title pref-matching or
%%asm-matching the keyword fragment ``c".


\subsubsection{Semantic Filters} 
\label{subsec:semfilter}

The goal of the semantic filter is to identify a set of offers that are
potentially relevant to a query where a number of category and/or facet names
occur.
% ; this is important in particular to retrieve results for \exq queries.
We define two semantic filters, one based on category matching
(\emph{cat-matching}), and one based on facet matching (\emph{fac-matching}).
Before applying semantic filters, stemming and stop-words removal are applied to
a query $q$ submitted by the user, obtaining a new query, denoted by
$\overline{q}$ to which filters are applied.

Intuitively, the \textit{cat-matching} filter returns all the offers
that belong to some category matching a query $\overline{q}$. This set
$O^{\overline{q}}_{CAT}$ is defined as follows. A category $c$ matches a keyword
$k$ iff there exists a category term $i\in I^{C}$ associated with $c$ such that
$i=k$ (remember that category names can contain more terms, and that an indexed
category term can be associated with more categories); an offer $o$ cat-matches
a query $\overline{q}=\{k_{1},...,k_{n}\}$  iff it belongs to a category $c$
that matches at least one keyword $k \in \overline{q}$.

% the set $O^{\overline{q}}_{CAT}$ of offers that cat-match with $\overline{q}$
% consist of all the offers that cat-match with \textit{at least} string $s\in
% \overline{q}$.
Intuitively, the \textit{fac-match} filter returns all the offers annotated with
some facet matching a query $\overline{q}$; this set $O^{\overline{q}}_{FAC}$ is
defined as follows. A facet $f$ matches a keyword $k$ iff there exists a facet
term $i\in I^{F}$ associated with $f$ such that $i=k$ (remember that only facet
values are considered in the matching process); an offer $o$ fac-matches a query
$\overline{q}=\{k_{1},...,k_{n}\}$ iff it is annotated with \textit{at least}
one facet $f$ that matches at least one keyword $k \in \overline{q}$ (remember
that an offer can be annotated several facets).
Given a query $q$, we also identify the sets $C^q$ and $F^q$, respectively
representing the set of all the categories and facets that match with at least a
keyword of $k$.

As an example suppose to have a query $q$=\quoted{players with mp3}, which is
transformed in the query $\overline{q}$=\quoted{player mp3} after stemming and
stop-words removal; since the category \quoted{mp3 and mp4 players} matches the
query, all the offers belonging to this category are included in the
$O^{\overline{q}}_{CAT}$ set; since several facets such as \quoted{compression: mp3}
(where \quoted{mp3} is the value of the attribute \quoted{compression}),
\quoted{audio format: mp3}, \quoted{recording format: mp3}, match $\overline{q}$
as well, every offer annotated with these facets is included in the
$O^{\overline{q}}_{FAC}$ set.

Observe that while an offer is required to syntactically match all the terms in
the query, an offer can semantically match only one keyword in order to be
considered in the semantic matches for that query. The reason for such a
difference is that, while for precise queries targeted to retrieve offers based
on their titles the user has more control on the precision of the results (under
the assumption that the user knows what he/she is looking for), for \exq
queries containing generic category and facet terms we want to consider all the
offers potentially relevant to these vaguer terms. Although the semantic match
significantly expands the set of matching offers, the ranking process will be
able to discriminate the offers that are more relevant to the given query.

%\begin{figure}[h!]
%	\includegraphics[width=1\columnwidth]{img/category_matching.pdf}
%	\caption{Category Matching} \label{fig:catmatching}
%\end{figure}

%\begin{itemize}
%\item Category Matching
%\item Facet Matching
%\end{itemize}

\smallsubsection{Ranking and Top-k Retrieval}

Several criteria are used to rank the set of matching offers. Each criterion
produces a local relevance score; all the local scores are combined by a
\textit{linear weighted function} which returns a global relevance score for
every matching offer; the top-k offers according to the global score are finally
selected to be presented to the user.
 
\subsubsection{Basic Local Scores}

Three basic local scores are introduced to assess the relevance of an offer with
respect to a query submitted by a user.


\textbf{Popularity}: $pop(o)$ of an offer $o$ is a normalized value in the range
$[0,1]$ that represents the popularity of an offer $o$ based on the number of
views the offer received. An absolute offer popularity score is assessed
off-line analyzing log files and it is frequently updated; the absolute
popularity score is normalized at run-time by distributing the absolute
popularity of the offers matching the input query into the range $[0,1]$.
% \begin{equation} pop(o) = p, \ \text{where} \ p \ \text{is the popularity of
% the offer} o \end{equation}

\textbf{Syntactic relevance}: offers retrieved by exact (prefix) matching should
be rewarded \wrt the offers approximately matched (e.g. the order of
digits makes a difference in technical terms like product codes); we therefore
define a syntactic relevance score that discriminates between the offers matched
by the two methods. The syntactic relevance $syn_q(o)$ of an offer $o$ \wrt a
query $q$ assumes a value $m$ if $o$ is a pref-match for $q$, $n$ if $o$ is a
asm-match for $q$, and $0$ elsewhere, where $m > n$.
% \begin{equation} syn_q(o) = \left\{ \begin{array}{l l}
%   	 	n & \ \text{if $o\in O^{q}_{\mathit{pref}, k_p}$}\\
%    	m & \ \text{if $o\in O^{q}_{nedit, k_n}$}\\
% 0 & \ \text{elsei} \end{array} \right. \end{equation}

\textbf{Semantic relevance}: semantic relevance is based the principle that the
more matching facets occur in an offer, the more relevant this offer is \wrt the
query. Semantic relevance does not consider categories because the number of
offers belonging to a category is generally high and because an offer belongs to
one category only. The semantic relevance $sem_q(o)$ of an offer $o$ to an input
query $q$ is computed as the number of facets $F^{o}$ associated with $o$ and
matching with $q$, normalized over the total number of the facets matching the
query $F^{q}$, according to the following formula:
% is assessed by considering how many facets matching the query occur as
% annotations in the offer; moreover, the score is normalized over the total
% number of facets matching the offer. Let $F^{o}$ be the the set of facets
% occurring as annotations in an offer $0$ and $F^{q}$ defined as in section
% \ref{subsec:semfilter}; the semantic relevance $sem_q(o)$ of an offer $o$ \wrt
% a query $q$ is defined by the following equation:
\begin{formula}\label{formula:semrel}
sem_q(o) = \cfrac{|F^{q} \cap F^{o}|}{|F^{q}|}
\end{formula}

%The above equation captures the intuition that the more matching facets occur
%in an offer, the more relevant this offer is \wrt the query. Observe
%that the semantic relevance score is based only on the matched facets and it
%does not consider the categories: the number of offers belonging to a category
%is generally high and an offer belongs to only one category; therefore it is
%not possible to assign a different scores to offers based on the number of the
%categories matching the query.

\subsubsection{Intersection Boost with Facet Salience}

When a user submits a \taq query (see Section \ref{sec:domain-and-problem}), the basic local scores introduced above
perform quite well. When the matching is driven mostly by syntactic criteria,
the accuracy of the syntactic match and the popularity become key ranking
factors. However, \exq queries are usually more ambiguous (each single
keyword can match more categories and more facets at a same time, and matches
for each keyword are combined by means of set union); in these cases, the
semantic relevance score (see equation \ref{formula:semrel}) based on the
matching facets can be too weak, with matching categories being not even
considered in the score.

As an example, all the offers belonging to the category \quoted{vacuum cleaner}
or annotated with the facet \quoted{type: robot} match the query \quoted{robot
vacuum cleaners}; however, some of these offers describe vacuum cleaners of
having \quoted{type: robot}, while some others describe other categories of
kitchen tools having \quoted{type: robot}. The ranking scores defined so far do
not discriminate among the two kinds of offers because they do not consider
their categories; intuitively, offers describing vacuum cleaners of type robot
should be better rewarded because they match on both the category and facet
dimensions. Considering the general case when offers are matched along more
dimensions, we introduce the general principle that offers that have been
matched by more than one filter are rewarded. To capture this principle we add
to the above discussed local scores an additional score, called
\textit{intersection boost}, which rewards the offers that have been matched by
more filters. Before formally defining the intersection boost function, we
discuss the boosting factor that can be defined in order to further improve the
distribution of relevance scores among matching offers, expecially in presence
of \exq queries.



%As an example, offers belonging to the category "vacuum cleaner" and annotated with the facet "type: robot" match the query "robot vacuum cleaners"; however, also offer the facet "type: robot" can be associated to offers belonging to the category "vacuum cleaners" as well as to offers belonging to the category "kitchen tools"; as a consequence offers describing robot kitchen tools are rewarded as much as offers describing robot vacuum cleaners and these offers are more or less discriminated only by popularity, which is counterintuitive. 

%A solution to better consider the semantics of annotation for ranking is to adopt the general principle according to which offers that have been matched by more than one filter are rewarded. In other words, we add to the above discussed local scores an additional score, a boosting factor, which reward the offers that have been matched according to more criteria; we call \textit{intersection boost} this additional score.

In order to better assess the relevance of the offers, and in particular of the
offers that have been matched by semantic filters, we introduce a method which
is based on the notion of \textit{facet salience}. Intuitively we define a
special set of offers that will be considered in the boosting function; this set
consists of offers annotated with facets particularly \textit{salient to} some
categories matching the query submitted by the user. Intuitively a facet is
salient to a category if it is highly likely to occur in offers that belong to
that category. Although a facet (e.g. \quoted{type: third-person shooter}) can
occur in the description of offers belonging to a category (e.g. \quoted{games
for ps3}), the number of offers belonging to such category that are annotated
with this facet can be limited; instead, other facets, which can have similar
values (e.g. \quoted{type: first-person shooter}), that occur more frequently in
offers belonging to the same category, can be considered more salient to this
category. Therefore,when a user submits an ambiguous query like \quoted{shooter
ps3}, the matching offers include the offers annotated with the facet
\quoted{type: first-person shooter} and the offers annotated with \quoted{type:
third-person shooter}, all of which belong to the category \quoted{games for
ps3}; however, offers annotated with \quoted{type: first-person shooter} can be
considered more relevant to the query because their facets are more salient to
the category \quoted{games for ps3}.

Formally, the salience of a facet $f$ to a category $c$ is defined by a function
$sal: F \times C \rightarrow [0,1] \subset \mathbb{R}$ that represents the
conditional probability of the occurrence of a facet $f$ in the set of offers
belonging to the category $c$, that is, $O^c$; given also the set $O^f$ of the
offers annotated with the facet $f$, $sal$ is defined by the following formula:
\begin{formula}
sal(f, c) = \cfrac{|\ O^f \cap O^c \ |}{|O^c|}
\end{formula}
The set $\hat{F}^q$ of facets salient to a query $q$ is defined as the set of
facets matching with $q$ whose salience to at least one category matching $q$
is higher than a given threshold $l$:
\begin{formula}
	\hat{F}^q = \{ f \in F^q \ | \ \exists c \in C^q \ \texttt{such that} \ sal(f,
	c)\geq l\}
\end{formula}
Given a query $q$, the set $O^q_{SAL}$ of offers salient to $q$ is defined as the set
of offers that fac-match some facet $f\in \hat{F}^q$.


%Explorative queries are intrinsically ambiguous because they target several offers; for this reason, we chose to union the set of offers that semantically match with at least one term contained in a query. However, several categories and facets can match with each query term and more terms can occur in a query; the amount of results returned to an \exq query can be huge and the scores associated with the offers can be very close to each other. 

 

   
%Formally, to define the salience of a facet $f$ to a category $c$ we use a function facet-category-relevance $frc: F \times C \rightarrow [0,1] \subset \mathbb{R}$ that represents the conditional probability of the occurrence of a facet $f$ in offers belonging to a category $c$; $fr$ is defined by the following formula:  
%\[fp(f, c) = \cfrac{|\ O^f \cap O^c \ |}{|O^c|}\]
%
%Now we can define the set $\hat{F}^q$ of salient facets to a query $q$ as the set of facets matching with $q$ whose relevance to at least one category matching with $q$ is higher than a given threshold $l$; formally the set can be defined as follows: 
%\begin{equation}
%	\hat{F}^q = \{ f \in F^q \ | \ \exists c \in C^q \ t.c. \ frc(f, c)\geq
%	l\}
%\end{equation}

%Formally:
%\begin{formula}
%	O^q_{LIKE} = O^q_{\widehat{FAC}} \cap O^q_{CAT}
%\end{formula}

Now we can introduce the {Boosting with Facet Salience} (BFS) score refining the
function $boost_q(o)$ so that the salience of the facets
matching the input query is taken into account. Let $\chi_{A}$ be the
characteristic function of a set $A$ (i.e, $\chi_{x}=1$ iff $x \in A$); the $boost_q(o)$ function can be formally
defined as follows:
\begin{formula}
	boost_q(o) = \sum\limits_{A \in M = \{ O^q_{SYN}, O^q_{CAT}, O^q_{FAC},\\
	O^q_{SAL} \} } {\cfrac{\chi_A(o)}{|M|}}
\end{formula}
Observe that when there does not exist any category that matches the query,
$O^q_{SAL}$ is empty.
%
%\begin{equation}
%	w_{O^q_{SYN}}, w_{O^q_{FAC}}, w_{O^q_{CAT}}, w_{O^q_{LIKE}} \in [0,1] \subset \mathbb{R},\\
% 	w_{O^q_{SYN}}+ w_{O^q_{FAC}}+ w_{O^q_{CAT}}+ w_{O^q_{LIKE}} = 1
%\end{equation}

   
  

%Given a number of categories and facets referenced in a query we try to understand if some offers are more relevant than others because they are annotated with facets that are particular significant to their category. As an example when a user ask for "shooter ps3", there are offers matching the query because "ps3" match with the category "games for ps3"; however among these offers some match   
%

%\begin{figure}[h!]
%	\includegraphics[width=1\columnwidth]{images/facet-likelihood.pdf}
%	\caption{Facet Salience} \label{fig:facetlikelihood}
%\end{figure}


\subsubsection{Global Matching Scores and Ranking}

The global scoring function can therefore be modified considering also the
intersection boost with facet salience. Formally, the \textit{global score} can
be defined by a function $score_q$: $O^q \rightarrow [0, 1] \subseteq \mathbb{R}$ as follows:
\begin{eqnarray}
score_q(o) & = & w_{pop} \cdot pop(o) + w_{syn} \cdot syn_q(o) + \\
& & + w_{sem} \cdot sem_q(o) + w_{boost} \cdot boost_q(o) \nonumber
\end{eqnarray}
%
%\begin{equation}
%	gs(o) = w_{pop} \cdot pop(o) + w_{syn} \cdot 
%	syn(o) + w_{sem} \cdot sem(o) + w_{boost} \cdot
%	boost_q(o)
%\end{equation}
where all $w_{i}$ with $i\in{pop,syn,sem,boost}$ are in the range $[0,1]$ and sum up to 1.


%\begin{equation}
%	w_{pop}, w_{syn}, w_{sem}, w_{boost} \in [0,1] \subset \mathbb{R} \\
%	w_{pop} + w_{syn} + w_{sem} + w_{boost} = 1
%\end{equation}




