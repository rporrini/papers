\section{Problem definition}\label{sec:domain-and-problem}

\begin{figure}[h]
\includegraphics[width=1\columnwidth]{images/merchantOffers-grey.pdf}
	\caption{Example of offer description with lightweight semantic annotations} \label{fig:domain}
\end{figure}

An e-marketplace publishes his product offers as semi-structured documents,
which contain several descriptive fields such as summaries, free-text
descriptions, and so on. In this section we focus on three types of elements
that occur in the offer descriptions published by most of the e-marketplaces,
which are highlighted in Figure~\ref{fig:domain}: \emph{titles},
\emph{categories} and \emph{facets}. In the following sections we introduce the
terminology used in the rest of the paper and define the problem of
autocompleting user queries considering these elements.

\subsection{Terminology} 
An offer is associated with exactly one \textit{title} that
shortly describes the offer content (e.g., \quoted{Samsung $7.0''$ 16 GB Galaxy Tab with
Android 2.2 Wi-Fi Only}). It is also associated with exactly one
\textit{category} (e.g., \quoted{Tablet PCs}) taken from a category hierarchy. It is
associated with a set of \textit{facets} describing technical features of the
offered item, which are represented by $\langle attribute\_name,value\rangle$ couples
(e.g., \quoted{O.S.: Android 2.2}, \quoted{battery capacity: 4000 mAh} and so
on). An offer is thus represented by the triple $o=\langle
t^{o},c^{o},F^{o}\rangle$, where $t^{o}$ is an offer name, $c^{o}$ is a
category, and $F^{o}=\{f_{1},...,f_{n}\}$ is a set of facets.

Categories and facets are annotations organized according to a simple, yet
semantic, structure. When an offer $o$ is associated with a category $c$ (or a
facet $f$), we also say that $o$ is \textit{annotated with} $c$ (or $f$); since
each offer $o$, is associated with exactly one category, we also say that $o$
\textit{belongs to} a category $c$. Titles, categories and facets will be also
called descriptive dimensions (\emph{dimensions}, for short). Since one facet
can occur in offers belonging to several categories (e.g., \quoted{screen size}
can occur in offers describing \quoted{monitors}, \quoted{mobile phones},
\quoted{laptops}, and so on) categories and facets represent two different and
orthogonal annotation schemes. Moreover, it must be observed that an offer is
usually annotated with many facets, one facet attribute can be associated with
several values, and one facet value can be associated with several attributes
(e.g., \quoted{recording format: mp3} and \quoted{supported media: mp3}).

The structure whereby categories and facets are organized and the offer
annotation model used in this paper are language independent and common to
several information organization systems. For example, one can represent offers
information using knowledge representation languages from Semantic Web such as
RDF\footnote{\url{http://www.w3.org/RDF/}},
SKOS\footnote{\url{http://www.w3.org/2004/02/skos/}} or
OWL\footnote{\url{http://www.w3.org/TR/owl2-overview/}}. In this case categories
can be represented as ontology classes or SKOS concepts, while facets can be
represented as couples $\langle property~,~value\rangle$. The proposed approach
is therefore independent from a specific representation language as long as the
offer annotation model is isomorphic to the one described above.

\subsection{The autocompletion problem}

A result-driven \as presents a set of $k$ results that are most relevant to 
keyword-based query typed by the user, by processing the query at each
interaction of the user with the system. Observe that every \textit{query
fragment} (a query where the last keyword typed in by the user is a string
representing a word fragment, e.g., \quoted{smartphone nok}) is considered an
input query by the \as.

In order to be effective and perceived as instantaneous the autocompletion
operation must be completed in a relatively short time span (i.e. maximum 100
ms)~\cite{HCI}: this constraint represents a serious limit to the possibility of
exploiting complex techniques to improve the accuracy and coverage
of the autocompletion results. Since there is an element of distraction caused
by User Interface interrupts, which can overcome the user while typing the
query, the high quality of the data retrieved by the \as must justify the
distraction caused to the user by the \as ~\cite{UIDIST}. An \as has therefore
to fulfill both \textit{efficiency} and \textit{effectiveness} requirements, and
since an improvement in the latter usually has a negative impact on the former,
finding a good trade-off between efficiency and effectiveness is a major goal
for matching algorithms developed in this context.

\textit{Targeted} queries (e.g., \quoted{Samsung Galaxy Tab}) are submitted by
users that look for a specific product; the keywords used in this case usually
point to specific terms used in offer titles (e.g., brand and model). These
queries can be handled using well known exact (e.g., prefix matching) and
approximate matching techniques~\cite{NavarroStringMatching2002}, which can
support provide results also when query terms are
misspelled~\cite{EXTENDING-AUTOCOMPLETE,EFFICIENT-FUZZY-SEARCH}.
However, even approximate matching techniques would be unsuccessful when an \exq
query is submitted. Consider a query such as "Tablet PC with Office Mobile"
which refers to a class of products rather than to a specific product.
In this query, neither the keywords nor any string approximately matching these
keywords occur in the offers' titles.

Instead, many keywords used in \exq queries describe product categories (e.g.,
\quoted{Tablet PC}) and/or technical features. These pieces of information are
often available from the annotations, i.e. categories and facets, associated
with the offers. Furthermore, the distinction between \taq and \exq queries is
not sharp; in fact queries such as \quoted{Samsung Tab Office Mobile} can
contain terms referring to specific products as well as to generic product
features; we will refer to these queries as \hyq queries. Using matching
algorithms that specifically leverage well-structured information associated
with the offers (i.e., semantic matching) can therefore play a crucial role in
effectively answering \exq (and \hyq) queries. One of the greatest challenges
for a result-driven \as seamlessly processing all the above types of queries
consists in combining syntactic and semantic matching so that the capability of
handling more types of queries (improving the coverage of AS) does not lower the
quality of the results returned when \taq queries are submitted.
