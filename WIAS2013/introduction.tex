\section{Introduction}\label{sec:introduction}

An ordinary e-marketplace typically performs a matchmaking activity between
search queries submitted by the users and a set of products. Maximizing the
\emph{accuracy} and \emph{coverage} of the matchmaking algorithm over search
queries is a crucial aim that a successful e-marketplace must achieve. Providing
an autocompletion interface is a recent but already established approach to
improve product search functionalities. In fact many successful search engines
for \ecomm like the ones implemented by
Amazon\footnote{\url{http://www.amazon.com/}}~\cite{AMAZON-AUTOCOMPLETE},
Bing\footnote{\url{http://www.bing.com/shopping}},
Google\footnote{\url{http://www.google.it/shopping}} and
Yahoo\footnote{\url{http://shopping.yahoo.com/}} have implemented autocompletion
interfaces to guide users through their searches for products and offers. For
price comparison engines like
PriceGabber\footnote{\url{http://www.pricegrabber.com}} (in the U.S.),
ShoppyDoo\footnote{\url{http://www.shoppydoo.com}} (in Italy), or other
e-marketplace aggregators, providing an autocompletion feature
\textit{as-a-service} to small/medium client e-marketplaces, often characterised
by a poor retrieval features, represents an interesting business opportunity.

An autocompletion interface can perform two types of autocompletion operations:
it can complete the \emph{query} that the user is expressing by proposing a set of
queries that are most relevant and reasonable to the query fragment already
typed by the user; this type of autocompletion can be called
\emph{query-oriented}~\cite{CONTEXT-AWARE, QUERY-AUTOCOMPLETION,
EXPANSION-WORDNET, TOPIC-BASED}. The autocompletion interface can instead preview
the \emph{results} of the query fragment that is being typed by the user; this
type of autocompletion can be called
\emph{result-oriented}~\cite{OUTPUT-SENSITIVE, EXTENDING-AUTOCOMPLETE,
EFFICIENT-FUZZY-SEARCH, SEMANTIC-CONCEPT-SELECTION, VENTURI}.

Most of the autocompletion interfaces proposed so far in the
\ecomm domain (e.g., Amazon, and Bing, Google and Yahoo Shopping) are based on query-oriented
approaches. In fact, all the above systems integrate product offers coming
from several e-marketplaces, achieving an almost complete coverage of the
products available on the market. However, when a query is submitted to a single
small/medium-sized e-marketplace, correctly completing a query that would not
lead to any result, or that would lead to poorly ranked results, would not be
effective. In this cases, result-oriented autocompletion can be highly
effective, by previewing only product offers available in the e-marketplace,
and by providing an insight on the internal working of the search functionality.
In this paper we focus on the latter scenario, investigating a novel matching and
ranking technique to support result-oriented autocompletion for small/medium
e-marketplaces.

One of the main problems to address for an Autocompletion System (\as) is to
seamlessly handle the different types of query that can be submitted by a user.
Autocompletion techniques that adopt string-based matching algorithms and
consider offers' titles and descriptions, can provide accurate results when
users submit queries targeted to specific products (e.g., \quoted{Samsung
i7500}), and they can be made error-tolerant by adopting approximate matching
methods~\cite{NavarroStringMatching2002,EXTENDING-AUTOCOMPLETE,EFFICIENT-FUZZY-SEARCH}
over misspelled terms (e.g., \quoted{Samsong 7500}). However, user queries are
often not really aimed at describing a specific product that is being searched
but they are rather aimed at \emph{exploring} the set of available products or
offers, looking for products of a given category (e.g., \quoted{mobile phones}) or
characterized by specific features (e.g., \quoted{Android} as \quoted{operating system}).
Sometimes, more targeted and more generic keywords are even mixed in a \hyq type
of query (e.g., \quoted{Samsung Android}). An analysis of queries submitted to
\textit{Lina24}\footnote{\url{http://www.lina24.com}} e-marketplace showed that
54.13\% of the 242 most popular queries are \taq queries, where \exq queries and
\hyq queries cover respectively a significant 21.90\% and 23.97\% of the
queries.

The hierarchies of product categories, the most significant product features and
the systematic annotation of products \wrt these schemes of metadata represent
an invaluable source of information for handling \exq and \hyq queries. Indexing
the terms contained in these annotations enables an autocompletion interface to
provide results also for \exq and \hyq queries but obtaining a relatively poor
ranking, as shown by experiments discussed later on. For sake of clarity, we
will call semantic techniques any matching technique aimed at handling these
annotations (categories and product features) in a different way from pure
textual descriptions, in the vein of research carried out in semantic
search~\cite{SEMANTIC-SEARCH-SURVEY,SEMANTIC-SEARCH-MIKA}.

In this paper we present Composite Match Autocompletion (COMMA), a novel
semantic approach to the realization of a result-oriented \as over
semi-structured data based on the integration of syntactic and semantic matching
techniques. COMMA has been implemented and tested in a real world scenario in
terms of an outsourced service that was installed in an Italian e-marketplace.
Experimental results show that the adopted approach is viable and scalable, and
it improves the accuracy and coverage of the autocompletion results when
compared to state of the art syntactic approaches (i.e., based on full-text
indexing of product features) respecting the strict time constrain of an \as.

The approach proposed in this paper is novel \wrt other autocompletion methods
proposed so far. To the best of our knowledge, most of the approaches to
\textit{result-oriented} \as adopt only syntactic matching techniques
(i.e.~\cite{NavarroStringMatching2002,EXTENDING-AUTOCOMPLETE,EFFICIENT-FUZZY-SEARCH});
instead, the few approaches considering semantics assume that data have been
annotated with Web ontology terms~\cite{SEMANTIC-AUTOCOMPLETION,VENTURI}, and,
more significantly, do not consider the problem of ranking the matches. In
addition, the efficiency of these approaches has not been systematically
evaluated. Finally, Bing Shopping, which explicitly uses categories and product
features for autocompletion, is query-oriented and it works very differently from
the approach proposed in this paper. Bing Shopping completes the user query with
both matching categories and product features, while our approach goes a step
further suggesting offers from matching categories and/or product features.

The paper is organized as follows: the autocompletion problem and the
application domain will be discussed in Section~\ref{sec:domain-and-problem};
the matching algorithm, at the core of the COMMA approach, will be described in
Section~\ref{sec:algorithm}; details about the implementation and the deployment of
COMMA are given in Section~\ref{sec:deployment}; the experiments carried out
to evaluate the system are discussed in Section~\ref{sec:evaluation}; the
comparison with related work is discussed in Section~\ref{sec:relworks}, and
Conclusions end the paper.
