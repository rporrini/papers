\section{The matching algorithm}\label{sec:algorithm}

\begin{figure}[t]
\includegraphics[width=1\columnwidth]{images/comma-process.pdf}
	\caption{Overview of the matching algorithm.} \label{fig:algorithm}
\end{figure}

The COMMA approach consists of three main phases as sketched in
Figure~\ref{fig:algorithm}:
\begin{itemize}
\item \textbf{indexing (offline)}: offers descriptions are indexed along three
different dimensions: their title, their category and the facets they are annotated
with;
\item \textbf{filtering (online)}: given a query fragment $q$ submitted by the
user, the matching algorithm applies three filters, one for each descriptive
dimension; one filter selects offers whose \emph{titles} match with the query
(syntactic matching); one filter selects offers whose \emph{categories} match
with the query (semantic matching); one filter selects offers whose
\emph{facets} match with the query (semantic matching); the results of the
three filters are combined by means of a set union returning a final set of
matching offers;
\item \textbf{ranking (online)}: several scoring methods are applied to rank
the offers selected by the filters; local scoring methods evaluate the relevance
of each offer to the query by considering different criteria such as their
popularity, the strength of the syntactic match and the semantic relevance; the
local scores are combined in a final relevance score, which is used to rank the
offers and define the list of top-k relevant offers.
\end{itemize}

Ranking is applied after filtering because most of the scores applied in ranking
reuse the results of matches discovered during filtering; moreover, this
approach allows to limit the number of items to which scoring functions are
applied, achieving a better efficiency.

\subsection{Indexing}

In order to index the titles, categories and facets associated with each offer,
three inverted indexes are built: $I^{T}$, $I^{C}$ and $I^{F}$ respectively
denote the title, category and facet indexes. In the facet index $I^{F}$ only
facet values are indexed under the assumption that the attribute names of facets
are not very significant for searches (e.g., a user is more likely to search for
\quoted{phone Samsung android}, instead of \quoted{phone brand Samsung operating system
Android}).

All the indexes are built by extracting every term occurring in the indexed
string (respectively the name, the category identifier and the facets' values);
we tokenize the strings representing codes (e.g., from \quoted{Nokia c5-03} we
extract the three terms \quoted{nokia}, \quoted{c5} and \quoted{03}); terms are
stemmed and stop-words are removed. We call \emph{title terms}, \emph{category terms} and
\emph{facet terms} the elements belonging respectively to the sets $I^{T}$,  $I^{C}$
and $I^{F}$.

\subsection{Syntactic and Semantic Filtering}
Let $O^q_{SYN}$, $O^q_{CAT}$, and $O^q_{FAC}$ be the set of offers whose textual
descriptions (titles in our case), categories and facets respectively match a
query $q$. The set $O^q$ of offers matching a query $q$ can be defined as
follows:
\begin{equation}
	O^q = O^q_{SYN} \cup O^q_{CAT} \cup O^q_{FAC}\label{def:total}
\end{equation}

In other words, all the offers that match the input query along at least one
dimension are selected in the filtering phase. The next sections explain how
each matching set is obtained.

\subsubsection{Syntactic filters}

The set $O^q_{SYN}$ of the offers that syntactically match against a query
fragment is computed by performing a set union of the results of two filters: a
prefix-based (\emph{pref}) string matcher, and an approximate string matcher
(\emph{asm}) based on normalized edit distance
(\emph{nedit}~\cite{NavarroStringMatching2002}) and a filtering threshold
$th^{asm}$. We combine the results of these two filters to handle keyword
fragments together with complete keywords with possible misspellings. In fact,
using only an asm matcher with a reasonably high threshold would lead to poor
results when matching short prefixes of long terms.

A title term $t\in I^{T}$ \textit{pref-matches} an input keyword
fragment $k$, iff $k$ is a prefix of $t$;  a title term $t\in I^{T}$
\textit{asm-matches} an input keyword fragment $k$, iff $nedit(k,t)\leq
th^{asm}$. An offer $o$ pref-matches (asm-matches,
respectively) an input keyword fragment $k$, iff there is at least a
title term $t\in o$ such that $t$ pref-matches (asm-matches) $k$.
An offer $o$ pref-matches (asm-matches, respectively) a query
$q=\{k_{1},...,k_{n}\}$ if and only if $o$ pref-matches (asm-matches)
\textit{every} keyword $k_{i}\in q$, with $1 \leq i\leq n$. The set $O^q_{SYN}$
of offers syntactically matching an input query $q$ is defined as the union of
of all the offers pref-matching $q$ and all the offers asm-matching $q$.

\begin{example}
Suppose to have a query $q$=\quoted{nakia c}, and two offers
named \quoted{Nokia c5-03} and \quoted{Nokia 5250 Blue}; \quoted{Nokia c5-03}
syntactically matches $q$ because \quoted{nakia} asm-matches the keyword
\quoted{Nokia} and \quoted{c} asm-matches the keyword fragment \quoted{c}
(instead, observe that this is not a pref-match because
\quoted{nakia} does not pref-match \quoted{Nokia}); instead \quoted{Nokia 5250 Blue}
does not syntactically match  $q$ because there is no term in the offer title
pref-matching or asm-matching the keyword fragment \quoted{c}.
\end{example}

\subsubsection{Semantic filters} 
\label{subsec:semfilter}

The goal of the semantic filter is to identify a set of offers that are
potentially relevant to a query where some category and/or facet names occur as
keywords. We define two semantic filters, one based on category matching
(\emph{cat-matching}), and one based on facet matching (\emph{fac-matching}).
Before applying semantic filters, stemming and stop-words removal are applied to
a query $q$ submitted by the user, obtaining a new query, denoted by
$\overline{q}$ to which filters are applied.

Intuitively, the \textit{cat-matching} filter returns all the offers
that belong to some category matching a query $\overline{q}$. This set
$O^{\overline{q}}_{CAT}$ is defined as follows. A category $c$ matches a keyword
$k$ iff there exists a category term $i\in I^{C}$ associated with $c$ such that
$i=k$ (remember that category names can contain more terms, and that an indexed
category term can be associated with more categories); an offer $o$ cat-matches
a query $\overline{q}=\{k_{1},...,k_{n}\}$  iff it belongs to a category $c$
that matches at least one keyword $k \in \overline{q}$.

Intuitively, the \textit{fac-match} filter returns all the offers annotated with
some facet matching a query $\overline{q}$; this set $O^{\overline{q}}_{FAC}$ is
defined as follows. A facet $f$ matches a keyword $k$ iff there exists a facet
term $i\in I^{F}$ associated with $f$ such that $i=k$ (remember that only facet
values are considered in the matching process); an offer $o$ fac-matches a query
$\overline{q}=\{k_{1},...,k_{n}\}$ iff it is annotated with \textit{at least}
one facet $f$ that matches at least one keyword $k \in \overline{q}$ (remember
that an offer can be annotated several facets).
Given a query $q$, we also identify the sets $C^q$ and $F^q$, respectively
representing the set of all the categories and facets that match with at least a
keyword of $k$.

\begin{example}
Suppose to have a query $q$=\quoted{players with mp3}, which is
transformed in the query $\overline{q}$=\quoted{player mp3} after stemming and
stop-words removal; since the category \quoted{mp3 and mp4 players} matches the
query, all the offers belonging to this category are included in the
$O^{\overline{q}}_{CAT}$ set; since several facets such as \quoted{compression: mp3}
(where \quoted{mp3} is the value of the attribute \quoted{compression}),
\quoted{audio format: mp3}, \quoted{recording format: mp3}, match $\overline{q}$
as well, every offer associated with any of these facets is included in the
$O^{\overline{q}}_{FAC}$ set.
\end{example}

Observe that while an offer is required to syntactically match all the terms in
the query, an offer can semantically match only one keyword in order to be
considered in the semantic matches for that query. The reason for such a
difference is that, while for precise queries targeted to retrieve offers based
on their titles the user has more control on the precision of the results (under
the assumption that the user knows what he/she is looking for), for \exq
queries containing generic category and facet terms we want to consider all the
offers potentially relevant to these vaguer terms. Although the semantic match
significantly expands the set of matching offers, the ranking process will be
able to discriminate the offers that are more relevant to the given query.

\subsection{Ranking and top-k retrieval}

Several criteria are used to rank the set of matching offers. Each criterion
produces a local relevance score; all the local scores are combined by a
\textit{linear weighted function} which returns a global relevance score for
every matching offer; the top-k offers according to the global score are finally
selected and presented to the user.
 
\subsubsection{Basic local scores}

Three basic local scores are introduced to assess the relevance of an offer with
respect to a query submitted by a user.


\textbf{Popularity}: $pop(o)$ of an offer $o$ is a normalized value in the range
$[0,1]$ that represents the popularity of an offer $o$ based on the number of
views the offer received. An absolute offer popularity score is assessed
offline analyzing log files and it is frequently updated; the absolute
popularity score is normalized at runtime by distributing the absolute
popularity of the offers matching the input query into the range $[0,1]$.

\textbf{Syntactic relevance}: offers retrieved by exact (prefix) matching should
be rewarded \wrt the offers approximately matched (e.g., because the order of
digits makes a difference in technical terms like product codes); we therefore
define a syntactic relevance score that discriminates between the offers matched
by the two methods. The syntactic relevance $syn_q(o)$ of an offer $o$ \wrt a
query $q$ assumes a value $m$ if $o$ is a pref-match for $q$, $n$ if $o$ is a
asm-match for $q$, and $0$ elsewhere, where $m > n$.

\textbf{Semantic relevance}: semantic relevance is based on the principle that the
more matching facets occur in an offer, the more relevant this offer is \wrt the
query. Semantic relevance does not consider categories because the number of
offers belonging to a category is generally high and because an offer belongs to
one category only. The semantic relevance $sem_q(o)$ of an offer $o$ to an input
query $q$ is computed as the number of facets $F^{o}$ associated with $o$ and
matching with $q$, normalized over the total number of the facets matching the
query $F^{q}$, according to the following formula:
\begin{equation}\label{formula:semrel}
sem_q(o) = \cfrac{|F^{q} \cap F^{o}|}{|F^{q}|}
\end{equation}

\subsubsection{Intersection boost and facet salience}

When a user submits a \taq query (see Section~\ref{sec:domain-and-problem}), the
basic local scores introduced above perform quite well. When the matching is
driven mostly by syntactic criteria, the accuracy of the syntactic match and the
popularity become key ranking factors. However, \exq queries are usually more
ambiguous (each single keyword can match more categories and more facets at the
same time, and matches for each keyword are combined by means of set union); in
these cases, the semantic relevance score (see equation \ref{formula:semrel})
based on the matching facets can be too weak, with matching categories not even
considered in the score.

\begin{example}
All the offers belonging to the category \quoted{vacuum cleaner}
or annotated with the facet \quoted{type: robot} match the query \quoted{robot
vacuum cleaners}; however, some of these offers are about vacuum cleaners 
having \quoted{type: robot}, while others are associated with kitchen tools having \quoted{type: robot}. The ranking scores defined so far do
not discriminate among the two kinds of offers because they do not consider
their categories; intuitively, offers describing vacuum cleaners of type robot
should be better rewarded because they match on both the category and facet
dimensions. 
\end{example}

Considering the general case when offers match the query according to more than
one dimension, we define a new local score called \textit{Intersection Boost}.
This local score aims at rewarding offers that occur in many specific matching
sets selected by different filters. Before formally defining the Intersection
Boost score we introduce a new filter in addition to previously defined ones.
In order to better assess the relevance of the offers, and in particular of the
offers that have been matched by semantic filters, we introduce a filter which
is based on the notion of \textit{Facet Salience}. Intuitively we define a
special set of offers that consists of the ones annotated with facets particularly
\textit{salient to} some categories matching the query submitted by the user.
Intuitively a facet is salient to a category if it is highly likely to occur in
offers that belong to that category. Although a facet (e.g., \quoted{type:
third-person shooter}) can occur in the description of offers belonging to a
category (e.g., \quoted{games for ps3}), the number of offers belonging to such
category that are annotated with this facet can be limited; instead, other
facets, which can have similar values (e.g., \quoted{type: first-person
shooter}), that occur more frequently in offers belonging to the same category,
can be considered more salient to this category. Therefore,when a user submits
an ambiguous query like \quoted{shooter ps3}, the matching offers include the
offers annotated with the facet \quoted{type: first-person shooter} and the
offers annotated with \quoted{type:
third-person shooter}, all of which belong to the category \quoted{games for
ps3}; however, offers annotated with \quoted{type: first-person shooter} can be
considered more relevant to the query because their facets are more salient to
the category \quoted{games for ps3}.

Formally, the salience of a facet $f$ to a category $c$ is defined by a function
$sal: F \times C \rightarrow [0,1] \subset \mathbb{R}$ that represents the
conditional probability of the occurrence of a facet $f$ in the set of offers
$O^c$ belonging to the category $c$; given also the set $O^f$ of the offers
annotated with the facet $f$, $sal$ is defined by the following formula:
\begin{equation}
sal(f, c) = \cfrac{|\ O^f \cap O^c \ |}{|O^c|}
\end{equation}
The set $\hat{F}^q$ of facets salient to a query $q$ is defined as the set of
facets matching with $q$ whose salience to at least one category matching $q$
is higher than a given threshold $l$:
\begin{equation}
	\hat{F}^q = \{ f \in F^q \ | \ \exists c \in C^q \ : \ sal(f,c)\geq l\}
\end{equation}
Given a query $q$, the set $O^q_{SAL}$ of offers salient to $q$ is defined as the set
of offers that fac-match some facet $f\in \hat{F}^q$.

Now we can introduce the {Boosting with Facet Salience} (BFS) score. Let $M$ be
the set of all the matching filters defined above (i.e. syntactic, category and
facet match and Facet Salience) and $M^o_q \subseteq M$ the subset of all
matched filters for the offer $o$ given the query $q$; the $boost_q(o)$
function can be formally defined as follows:
\begin{equation}
	boost_q(o) = \cfrac{|M^o_q|}{|M|}
\end{equation}

\begin{example}
Consider the query $q$ ``robot vacuum cleaner''. Suppose that the offer $o_1$ ``i-robot roomba'', 
belonging to category ``vacuum cleaners'' and annotated with facet ``type:robot'' is matched by syntactic, 
category, facet and Facet Salience. Therefore, since $o_1$ is matched by 4 out of 4 filters, 
$boost_q(o_1) = 4/4 = 1$. On the other hand, suppose that the offer $o_2$ ``kenwood cooking chef kitchen machine'', belonging to category 
``kitchen tools'' and annotated with facet ``type:robot'', is only matched by facet filter. 
Thus, $boost_q(o_2) = 1/4 = 0.25$.
\end{example}

\subsubsection{Global Matching Score and Ranking}
The global scoring function can therefore be defined considering the
Intersection Boost with Facet Salience. Formally, the \textit{global score} can
be defined by a function $score_q$: $O^q \rightarrow [0, 1] \subseteq
\mathbb{R}$ as follows:
\begin{eqnarray}\label{global-score}
score_q(o) & = & w_{pop} \cdot pop(o) + w_{syn} \cdot syn_q(o) + \\
& & + w_{sem} \cdot sem_q(o) + w_{boost} \cdot boost_q(o) \nonumber
\end{eqnarray}
where all $w_{i}$ with $i\in{pop,syn,sem,boost}$ are in the range $[0,1]$ and sum up to 1.

\subsubsection{Top-k selection and presentation}\label{sec:presentation}
The output of the matching algorithm is a ranked set $O^q$ of offers matching a
query $q$. To effectively present $O^q$ to a user as result of an autocompletion
operation we need to present a subset of the results, namely $O^{q,k}$, selected as the top-$k$
ranked offers according the \emph{global score} function defined in
Equation~\ref{global-score}. It must be noted that due to UI constrains, the
list of results of the autocompletion operation is necessarily small and should
only contain high quality completions~\cite{EARLY-INTERFACE, UIDIST}.

A straightforward way to present the set $O^{q,k}$ of the top-k results is to
display a flat list of offers ordered by rank (\textit{presentation by rank}).
However, it has been shown that grouped presentation of autocompletion results
helps users to perform search tasks more
quickly~\cite{HIERARCHY-QUERY-EXPANSION, GROUPING-STRATEGIES}.
Thus, we defined a simple and effective \textit{presentation by category}
strategy that presents offers in $O^{q,k}$ grouped by category. Offers within
each group are ordered by rank and groups are ordered by the \emph{score} of
their top ranked offer. An example of the application of this grouping strategy
is depicted in Figure~\ref{fig:category-ranking}.

\begin{figure}[t]
\includegraphics[width=1\columnwidth]{images/category-ranking.pdf}
	\caption{The category grouping strategy.} \label{fig:category-ranking}
\end{figure}