set yrange [0:22]
set ytics 2
set ylabel "Milliseconds"
set xlabel "Dataset Magnitude Order (powers of 10)"
set xrange [1:4.5]
set xtics 0.5

set key left top
set size ratio 0.7

plot "efficiency.data" using 1:2 title "COMMA", \
"efficiency.data" using 1:4 title "TF-IDF-based (robust)", \
"efficiency.data" using 1:3 title "COMMA (not robust)", \
"efficiency.data" using 1:5 title "TF-IDF-based (not robust)"
