\section{Problem definition}\label{sec:domain-and-problem}

%In this paper we consider a query a set of keywords $q=<k_{1},...,k_{n}>$. Since the autocompletion systems has to return results while the user keeps on digiting the query, the input of the system is a \textit{query fragment}, where the last keyword of the query is usually a string that represents a fragment of a keyword (e.g. "cellphones nok"). Since keywords and keyword fragments are formally handled in the same way by a autocompletion matchmaking algorithm, we will use both the term query and query fragment in the rest of the paper.  

The
main goal of a Price Comparison Engine (PCE) is to provide a platform where a buyer can compare offers of products published by several e-marketplaces, also called \textit{Merchants}, with respect to the offers' prices and features.   
PCEs such as ShoppyDoo\footnote{\url{www.shoppydoo.com}} or PriceGabber\footnote{\url{http://www.pricegrabber.com}} therefore index product
offers from a set of e-marketplaces, and provide retrieval features such as
faceted and full-text search among them.
Providing thus an autocompletion feature in
terms of an outsourced service to those e-marketplaces with poor retrieval
features represents an interesting business opportunity.



%E-marketplaces are called
%\textit{Merchants} and the \textit{Offers} are the the product they sell. 
%To achieve this goal the concept of \textit{Product} can be defined: every offer is associated with one and only one product, thus the user can search for a product and can compare
%all the offers that refer to it. 
%When a user decide to buy an offer, is
%redirected to the merchant web site and leaves ShoppyDoo. 


%\begin{figure}
%	\includegraphics[width=1\columnwidth]{images/domain.pdf}
%	\caption{The ShoppyDoo domain} \label{fig:domain}
%\end{figure}

\begin{figure}
	\includegraphics[width=1\columnwidth]{images/merchantOffers.pdf}
	\caption{Example of offer description with lightweight semantic annotations} \label{fig:domain}
\end{figure}

A Merchant publishes his offers as semi-structured documents, which contain several descriptive fields such as summaries, free-text descriptions, and so on. We focus on three types of elements that occur in the offer descriptions published by most of the e-marketplaces, which are highlighted in Figure \ref{fig:domain}. An offer is associated with exactly one \textit{title} that shortly describes the offer content (e.g.Samsung 7.0" 16 GB Galaxy Tab with Android 2.2 (Wi-Fi Only)). An offer is associated with exactly one \textit{category} (e.g. Tablet PCs) taken from a category hierarchy. An offer is associated with a set of \textit{facets} describing technical features of the offered item, and represented by couples $\langle attribute\_name,value\rangle$ (e.g. operating system: Android 2.2; battery capacity: 4000 mAh, and so on). In the rest of the paper, an offer will be therefore represented ad a triple: $o=\langle t^{o},c^{o},F^{o}\rangle$, where $t^{o}$ is an offer name, $c^{o}$ is a category, and $F^{o}=\{f_{1},...,f_{n}\}$ is a set of facets.   

Since categories and facets associated with an offer have a structure, they can be considered \textit{lightweight semantic annotations} associated to the offers; when an offer $o$ is associated with a category $c$ (or a facet $f$), we also say that $o$ is \textit{annotated with} $c$ (or $f$); since each offer $o$ is associated with exactly one category $c$, we also say that $o$ \textit{belongs to} $c$. 
Titles, categories and facet will be also called descriptive dimensions in the rest of the paper (dimensions, for short). Since one facet can occur in offers belonging to several categories (e.g. screen size can occur in offers describing monitors, mobile phones, laptops, and so on) categories and facets represent two different annotation schemes, one orthogonal to the other one.
Moreover, observe that an offer is usually annotated with many facets, one facet attribute can be associated with several values, and one facet value can be associated with several attributes (e.g. recording format: mp3, and supported media: mp3). 

\subsection{The Autocompletion Problem}

 An AS processes the query at each interaction of the user with the system, presenting a set of results to the user; therefore, the input of the system is in general a \textit{query fragment}, that is, a query where one keyword (the last one typed in by the user) is a string representing a word fragment (e.g. "smartphone nok"). The autocompletion problem is a matching problem that in the e-Commerce domain can be defined as follows.
\textit{Given a query fragment $q=\{k_{1},...,k_{n}\}$ with keywords $k_{1},...,k_{n}$, and a set of offers $O$, the goal of a matching algorithm for result-driven semantic autocompletion is to retrieve a set $O_{n}\subseteq O$ of documents most relevant to $q$ within a time interval such that the user perceives the interaction as instantaneous; $n$ represent the cardinality of the set of results presented to the user and is usually fixed.}\footnote{Since all the keywords, including the ones representing word fragments, are formally handled in the same way by a autocompletion matchmaking algorithm, we will use the terms \textit{query} and \textit{query fragment} in an equivalent way in the rest of the paper.}. 


In order to be
effective and perceived as instantaneous the autocompletion operation
must be completed in a relatively short time span (i.e. maximum 100
ms)\cite{HCI}: this constraint represents a serious limit to the possibility of
exploiting complex techniques for the improvement of the accuracy and coverage
of the autocompletion results.
Since there is an element of distraction caused by User Interface interrupts, which can overcome the user while 
typing the query, the high quality of the data retrieved by the
\as must justify the distraction caused to the user by the \as \cite{UIDIST}.
An AS has therefore to fulfill both \textit{efficiency} and \textit{effectiveness} requirements, and since an improvement in \textit{effectiveness} usually corresponds to a loss of
\textit{efficiency}, finding a good trade-off between efficiency and effectiveness is a major goal for matching algorithms developed in this context.  

%In order to be effective a result-oriented AS should accurately retrieve and present to the user a set $D$ of $k$
%documents, offers in our domain, such that $D$ reflects the set of documents that a user would consider more relevant to $q$. If $D$ consists of an ordered list, the AS should minimize the discrepancy between the ordered list $D$ and an ideal order $D'$ that a user would consider optimal to that query.    

Let us lake a closer look to the different types of queries that a user can submit to a Merchant. Targeted queries (e.g. "Samsung Galaxy Tab") are queries submitted by users that look for a a specific product; the keywords used by a user in a targeted query usually points to specific terms used in offer titles (e.g. brand, model, and so on). Well known exact matching techniques (e.g. prefix matching) \cite{NavarroStringMatching2002} can be adopted for this type of queries. However, even when the user submits a targeted query, she might misspell the terms occurring in the product definition, and prevent the algorithm to return any match. Approximate matching techniques \cite{NavarroStringMatching2002}, which provide a solution to this problem supporting more flexible autocompletion \cite{EXTENDING-AUTOCOMPLETE,EFFICIENT-FUZZY-SEARCH}. 

However, even approximate matching techniques would be unsuccessful when an explorative query is submitted. Consider a query such as "Tablet PC with Office Mobile" which refer to a class of products rather than to a specific product. In this query, neither the keywords nor any string approximately matching these keywords occur in the offers' titles. 
%A full text search technique can provide some results to these queries, but exploiting noisy data. 
Instead, many keywords used in explorative queries aim at product categories (e.g. Tablet PC) and/or technical features. These information about products are in fact available from the lightweight semantic annotations, i.e. categories and facets, associated with the offers. Semantic can therefore play a crucial role in order to return results to explorative queries considering accurate information associated with the offers. Furthermore, the distinction between targeted and explorative queries is not sharp; in fact queries such as "Samsung Tab Office Mobile" can contain terms referring to specific products as well as to generic product features; we will refer to these queries as hybrid queries.   
%Observe that some query-driven AS available on the Web, e.g. Amazon and PriceGrubber, can complete a keyword fragment with a category term (representing a query).   

%In this paper we aim to develop a result-oriented AS capable not only to efficiently provide effective results to explorative queries, but also to seamlessly process arbitrary multi-keyword queries $q=\{k_{1},...,k_{n}\}$, independently from their type (targeted, explorative and hybrid). Most of the autocompletion techniques proposed so far in the Web domain aim to complete an input string sequence \cite{}; instead, since our AS should provide results to queries whose keywords do not explicitly refer to terms occurring in the offers titles the problem we are addressing can be considered a more general matchmaking problem. 

If we aim to define a matching algorithm that seamlessly process all the above types of queries, it is necessary to find a good balance between the accuracy of string-based matching algorithms in matching targeted queries, and the coverage that can be achieved considering terms such as categories and technical features that address a large classes of matching offers. In other words, although we want to be able to provide results for explorative and hybrid queries, this capability should not lead to a low accuracy when matching targeted queries.   




%
%A problem that is particularly relevant in our domain is to find a good trade-off between the accuracy of the order 
%
%
%In order to improve the accuracy of the results 
%
%maximize both \textit{precision} and \textit{recall} of the
%matched set of products. To address the \textit{precision} problem there are
%several approaches that can be borrowed both from Information Retrieval and
%Exact String Matching. \textit{Recall} can be maximized both considering
%possible mispellings in queries, and matching also those products that
%are semantically similar to queries with respect to product's features.
%
%Semantic techniques can be used within an AS both to extend the autocompletion
%search to concepts like product categories or product features and to enlarge
%the query result set considering semantic similarity of products with respect to
%queries.
%









%In this paper we consider a query a set of keywords $q=\{k_{1},...,k_{n}\}$. An autocompletion systems process the query at each interaction of the user with the system and return some results; therefore, the input of the system is in general a \textit{query fragment}, that is a query where one keyword (the last one digited by the user) is a string representing a word fragment (e.g. "cellphones nok"). Since all the keywords, including the ones representing word fragments, are formally handled in the same way by a autocompletion matchmaking algorithm, we will use both the terms query and query fragment in the rest of the paper.  

%The autocompletion problem can be defined as follows: given an input query fragment $q$, suggest a 
%retrieve a ordered list of $k$ results (offer titles in this domain) that best satisfy the user needs, where $k$ is small enough not to distract the user from itssuch that the user  that best match  

%The concept of autocompletion search query has been formalized in
%\cite{COMPLETE-SEARCH} as follows:
%\begin{definition}
%An autocompletion search query is a pair $(W,D)$, where $W$ is a range of words
%(all possible completions of the last word which the user has started typing),
%and $D$ is a set of documents (the hits for the preceding part of the query).
%To process the query means to compute the set $\Phi$ of all word-in-document
%pairs $(w, d)$ with $w \in W$ and $d \in D$, as well as both the set of
%matching documents $D′=\{d : \exists(w, d) \in \Phi\}$ and the set of matching
%words $W′=\{w : \exists(w, d)\in \Phi\}$. (Note that for the very first query
%word, $D$ is the set of all documents.)
%\end{definition} If we are interestred in integrating an autocompletion
%system within an e-markeplace the definition above can be extended as
%follows:
%\begin{definition}
%An autocompletion search query is a pair $(W,D)$, where $W$ is a range of words,
%and $D$ is a set of products. To process the query means to compute the set
%$\Phi=\{(w,d) \; with\; w \in W\; and\; d \in D\;|\; match(w, d) \geq k \}$
%where $match: W \times D \rightarrow \left[ 0, 1 \right]$ is considered to be a
%matching function between a query word and the set of products, and $k$ is
%a fixed threshold.\end{definition} 

%When we integrate an AS within a small or
%medium business e-markeplace with poor retreival functionality we cannot trust
%e-marketplace search feature. Thus we 
%want to evaluate only the set
%of all matching documents, because this will allow the user to bypass the
%e-markeplace's poor search feature.


