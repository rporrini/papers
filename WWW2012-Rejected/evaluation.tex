\section{COMMA Evaluation}\label{sec:evaluation}
We performed an empirical study in order to assess both effectiveness and
efficiency of the proposed algorithm. Furthermore, we also evaluated COMMA in a
real world scenario, as an \as installed in an Italian e-marketplace.

From the effectiveness point of view, the goal of the study are the following:
(i) to compare  the best configuration of COMMA to a purely syntactic approach;
(ii) to assess the effects of the different parameters of COMMA on accuracy and
coverage of the autocompletion results; (iii) to study the effects of the
category ranking function on accuracy and coverage of the autocompletion
results. Considering the analysis of the efficiency of the approach, the goal of
the study is: (iv) to verify that COMMA satisfies the strict time constraint
imposed by the domain and (v) to verify that the approach can handle the current
amount of data to be managed in the autocompletion operation and possibly scale
up. Finally, the goal of the real world scenario evaluation is (vi) to show that
the employment of this \as increases the \textit{conversion rate}, that is, the
ratio of users that complete a purchase over the users that visit the e-marketplace.

\begin{figure}[t] \includegraphics[width=1\columnwidth]{images/architecture.pdf}
	\caption{COMMA evaluation deployment scenario.} \label{fig:architecture}
\end{figure}

COMMA has been implemented using the Java programming language exploiting
several functions of the Lucene search engine library\footnote{http://lucene.apache.org/};
further details COMMA's implementation are out of the scope of this paper. We only
clarify that, as shown in Figure~\ref{fig:architecture}, the system accepts the AJAX calls
from the client web page and supplies results of the matching operation on offers,
exploiting category and facet information, by means of JSON format. This
deployment allowed for the provisioning of the autocompletion functionality as a
remote service rather without requiring a deployment on the e-marketplace
server. It must be noted that due to UI constrains, the list of results of the
autocompletion operation is necessarily small: the size of this list was set to
$7$, that represents a good trade-off between compactness and coverage of
relevant results. Moreover, it is important to notice that the top $7$ offers
according to the COMMA global score are not ordered according to the score, but
they are rather grouped according to their category to reduce the cognitive load
of the user when interpreting the results of the autocompletion query~\cite{GROUPING-STRATEGIES}. 
Although we will evaluate our approach considering the final rank returned by the algorithm defined in Section \ref{sec:algorithm}, we will show that the categorized presentation of the autocompletion results does not 
significantly affect the AS effectiveness.

%Therefore,
%it may happen that an offer that would be ranked in a position between the
%second and sixth, according to its score, is actually presented later in the
%list of results, because it belongs to a category that is different from the one
%of the top offer.

All the experiments where run from May to October 2011 on an Intel
Core2 2.54GHz processor laptop, 4GB RAM, a single 150GB hard disk, under the
Ubuntu Linux Desktop 11.04 32bit operating system. In order to run the
experiments involving real users, COMMA has been deployed to an Ubuntu Linux
Server 10.04 LTS 64bit, 2GB RAM, 2GB storage, virtualized machine.

\subsection{Effectiveness}
In order to assess the effectiveness of the proposed algorithm we adopted the
Normalized Discounted Cumulative Gain (nDCG)~\cite{DCG-EARLY}, in the
formulation adopted in~\cite{DCG}. The Discounted Cumulative Gain (DCG) is a
measure of effectiveness commonly employed in the Information Retrieval area;
it measures the cumulative gain of documents proposed as results to a query
considering their position in the list of results, discounting the contribution of each
document to the overall score as rank increases. The rationale of the measurement
is that highly relevant documents should appear in the top positions of the list of svnresults
and, more generally, that the list should ideally be ordered according to the
relevance of the documents to the query. The nDCG measure is obtained
normalizing the DCG measure according to the ideal DCG measure of the result
list.

In our specific case, COMMA proposes a list of offers instead of documents,
therefore, given a ranked set of offers $O$ and the ideal ordering of the same
set $I$, the DCG at a particular rank $r$ is defined as
\begin{equation}
DCG(O, r) = \sum\limits_{i=1}^r \cfrac{2^{r(i)} - 1}{log(1+i)},
\end{equation}
where $r(i)$ is the assessed graded relevance of the offer $i$ (adopting the scale 
0=Bad, 1=Fair, 2=Good, 3=Excellent relevance). Consequently the nDCG of $O$ at
a particular rank $r$ is defined as
\begin{equation}
nDCG(O, r) = \cfrac{DCG(O, r)}{DCG(I, r)}
\end{equation}
Intuitively, a higher nDCG value corresponds to a better agreement between the list 
proposed by the system and the human judgements.

\begin{figure}[t] \includegraphics[width=.9\columnwidth]{plots/ndcg-overall-cropped.pdf}
\caption{Normalized DCG for the 4 configurations of COMMA at different rank
thresholds.}
	\label{fig:ndcg-overall}
\end{figure}

\subsubsection{Experimental Setup}
To the best of our knowledge there is no available benchmark and dataset defined
to evaluate and compare \as, and in particular semantic ones. Therefore, for the
experiments regarding the effectiveness a dataset containing 5594 offers from a
real e-marketplace has been adopted\footnote{An archive containing the datasets
adopted for evaluating COMMA's effectiveness and efficiency can be downloaded from
\url{http://www.lintar.disco.unimib.it/www2012/query_set_extended.zip}}. The offers in
the dataset are categorized
using vertical and transversal categories, respectively 92 and 463. Both the
graded relevance judgement and the ideal rank of the offers have been obtained
by having a questionnaire completed by a set of 10 average users of
e-marketplaces. The questionnaire presented a set of 20 queries (8 specific, 9
exploratory and 4 mixed queries) to the users. All of the queries have been
manually constructed, considering frequent types of queries made by Italian
users of the e-marketplace. For example, the query \textit{nakon coolpix} (notice
the misspelling) is classified as specific, the query \textit{nokia
wifi} is exploratory, and the query \textit{lcd samsung} is classified as mixed, since
all the terms can refer both to an offer's name and facets. The user was asked to
assign a graded relevance judgement (adopting the
previously introduced scale) to a set of distinct 14 offers from the dataset
with respect to each query. Every other offer from the dataset was considered to
be irrelevant and thus the value 0 was assigned to it. All the questionnaires
were aggregated in order to create the mean graded relevance judgement and the
ideal rank of the offers from the dataset with respect to each query. COMMA results
to the above queries were then retrieved in different configurations
of the matching algorithm; the evaluation of these tests will now be given.

\begin{figure}[t]
\vspace{-2pt}
	\includegraphics[width=.9\columnwidth]{plots/ndcg-boost-varying-cropped.pdf}
	\caption{Normalized DCG at fixed rank values for different configurations of
	COMMA with different value of $w_{boost}$.}
	\label{fig:ndcg-boost-varying}
\vspace{-2pt}
\end{figure} 

\subsubsection{Experiments}

In the first experiment several configurations of COMMA are compared to a purely
syntactic approach and more precisely:  (i) COMMA best configuration; (ii) COMMA
configuration without both \textit{intersection boost} and \textit{facet
salience}; (iii) COMMA configuration with \textit{intersection boost} that not
considers \textit{facet salience}; (iv) COMMA configuration with syntactic
techniques only, that can be considered as a baseline.

All configurations of COMMA that leverage both semantic and syntactic techniques
perform better than the baseline, at all considered rank thresholds, as
depicted in the Figure~\ref{fig:ndcg-overall}. This is clearly due to the presence of
exploratory queries that are not correctly managed by purely syntactic approaches.
While this was therefore expected, the impact of some specific elements of the COMMA
approach were not as easily predicted. In particular, the \textit{intersection boost} usage
in the global scoring function represents a big improvement with respect
to the simple scoring function, because it rewards the offers selected by more
than one filtering function, and thus provides a more selective and appropriate rank
with respect to the user's judgement. The usage of \textit{facet salience} does
not affect the performance for rank threshold 1, while it enables COMMA to perform
a little better for higher rank thresholds. This indicates that the main difficulty in
responding ambiguous queries is to decide the offers that are not the most relevant
but still relevant to the query.

In the second experiment the effect of the $w_{boost}$ weight in determining the
overall effectiveness of COMMA is evaluated. Figure~\ref{fig:ndcg-boost-varying}
presents the nDCG values at fixed ranks (1, 7 and 14 respectively) for different
configurations of COMMA, each with a different value of $w_{boost}$ (ranging
from $0.1$ to $1$), while the other parameters were set according to the
following criteria
$w_{O^q_{SIN}},w_{O^q_{FAC}},w_{O^q_{CAT}},w_{O^q_{LIKE}}=0.25$ and $w_{pop} =
w_{synt} = w_{sem} = \frac{1 - w_{boost}}{3}$. This experiment shows that the
best values of $w_{boost}$ between $0.6$ and $0.9$. In other words, the higher
the weight of the intersection boost factor is, the better COMMA ranks according
to human judgement, with the exception of the maximum value of 1. In fact when
$w_{boost} = 1$ the global scoring function reduces to $score_q(o) = boost_q(o)$
(since all the local score's weights are 0 except for $w_{boost}$) and thus no
other local score can actively contribute to the global score. As the next
experiment will show all the local scores should contribute to the global score
in order to maximize the effectiveness of COMMA.

\begin{figure}[t]
\vspace{-2pt}
\includegraphics[width=.9\columnwidth]{plots/ndcg-localscores-varying-cropped.pdf}
\caption{Normalized DCG for different 4 configurations of COMMA at different rank thresholds.}
	\label{fig:ndcg-localscores-varying}
\vspace{-2pt}
\end{figure}

In the third experiment the effects of different combinations of weights
$w_{pop}$, $w_{synt}$ and $w_{sem}$ on the efficiency of COMMA are
studied. The Figure~\ref{fig:ndcg-localscores-varying} depicts the nDCG
values for 4 different configurations of COMMA at fixed rank thresholds. For
all the configurations the value of $w_{boost}$ is fixed to 0.6, while the values
of the remaining weights are combined in order to favor a different local score for
every different configuration. All those configurations are compared to the one
with all constant and fixed weights $w_{pop}$, $w_{synt}$ and $w_{sem}$, that
is, the one in which all the local scores contribute equally to the rank, a
configuration that can be considered as a baseline. Both the weight configuration
that privilege semantic and popularity scores perform similarly or worse than the
baseline, while the configuration that emphasizes syntactic local score performs
better. The results of this experiment indicate that syntactic criteria are
still the most valuable ranking factor, thus demonstrating the value of the
proposed approach of combining this criteria with semantic ones that instead
allow extending the set of offers potentially considered thanks to the semantic
filters.

\begin{figure}[t]
\vspace{-2pt}
\includegraphics[width=.9\columnwidth]{plots/ndcg-pure-vs-category-cropped.pdf}
\caption{Normalized DCG at different rank thresholds for COMMA using
two different ranking functions.}
	\label{fig:ndcg-pure-vs-category}
\vspace{-2pt}
\end{figure}

In the last experiment the effects of the aggregated category ranking function
on the overall COMMA effectiveness are analyzed. As discussed at the beginning
of this section, results are grouped according to their category and we wanted to
quantitatively evaluate the impact of this choice. The 
Figure~\ref{fig:ndcg-pure-vs-category} shows the Normalized DCG curves for two
configurations of COMMA that differs only in the ranking function. In the first
configuration the global scoring function $score$ is used as a ranking function,
while in the second configuration the $cat\_rank$ function is used. The curves
are substantially the same, thus demonstrating that a categorized display of
the autocompletion process results does not decrease the efficiency of COMMA.
Usability tests would be necessary to understand if the category ranking is
actually preferred by users in the real world setting.

\subsection{Efficiency}
The efficiency of an AS is actually a necessary condition for its effectiveness, as
discussed in the introduction of the paper. In particular the autocompletion
operation must take place in a relatively short time span (100 ms) in order to
be perceived as instant by the user. Therefore we measured the efficiency of
the proposed approach, to show that it is viable and scalable.

We assessed the efficiency of COMMA with respect to time elapsed for the
computation of an autocompletion operation. For these experiments we used
a dataset of 30725 offers from a real e-marketplaces (please consider that a
single e-marketplace, like the one employed for the tests described in the 
effectiveness section, generally has a much smaller set of offers). The offers in
the dataset are categorized using vertical and transversal categories, respectively
206 and 936. The elapsed time for the computation operation is computed for a
varying cardinality of the set of offers, in order to analyze the scalability of the
COMMA approach.

Test queries are taken from a set of 200 variable length queries (chosen among
the most common queries actually submitted by users of the PCE); each query was
submitted to COMMA and the elapsed time for computation was measured. All the
results of the single query computation are merged to a mean elapsed time measure.
The test carried out in a single computer, in order to isolate costs related to the actual
computation of the COMMA matching algorithm.
Results of this test are shown in Figure~\ref{fig:efficiency}: COMMA is compared to
a purely syntactic autocompletion approach, and both approaches have been compared
to their versions not employing approximate string matching algorithms (not robust, in the
graph). 

\begin{figure}[t]
\vspace{-2pt}
	\includegraphics[width=.9\columnwidth]{plots/efficiency-cropped.pdf}
	\caption{Average elapsed time for autocompletion operations with different sizes
	for the sets of offers.} \label{fig:efficiency}
	\vspace{-2pt}
\end{figure}



The first comment to the results is that when dealing with a set of offers of this cardinality
COMMA is able to respect the strict requirements of an AS, completing the
autocompletion operation in less than 25 ms. This leaves a reasonable time for network
overhead. A second kind of analysis is related to the semantic related costs of the
COMMA approach on the overall computation time: this value can be estimated by
considering the difference between COMMA and the purely syntactic robust approach.
Within the experimented field, this difference remains quite limited and it does not grow
significantly with the growth of the cardinality of the offers set. Finally, the computational
costs related to the approximate string matching dominates the overall costs of computation:
when COMMA was configured not to employ approximate matching the computation costs
significantly decreased to less than 5 ms. This problem is due to a suboptimal implementation
of the approximate string matching algorithm in the adopted Lucene framework; however, it
must be noted that the inclusion of an improved algorithm in the framework has already been
announced and therefore the overall performance of COMMA will significantly improve.

\subsection{Real World Experimentation}
The experiments in real world scenario were run to evaluate COMMA from a
commercial perspective. Two different experiments were run considering two
different configurations of COMMA deployed to an Italian e-marketplace dealing
with consumer electronics and photography related goods. The results reported in
this section are not as extensive as the ones previously described due to
confidentiality agreements with the e-marketplace company.

The goal of the first experiment was to evaluate the impact of the addition of
the autocompletion function to the e-marketplace web site, with particular
reference to the effects on usability of the web site search function. The
autocompletion function is a form of site adaptation and we were concerned that
it could reduce overall site usability. Therefore, we gathered data~\footnote{We
used Google Analytics in order to gather data about the usage of the
autocompletion function. More information can be found at
\url{http://www.google.com/analytics/}} about the usage of the autocompletion
function, adopting a purely syntactic configuration of COMMA (we ran the test in
the early phases of the work): results showed that during the test period (50
days) users increasingly employed the autocompletion function (considering the
frequency of activations of the autocompletion function on the overall number of
page views) and they also growingly selected offers proposed by the AS.

In a later phase, we evaluated by means of an A/B test~\footnote{The A/B test
was carried out using Visual Website Optimizer A/B testing tool. More
information can be found at \url{http://visualwebsiteoptimizer.com/}} during a
shorter time period (5 days, for a total of 7,339 visitors) the impact of the
semantic autocompletion function on the (previously introduced) conversion rate:
the introduction of this function led to an increase of $6.67\%$ of the
conversion rate.