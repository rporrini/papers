\section{INTRODUCTION}\label{sec:introduction}

%\begin{itemize}
%\item Contesto Ecommerce 
%\item SMEs, limited possibility to implement and maintain an internal search engine
%\item Role of PCEs and developments in providing search as a service
%\item Specificities of the context: availability of semi-structured information regarding categories and features of products, availability (through PCE data) of information about product popularity
%\item Opportunity to exploit the already available data for implementing a semantic form of product search by means of an autocompletion functionality
%\item The need of empowering this kind of search functionality with a semantic approach is due to the relatively significant presence of exploratory queries, I.e. Queries that do not specify a product but rather a category or specific features.
%\end{itemize}

An electronic marketplace (e-marketplace)
%~\cite{BAKOS} 
acts as a me-dium between
sellers and potential online buyers: matching seller's offerings with buyers'
preferences is therefore one of the most urgent goals for an e-marketplace. An
ordinary e-marketplace typically performs a matchmaking activity between a set
of products and search queries submitted by the users. Maximizing the
\emph{accuracy} and \emph{coverage} of the matchmaking algorithm over search
queries is a crucial aim that a successful e-marketplace must achieve.
Nonetheless, usually small and medium e-marketplaces do not provide advanced
search features, because they do not have the required technical skills,
competences and personnel to implement and maintain them. As a consequence,
small-medium e-marketplaces are often characterized by low quality search
functionalities, that is, with low accuracy, coverage and poor ranking.
Providing an autocompletion interface is a recent but already established
approach to improve product search functionalities, as testified by the fact
that many successful e-marketplaces like
Amazon\footnote{\url{http://www.amazon.com/}} implemented autocompletion
interfaces to drive users in their searches for products and
offers~\cite{AMAZON-AUTOCOMPLETE}. 
%Providing thus an autocompletion feature in
%terms of an outsourced service to those e-marketplaces with poor retrieval
%features represents an interesting business opportunity.

An autocompletion interface can perform two types of autocompletion operations:
on one hand, it can complete the \emph{query} that the user is expressing,
proposing a set of queries that are most relevant and reasonable to the query
fragment already typed by the user; this type of autocompletion can be called
\emph{query-oriented}\cite{CONTEXT-AWARE, QUERY-AUTOCOMPLETION,
EXPANSION-WORDNET, TOPIC-BASED}. On the other hand, the autocompletion interface
can preview the \emph{results} of the query fragment that is being typed by the
user; this type of autocompletion can be called
\emph{result-oriented}\cite{OUTPUT-SENSITIVE, EXTENDING-AUTOCOMPLETE,
EFFICIENT-FUZZY-SEARCH, SEMANTIC-CONCEPT-SELECTION, VENTURI}. 
Result-oriented autocompletion is generally appreciated by the users since they provide an insight of the
internal working of the search functionality.
% however the second one also
%simplifies the detection of typing mistakes that would lead to search operations
%producing no results. 
Moreover it is particularly relevant in the e-commerce
application context, since correctly completing a query that would not lead to
any result, or that would lead to poorly ranked results, would not be beneficial for the e-marketplace. 
%In order to be
%effective and perceived as instantaneous, however, the autocompletion operation
%must be completed in a relatively short time span (i.e. maximum 100
%ms)\cite{HCI}: this constraint represents a serious limit to the possibility of
%exploiting complex techniques for the improvement of the accuracy and coverage
%of the autocompletion results.

%The increasing popularity and success of e-marketplaces has created the
%conditions for the birth of a market for the so called Price Comparison Engines
%(\textit{PCE}s). A PCE (e.g. ShoppyDoo\footnote{\url{www.shoppydoo.com}} or
%TrovaPrezzi\footnote{\url{www.trovaprezzi.it}}, to mention two significant cases
%in the Italian market) collects all products from a set of e-marketplaces and
%provides retrieval and comparison features among them. A PCE usually gathers
%e-marketplaces that are small or medium, in order to increase their visibility
%with respect to search engines' results. Usually small and medium e-marketplaces
%do not provide advanced search features, because they don't have the required
%technical skills, competences and personnel to implement and maintain them; this
%usually results in low quality search functionalities, that is, with low
%accuracy, coverage and poor ranking. Providing thus an autocompletion feature in
%terms of an outsourced service to those e-marketplaces with poor retrieval
%features represents a business opportunity that can be exploited by PCEs.

%In addition to this fertile context, there is also a structural opportunity to
%be exploited: PCEs gather information about products and offers of
%e-marketplaces in terms of semi-structured information that is related to
%categories and features of products. Moreover, they also gather information
%about the popularity of various products in terms, for instance, of frequency of
%comparison of different prices for a specific product. 

Syntactic autocompletion techniques based on string-based matching algorithms, can return accurate results 
when users submit queries targeted to specific products (e.g. "Samsung i7500"); however, result-driven autocompletion purely based on syntactic matching can not manage other types of queries, even when approximate matching methods \cite{NavarroStringMatching2002,EXTENDING-AUTOCOMPLETE,EFFICIENT-FUZZY-SEARCH}  are adopted and the system is able to return results for queries with misspelled terms (e.g. "Samsong 7500").
It must be considered, in fact, that user queries
sometimes are not aimed at describing a specific product that is being searched
but they are rather aimed at \emph{exploring} the set of available products or
offers, looking for products of a given category (e.g. mobile phones) or
characterized by specific features (e.g. Android as operative system). Sometimes more targeted and more generic keywords are mixed in a hybrid type of query (e.g. "Samsung Android"). As an example, in the \textit{Lina24}\footnote{\url{www.lina24.com}} e-marketplace, 54.13\% of the 242 most popular queries are targeted queries, where exploratory queries and hybrid queries cover respectively a significant 21.90\% and 23.97\% of the queries.  

The hierarchies of
product categories, the most significant product features and the systematic
annotation of products with respect to these schemes of metadata represent an
invaluable source of information that can be exploited to extend the basic
syntactic matching algorithms to consider some forms of semantics as
well~\cite{SEMANTIC-SEARCH-SURVEY}. 
 The idea of \emph{semantic
autocompletion} has been introduced in the semantic Web and applied to documents annotated with Web ontology terms~\cite{SEMANTIC-AUTOCOMPLETION,VENTURI}; the implementation of a \emph{semantic
autocompletion} approach in this domain, is therefore not only
viable but also useful to improve the accuracy of the search functionality as
well as to enable the management of a category of queries that could not be
managed by a simple syntactic approach.

In this paper we present Composite Match Autocompletion (COMMA), a novel semantic approach to the
realization of a (result-oriented) Autocompletion System (\as) over semi-structured data based on the integration of syntactic and semantic matching techniques. COMMA has been implemented and tested in a real world scenario in terms of an
outsourced service that was installed in an Italian e-marketplace. Experimental results show that the adopted approach
is viable and scalable, and it improves the accuracy and coverage of the autocompletion results when
compared to a purely syntactic approach respecting the strict time constrain of
an \as.

The more significant contributions of the approach proposed int his paper are: the capability to seamlessly process queries of arbitrary length and type; the exploitation of lightweight semantic annotations largely available in many semi-structured datasets, and in particular, in the e-Commerce domain; the efficient integration of robust
syntactic matching techniques with semantic matching techniques into a single ranking score. To the best of our knowledge, most of the approaches to result-oriented AS adopts only syntactic matching techniques (i.e. \cite{NavarroStringMatching2002,EXTENDING-AUTOCOMPLETE,EFFICIENT-FUZZY-SEARCH}); instead, the few approaches considering semantics assume that data have been annotated with Web ontology terms~\cite{SEMANTIC-AUTOCOMPLETION,VENTURI}, and, more significantly, do not consider the problem of producing a final ranking of the matches (in addition, the efficiency of these approaches has not been systematically evaluated).      

The paper is organized as follows: the autocompletion problem and the
application domain will be discussed in Section~\ref{sec:domain-and-problem}; the matching algorithm, at the core of the COMMA approach, will be described in Section~\ref{sec:algorithm}; the experiments carried out to evaluate the system are discussed in Section~\ref{sec:evaluation}; the comparison with related work is discussed in Section~\ref{sec:relworks}, and Conclusions end the paper.
%
%the Autocompletion Problem and the
%application domain will be formalized in Section~\ref{sec:domain-and-problem};
%the resulting algorithm (that will be described in Section~\ref{sec:algorithm})
%has been implemented and tested in a real world scenario in terms of an
%outsourced service that was installed in an Italian e-marketplace. The achieved
%results will be described in Section~\ref{sec:evaluation}. The adopted approach
%is innovative and novel, as we will discuss in Section~\ref{sec:relworks}, but
%also viable and scalable, as the experimental results will show: the adopted
%approach improves the accuracy and coverage of the autocompletion results when
%compared to a purely syntactic approach respecting the strict time constrain of
%an \as. Conclusions about the research effort and its application will end the
%paper.
