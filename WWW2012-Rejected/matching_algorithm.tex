\section{The matching algorithm}\label{sec:algorithm}

The COMMA approach consists of three main phases as sketched in
Figure~\ref{fig:two-phases}:
\begin{itemize}
\item \textbf{indexing (off-line)}: offers descriptions are indexed along three different dimensions: their title, category and the facets they are annotated with. 
\item \textbf{filtering (runtime)}: given a query fragment $q$ submitted by the user, the matching algorithm applies three filters, one for each descriptive dimension; one filter selects offers whose \emph{titles} match with the query (syntactic matching); one filter selects offers whose \emph{categories} match with the query (semantic matching); one filter selects offers whose \emph{facets} match with the query (semantic matching); the results of the three filters are combined by means of a set union returning an overall set of matching offers; 
\item \textbf{ranking (runtime)}: several scoring methods are applied to rank the offers selected by the filters; local scoring methods evaluate the relevance of each offer to the query, by considering different criteria such as their popularity, the strength of the syntactic match, the semantic relevance; the local scores are combined in a final relevance score, which is used to rank the offers and define the list of top-k relevant offers.        
\end{itemize}


\begin{figure}[t]
	\includegraphics[width=1\columnwidth]{images/overall_process.pdf}
	\caption{Overview of the proposed approach} \label{fig:two-phases}
\end{figure}

\subsection{Indexing}

In order to index the titles, categories and facets associated with each offer,
three inverted indexes are built: $I^{T}$, $I^{C}$ and $I^{F}$ respectively
denote the title, category and facet indexes. In the facet index $I^{F}$ only
facet values are indexed under the assumption that the attribute names of facets
are not very significant for searches (e.g. a user is more likely to search for
``phone Samsung android", instead of ``phone brand Samsung operating system
Android").

All the indexes are built by extracting every term occurring in the indexed
string (respectively the name, the category identifier and the facets' values);
we tokenize the strings representing code (e.g. from ``Nokia c5-03'' we extract
the three terms ``nokia'', ``c5'' and ``03''. Moreover, stemming and stop-word
removal are performed to build the category and facet indexes; these techniques
are applied to those indexes because the identifiers of categories and facets'
values can be represented by natural language expressions (e.g. ``printer for
photos'') and the matching process should be transparent with respect to
singular and plural forms. We will call title terms, category terms and facet
terms the elements belonging respectively to the sets $I^{T}$,  $I^{C}$ and
$I^{F}$.

\subsection{Syntactic and Semantic Filtering}
Let $O^q_{SIN}$, $O^q_{CAT}$, and $O^q_{FAC}$ be the set of offers whose titles,
categories and facets respectively match a query fragment $q$. The set $O^q$ of
offers matching a query fragment $q$ can be defined as follows:
\begin{equation}
	O^q = O^q_{SIN} \cup O^q_{CAT} \cup O^q_{FAC}\label{def:total}
\end{equation}

In other words, all the offers that match the input query along at least one
dimension are selected in the filtering phase.
% We call syntactic match when the name is considered, and semantic match when
% the category or the facet are considered.
We explain how each matching set is obtained in the next sections.

\subsubsection{Syntactic Filter}

The set $O^q_{SIN}$ of the offers that syntactically match against a query
fragment is computed by performing a set union of the results of two filters,
respectively based on a prefix matching and approximate string matching
algorithms.

A title term $t\in I^{T}$ has a \textit{pref-match} with an input keyword
fragment $k$, iff $k$ is a prefix of $t$ \cite{NavarroStringMatching2002}; an
offer $o$ has a \textit{pref-match} with an input keyword fragment $k$, iff
there is at least a title term $t\in o$ such that $t$ pref-matches $k$.

The approximate string matching method we use is based on the normalized edit
distance (nedit) similarity metric~\cite{NavarroStringMatching2002}. A title
term $t\in I^{T}$ has an \textit{asm-match} with an input keyword fragment $k$,
iff the normalized edit distance between the two strings  $nedit(k,t)\leq
th^{asm}$, where $th^{asm}$ is a similarity threshold; an offer $o$ has an
\textit{asm-match} with an input keyword fragment $k$, iff there is at least a
title term $t\in o$ such that $t$ asm-matches $k$.

An offer $o$ pref-matches (asm-matches, respectively) a query
$q=\{k_{1},...,k_{n}\}>$ if and only if $o$ pref-matches (asm-matches) with
\textit{every} keyword $k_{i}\in q$, with $1 \leq i\leq n$. The set $O^q_{SIN}$
of offers syntactically matching an input query $q$ is defined as the union of
the set of all the offers pref-matching $q$ and the the set of all the offers
asm-matching $q$.




%\cite{NormEditDistance93}
% \cite{APPROXIMATE-STRING-MATCHING}


%The set $O^q_{SIN}$ of the offers that syntactically match against a query fragment is computed by unioning the results of a prefix matching algorithm,  and of an approximate string matching algorithm.  An offer $o$ \textit{pref-match} with an input keyword fragment $k$, iff $k$ is a prefix of some title term in $q$ there exists a title term $$whose prefix is $s$; we say that an offer \textit{pref-match} with $k$. Given an input keyword fragment $k$, approximate string matching returns all the offers for which there exists a title term whose similarity wrt $k$ is higher than a given threshold $th^{asm}$; we say that these offers \textit{asm-match} with $k$. An offer pref-match (asm-match, respectively) a query $q=\{k_{1},...,k_{n}\}>$ if and only if an offer pref-match (asm-match) with \textit{every} string $s\in q$. The set $O^q_{SIN}$ of offers syntactically relevant to the input query $q$ consists of all the offers that are pref-match \textit{or} asm-match for $q$. 


As an example, suppose to have a query $q$=``nakia c", and two offers named
``Nokia c5-03" and ``Nokia 5250 Blue"; ``Nokia c5-03" syntactically match $q$
because ``nakia" asm-matches the keyword ``Nokia" and ``c" asm-matches the
keyword fragment ``c" (instead, observe that this is not a pref-match because
"nakia" does not pref-match ``Nokia"); instead ``Nokia 5250 Blue" does not
syntactically match  $q$ because there is no term in the offer title pref-matching or
asm-matching the keyword fragment ``c".

%\begin{equation}
%O^q_{SIN} = O^{q}_{\mathit{pref}, k_p} \cup O^{q}_{nedit, k_n}
%\end{equation}

\subsubsection{Semantic Filter} 
\label{subsec:semfilter}

The goal of the semantic filter is to identify a set of offers that are potentially relevant to a query 
where a number of category and/or facet names occur; this is important in particular to retrieve 
results for explorative queries.  
We therefore define two semantic filters, one based on category matching (\emph{cat-matching}), and 
one based on facet matching (\emph{fac-matching}). 

Intuitively, the cat-match filter returns all the offers that belongs to some
category matching a query fragment submitted by the user. More formally the set
$O^{\overline{q}}_{CAT}$ of offers that has cat-match with a query fragment $q$
is defined as follows: given a input query fragment $q$, stemming and stop word
removal is applied in order to obtain a query
$\overline{q}=\{k_{1},...,k_{n}\}$; a category $c$ matches a keyword $k$ iff
there exists a category term $i^{c}\in I^{C}$ such that $i^{c}=k$ and $i^{c}$ is
associated with $c$ (remember that category names can contain more terms, and
that an indexed category term can be associated with more categories); an offer
$o$ cat-matches a keyword $k$ iff it belongs to a category $c$ that matches $k$;
an offer $o$ cat-matches a query $q$ iff it cat-matches at least one keyword $k
\in \overline{q}$.

% the set $O^{\overline{q}}_{CAT}$ of offers that cat-match with $\overline{q}$
% consist of all the offers that cat-match with \textit{at least} string $s\in
% \overline{q}$.
Intuitively, the fac-match filter returns all the offers that are annotated with
some facet matching a query fragment submitted by the user; the process is
analogous to the one given for cat-match although an offer can be annotated
several facets. Let $\overline{q}=\{k_{1},...,k_{n}\}$ be a query $q$ after
stemming and stop word removal; the set $O^{\overline{q}}_{FAC}$ of offers that
fac-matches a query fragment $q$ is defined as follows. A facet $f$ matches a
keyword $k$ iff there exists a facet term $i^{f}\in I^{F}$ such that $i^{f}=k$,
$i^{f}$ is associated with $f$ (remember that only facet values are considered
in the matching process); an offer $o$ fac-matches a keyword $k$ iff it is
annotated with at least one facet $f$ that matches $k$; an offer $o$ fac-matches
a query $q$ iff it fac-matches at least one keyword $k \in \overline{q}$.  Given
a query $q$, we also identify the sets $C^q$ and $F^q$, respectively
representing the set of all the categories and facets that match with at least a
keyword of $k$.

As an example suppose to have a query $q$=``players with mp3", which is
transformed in the query $\overline{q}$=``player mp3" after stemming and stop
word removal; since the category ``mp3 and mp4 players" matches the query, all
the offers belonging to this category are included in the
$O^{\overline{q}}_{CAT}$ set; since several facets such as "compression: mp3"
(where ``mp3" is the value of the attribute ``compression"), ``audio format:
mp3", ``recording format: mp3", match $\overline{q}$ as well, every offer
annotated with these facets is included in the $O^{\overline{q}}_{FAC}$ set.

Observe that while an offer is required to syntactically match all the terms in
the query, an offer can semantically match only one keyword in order to be
considered in the semantic matches for that query. The reason for such a
difference is that, while for precise queries targeted to retrieve offers based
on their titles the user has more control on the precision of the results (under
the assumption that the user knows what he/she is looking for), for explorative
queries containing generic category and facet terms we want to consider all the
offers potentially relevant to these vaguer terms. Although the semantic match
significantly expands the set of matching offers, the ranking process will be
able to discriminate the offers that are more relevant to the given query.

%\begin{figure}[h!]
%	\includegraphics[width=1\columnwidth]{img/category_matching.pdf}
%	\caption{Category Matching} \label{fig:catmatching}
%\end{figure}

%\begin{itemize}
%\item Category Matching
%\item Facet Matching
%\end{itemize}

\subsection{Ranking and Top-k Retrieval}

Several criteria are used to rank the set of matching offers. Each criterion
produces a local relevance score; all the local scores are combined by a
\textit{linear weighted function} which returns a global relevance score for
every matching offer; the top-k offers according to the global score are finally
selected to be presented to the user.
 
\subsubsection{Basic Local Scores}

Three basic local scores are introduced to assess the relevance of an offer with
respect to a query submitted by a user.


\textbf{Popularity}: $pop(o)$ of an offer $o$ is a normalized value in the range
$[0,1]$ that represents the popularity of an offer $o$ based on the number of
views the offer received. An absolute offer popularity score is assessed
off-line analyzing log files and it is frequently updated; the absolute
popularity score is normalized at run-time by distributing the absolute
popularity of the offers matching the input query into the range $[0,1]$.
% \begin{equation} pop(o) = p, \ \text{where} \ p \ \text{is the popularity of
% the offer} o \end{equation}

\textbf{Syntactic relevance}: offers retrieved by exact (prefix) matching should
be rewarded with respect to the offers approximately matched (e.g. the order of
digits makes a difference in technical terms like product codes); we therefore
define a syntactic relevance score that discriminates between the offers matched
by the two methods. The syntactic relevance $sint_q(o)$ of an offer $o$ wrt a
query $q$ assumes a value $m$ if $o$ is a pref-match for $q$, $n$ if $o$ is a
asm-match for $q$, and $0$ elsewhere, where $m > n$.
% \begin{equation} sint_q(o) = \left\{ \begin{array}{l l}
%   	 	n & \ \text{if $o\in O^{q}_{\mathit{pref}, k_p}$}\\
%    	m & \ \text{if $o\in O^{q}_{nedit, k_n}$}\\
% 0 & \ \text{elsei} \end{array} \right. \end{equation}

\textbf{Semantic relevance}: the semantic relevance of an offer to an input
query is assessed by considering how many facets matching the query occur as
annotations in the offer; moreover, the score is normalized over the total
number of facets matching the offer. Let $F^{o}$ be the the set of facets
occurring as annotations in an offer $0$ and $F^{q}$ defined as in section
\ref{subsec:semfilter}; the semantic relevance $sem_q(o)$ of an offer $o$ wrt a
query $q$ is defined by the following equation:
\begin{equation}\label{formula:semrel}
sem_q(o) = \cfrac{|F^{q} \cap F^{o}|}{|F^{q}|}
\end{equation}
The above equation captures the intuition that the more matching facets occur
in an offer, the more relevant this offer is with respect to the query. Observe
that the semantic relevance score is based only on the matched facets and it
does not consider the categories: the number of offers belonging to a category
is generally high and an offer belongs to only one category; therefore it is
not possible to assign a different scores to offers based on the number of the
categories matching the query.

\subsubsection{Intersection Boost with Facet Salience}

When a user submits a targeted query, the basic local scores introduced above
performs quite well. When the matching is driven mostly by syntactic criteria,
the accuracy of the syntactic match and the popularity become key ranking
factors. However, explorative queries are usually more ambiguous (each single
keyword can match more categories and more facets at a same time, and matches
for each keyword are combined by means of set union); in these cases, the
semantic relevance score (see equation \ref{formula:semrel}) based on the
matching facets can be insufficient, and matching categories are not even
considered for determining this score.

As an example, all the offers belonging to the category ``vacuum cleaner" and
all those annotated with the facet ``type: robot" match the query ``robot vacuum
cleaners"; however, some of these offers are effectively associated with vacuum
cleaners of type robot, while some others describe kitchen tools of type robot.
The ranking algorithm does discriminate among the two kinds of offers because it
does not take into account the matching categories; intuitively offers
describing vacuum cleaners of type robot should be better rewarded because they
match on both the category and facet dimensions. Considering the general case
when offers are matched along more dimensions, we introduce the general
principle that offers that have been matched by more than one filter are
rewarded. To capture this principle we add to the above discussed local scores
an additional score, called \textit{intersection boost}, which rewards the
offers that have been matched by more filters. Before formally defining the
intersection boost function, we discuss the boosting factor that can be defined
in order to further improve the distribution of the relevance scores among the
matching offers, in particular when explorative queries are considered.



%As an example, offers belonging to the category "vacuum cleaner" and annotated with the facet "type: robot" match the query "robot vacuum cleaners"; however, also offer the facet "type: robot" can be associated to offers belonging to the category "vacuum cleaners" as well as to offers belonging to the category "kitchen tools"; as a consequence offers describing robot kitchen tools are rewarded as much as offers describing robot vacuum cleaners and these offers are more or less discriminated only by popularity, which is counterintuitive. 

%A solution to better consider the semantics of annotation for ranking is to adopt the general principle according to which offers that have been matched by more than one filter are rewarded. In other words, we add to the above discussed local scores an additional score, a boosting factor, which reward the offers that have been matched according to more criteria; we call \textit{intersection boost} this additional score.

In order to better assess the relevance of the offers, and in particular of the
offers that have been matched by semantic filters, we introduce a method which
is based on the notion of \textit{facet salience}. Intuitively we define a
special set of offers that will be considered in the boosting function; this set
consists of offers annotated with facets particularly salient to the query,
because they are salient to some matched category.
 Intuitively a facet is salient to a category if it is highly likely to occur in
 offers that belong to that category.
Although a facet (e.g. ``type:third-person shooter") can occur in the
description of offers belonging of a category (e.g. ``games for ps3"), the
number of offers belonging to such category that are annotated with this facet
can be limited; instead, other facets, which can have similar values (e.g.
``type: first-person shooter"), that occur more frequently in offers belonging
to the same category, can be considered more salient to this category.
Therefore,when a user submits an ambiguous query like ``shooter ps3", the
matching offers include the offers annotated with the facet ``type: first-person
shooter" and the offers annotated with ``type: third-person shooter", all of
which belong to the category ``games for ps3"; however, offers annotated with
``type: first-person shooter" can be considered more relevant to the query
because their facets are more salient to the category ``games for ps3".

Formally, the salience of a facet $f$ to a category $c$ is defined by a function
$sal: F \times C \rightarrow [0,1] \subset \mathbb{R}$ that represents the
conditional probability of the occurrence of a facet $f$ in the set of offers
belonging to the category $c$; $sal$ is defined by the following formula:
\begin{equation}
sal(f, c) = \cfrac{|\ O^f \cap O^c \ |}{|O^c|}
\end{equation}
The set $\hat{F}^q$ 
of facets
salient to a query $q$ is defined as the set of facets matching with $q$ whose
salience to at least one category matching $q$ is higher than a given threshold
$l$; formally the set can be defined as follows:
\begin{equation}
	\hat{F}^q = \{ f \in F^q \ | \ \exists c \in C^q \ \texttt{such that} \ sal(f,
	c)\geq l\}
\end{equation}
Given a query $q$, the set of salient offers $O^q_{SAL}$ is defined as the set
of offers that fac-match some facet $f\in \hat{F}$.


%Explorative queries are intrinsically ambiguous because they target several offers; for this reason, we chose to union the set of offers that semantically match with at least one term contained in a query. However, several categories and facets can match with each query term and more terms can occur in a query; the amount of results returned to an explorative query can be huge and the scores associated with the offers can be very close to each other. 

 

   
%Formally, to define the salience of a facet $f$ to a category $c$ we use a function facet-category-relevance $frc: F \times C \rightarrow [0,1] \subset \mathbb{R}$ that represents the conditional probability of the occurrence of a facet $f$ in offers belonging to a category $c$; $fr$ is defined by the following formula:  
%\[fp(f, c) = \cfrac{|\ O^f \cap O^c \ |}{|O^c|}\]
%
%Now we can define the set $\hat{F}^q$ of salient facets to a query $q$ as the set of facets matching with $q$ whose relevance to at least one category matching with $q$ is higher than a given threshold $l$; formally the set can be defined as follows: 
%\begin{equation}
%	\hat{F}^q = \{ f \in F^q \ | \ \exists c \in C^q \ t.c. \ frc(f, c)\geq
%	l\}
%\end{equation}

%Formally:
%\begin{formula}
%	O^q_{LIKE} = O^q_{\widehat{FAC}} \cap O^q_{CAT}
%\end{formula}

Now we can introduce the {Boosting with Facet Salience} (BFC) score refining the
linear weighted function $boost_fs(o)$ so that the salience of the facets
matching the input query is taken into account. Let $\chi_{A}$ be the
characteristic function of a set $A$; the $boost_fs(o)$ function can be formally
defined as follows:
\begin{equation}
	boost_fs(o) = \sum\limits_{A \in \{ O^q_{SYN}, O^q_{CAT}, O^q_{FAC},\\
	O^q_{SAL} \} } {w_A \cdot \chi_A(o)}
\end{equation}
where all $w_A$ are in the range $[0,1]$ and sum up to 1.
%
%\begin{equation}
%	w_{O^q_{SIN}}, w_{O^q_{FAC}}, w_{O^q_{CAT}}, w_{O^q_{LIKE}} \in [0,1] \subset \mathbb{R},\\
% 	w_{O^q_{SIN}}+ w_{O^q_{FAC}}+ w_{O^q_{CAT}}+ w_{O^q_{LIKE}} = 1
%\end{equation}

   
  

%Given a number of categories and facets referenced in a query we try to understand if some offers are more relevant than others because they are annotated with facets that are particular significant to their category. As an example when a user ask for "shooter ps3", there are offers matching the query because "ps3" match with the category "games for ps3"; however among these offers some match   
%


Observe that when there does not exist any category that matches the query,
$O^q_{SAL}$ is empty. The weights in the $boost_fs(o)$ function allow to bend
the customize the boosting factor in order to reward specific matching
dimension; in the experiments discussed in section we will test different
assignments tot he weights and their effect on the accuracy of the algorithm.



%\begin{figure}[h!]
%	\includegraphics[width=1\columnwidth]{images/facet-likelihood.pdf}
%	\caption{Facet Salience} \label{fig:facetlikelihood}
%\end{figure}


\subsubsection{Global Matching Scores and Ranking}

The global scoring function can therefore be modified considering also the
intersection boost with facet salience. Formally, the \textit{global score} can
be defined by a function $score_q$: $O^q \rightarrow [0, 1] \subseteq
\mathbb{R}$ as follows:
\begin{eqnarray}
gs(o) & = & w_{pop} \cdot pop(o) + w_{syn} \cdot syn(o) + \\
& & + w_{sem} \cdot sem(o) + w_{boost} \cdot boost_fs(o) \nonumber
\end{eqnarray}
%
%\begin{equation}
%	gs(o) = w_{pop} \cdot pop(o) + w_{syn} \cdot 
%	syn(o) + w_{sem} \cdot sem(o) + w_{boost} \cdot
%	boost_fs(o)
%\end{equation}
where all $w_{i}$ with $i\in{pop,synt,sem,boost}$ are in the range $[0,1]$ and sum up to 1.


%\begin{equation}
%	w_{pop}, w_{sint}, w_{sem}, w_{boost} \in [0,1] \subset \mathbb{R} \\
%	w_{pop} + w_{sint} + w_{sem} + w_{boost} = 1
%\end{equation}




