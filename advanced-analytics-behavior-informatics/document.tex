\documentclass{article}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage[fleqn]{amsmath}
\usepackage[margin=2.5cm]{geometry}

\newcommand*{\argmax}{\operatornamewithlimits{argmax}\limits}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\numberwithin{definition}{section}

\author{Riccardo Porrini \\ DISCo \\ Universit\'a degli Studi di Milano-Bicocca \\ \texttt{riccardo.porrini@disco.unimib.it}}
\title{\textbf{Ontology Matching using Dissimilar Matchers} \\ \small{Assigment for the course: \emph{Advanced Analytics and Behavior Informatics}}}
\date{}

\begin{document}
\maketitle

\abstract{A well established approach to the problem of Ontology Matching is to
develop different matchers, each one capturing different features of ontology
concepts. Given a pair of concepts from two different ontologies, different
\emph{local similarity} scores (computed by different matchers) are aggregated
through pre-defined aggregation functions (i.e. \emph{global similarity score}).
Currently many different matchers are available: from syntax (e.g. edit
distance) to structure (e.g. graph similarity). However, hidden coupling between
matchers can introduce bias in the global similarity score. We propose to adapt
the global similarity score function with a \emph{Global Coupled Matcher
Dissimilarity} measure in order to mitigate the effect of strongly-coupled
matchers. We aim at assessing the degree of coupling between matchers by
observing their ``behavior'', that is, looking at the matchings they compute in
terms of occurrences (i.e. intra-coupling) and reciprocal dependencies (i.e.
inter-coupling). This approach looks promising (i) because of its novelty, (ii)
because it can be easily evaluated, (iii) and because can be further applied
to different granularities (e.g. coupled concept similarity).}

\section{Introduction}
Matching schemas from heterogeneous data sources plays a crucial role in the
field of Knowledge Representation and Integration~\cite{HALEVY}. The problem of
matching schemas has been extensively studied in research on Data Integration
(under the name of Schema Matching~\cite{Schema}) and Semantic Web (under the
name of Ontology Matching~\cite{OM}). In this proposal we focus on the problem
of Ontology Matching, that is, map concepts of a \emph{source} ontology to
semantically related concepts of a \emph{target} ontology~\cite{OM}. The
resulting set of mappings is called an \emph{alignment}. The main difference
between Schema Matching and Ontology Matching is that the input of the former
are databases (or even XML) schemas elements, while the input of the latter are
ontology concepts.

The common approach in computing alignments between two ontologies is to use an
ensemble of automatic algorithms, called \emph{matchers}. A matcher takes two
concepts as input and compute a \emph{local similarity} score. Each matcher
focuses on different concept features. Some matchers compare concept properties
such as label or comments using a string similarity metric, while other matchers
consider structural information. The \emph{global similarity} score of two
concepts is computed by composing local similarity scores from different
matchers through pre-defined aggregation functions. A common aggregation
function consists of a linear weighted sum of local similarity scores. However,
despite the large body of work on Ontology Matching research, automatic matcher
selection and weighting is still an open problem~\cite{OM-RECENT}.

Aggregating local similarity scores from different matchers is the most
effective approach to Ontology Matching, as testified by results of the Ontology
Alignment Evaluation Initiative
(OAEI)\footnote{\url{http://oaei.ontologymatching.org/2012/q}}. However, there
is a subtle problem that has still not been tackled in literature yet: \emph{matchers
coupling}. There are two different types of matcher coupling:
\emph{intra-coupling} and \emph{inter-coupling}. Given a concept from the target
ontology, a matcher may compute the same similarity score for many (or even all)
concepts from the source ontology (i.e. intra-coupling). Moreover, two matchers
may compute very similar scores for the same pairs of concepts from source and
target ontology (i.e. inter-coupling). An example of inter-coupled matchers is
depticted in Figure~\ref{fig:matchers}. Matchers $m_1$ and $m_2$ compute similar
scores for every pair of concepts from souce and target ontology (e.g.
\texttt{Locomotive} and \texttt{Automobile}): they are strongly inter-coupled. The
problem of coupled matchers is evident if we compute the global similarity score
between \texttt{Engine} and \texttt{Train} and between \texttt{Locomotive} and
\texttt{Train}. For simplicity and without loss of generality suppose that $w_1
= w_2 = w_3 = 1/3$ for the global similiarity score function $score$. Then
$score(\texttt{Engine}, \texttt{Train}) = 1/3 \cdot 0.4 + 1/3 \cdot 0.4 + 1/3
\cdot 0 = 0.2\bar{6}$ and $score(\texttt{Locomotive}, \texttt{Train}) = 1/3
\cdot 0 + 1/3 \cdot 0 + 1/3 \cdot 0.6 = 0.2$. Thus \texttt{Engine} from source
ontology is mapped to \texttt{Train} from target ontology, since
$score(\texttt{Engine}, \texttt{Train}) > score(\texttt{Locomotive},
\texttt{Train})$. In this toy example coupled matchers $m_1$ and $m_2$ introduce
a bias in the global similarity scoring function.

\begin{figure}
	\centering
	\includegraphics[width=.8\textwidth]{images/matchers.pdf}
	\vspace{-2cm}
	\caption{Example of two inter-coupled matchers.}
	\label{fig:matchers}
\end{figure}

A global scoring function may be biased by local scores computed by
strongly-coupled matchers. Moreover, matchers coupling degree depends on the
ontologies to match. A non-biased global scoring function should emphatize local
scores of matchers that capture significant features, that is, a minimal set of
loosely-coupled matchers. To capture this intuition we propose a \emph{Coupled
Matcher Dissimilarity} (CMD) measure based on both intra and inter coupling
between matchers. The main idea is that we can compute both intra and inter
coupling between matchers by looking at their ``behavior'', that is, looking at
matching results they produce (i.e. occurence and reciprocal dependency of top
scored concept pairs). CMD is computed for each pair of matchers and is inspired
by recent works on Coupled Clustering Ensembles. Coupled Clustering Ensembles
combine results from various base clustering methods in order to acheive overall
high-quality clustering~\cite{CLUSTERING-ENSEMBLES}. More precisely, we map the
problem of computing the coupled similarity (and consequently its dual: coupled
dissimilarity) between matchers to the problem of computing coupled similarity
between clustering methods. We also propose to use CMD to compute a \emph{Global
Coupled Matcher Dissimilarity} (GCMD) measure, which captures the inter and
intra coupling of one matcher with respect to every other matchers, and to use
this measure in the global scoring function to limit the effects of coupled
matchers.

This proposal is organized as follows. In Section~\ref{sec:related_work} we
briefly review the related work on matcher selection and configuration in
Ontology Matching along with approaches from Data Mining that introduced the
concept of coupled similarity. In Section~\ref{sec:problem_statement} we
formally introduce the problem of Ontology Matching and the problem of computing
coupled dissimilarity between matchers. In Section~\ref{sec:matching} we
formally describe how to compute coupled dissimilarity between matchers and,
consequently, how to adapt the global similarity scoring function in order to
compute the final alignment. Conclusions end the proposal.

\section{Related Work}\label{sec:related_work}
The problem of finding the best matcher configuration has been widely studied in
the Ontology Matching literature (see~\cite{OM-RECENT} for a recent survey). The
most effective and recent approaches define the problem of optimal matcher
selection as a learning problem and use different Machine Learning techiques.
Most of state of art approaches focus on Supervised Learning techiques
(e.g.~\cite{AM-EARLY, RITZE}) and Active Learning techiques
(e.g.~\cite{AM-FEEDBACK}), while a few focus on Unsupervised Learning ones
(e.g.~\cite{GENETIC}). However, the closest approach to our proposal is
AgreementMaker~\cite{AM}. AgreementMaker combines different local similarity
scores from different matchers using a linear weighted function. The authors
define the concept of \emph{confidence} that aims at capturing the ability of a
matcher to discriminate between different concepts. Confidence can be seen as a
measure of intra-coupling. However, AgreementMaker does not consider
inter-coupling between matchers, since confidence is computed for matchers taken
in isolation.

The concept of object coupling is well known in the field of Data Mining. Data
mining usually deals with categorical data, that is, objects that have many
attributes with different values. Both coupling between values and attributes
need to be considered in order to gain a more comprehensive understanding of the
similarity between objects from categorical data~\cite{COUPLING}. A recent
approach propose the \emph{Coupled Obejct Similarity} (COS) measure~\cite{CNS}.
COS evaluates the similarity between two objects considering both intra-coupled
similarity within an attribute (i.e. value frequency distribution) and
inter-coupled similarity between attributes (i.e. attribute dependency
aggregation).

Aggregating more matchers to compute more correct mappings between ontologies
captures the simple intuition that ``team is better than singles''. This simple
intuition underlays also recent works on Clustering
Ensembles~\cite{CLUSTERING-ENSEMBLES}. Clustering Ensembles combine results from
various base clustering methods in order to acheive overall high-quality
clustering on a dataset of objects. A recent and effective approach considers
and integrates the coupling relationships between base clusterings and also
between objects~\cite{CLUSTERING}. More generally, the work is about computing
coupled similarities between different algorithms based on the analysis of data
they compute. Our proposed approach to matcher selection and weighting is built
on top of this approach. We map the problem of computing the coupled
dissimilarity between matchers to the problem of computing coupled similarity
between clustering methods (we consider coupled similarity dual with respect to
coupled dissimilarity).

\section{Problem Statement}\label{sec:problem_statement}
The problem of Ontology Matching can be formally defined as follows.
\begin{definition}
$s$ is a source concept from a source ontology $S$
\end{definition}
\begin{definition}
$t$ is target concept from a target ontology $T$
\end{definition}
\begin{definition}
$m$ is a function (i.e. matcher) that compute a \emph{local similarity score} for each pair
$(s_i,t_j)$
\end{definition}
\begin{definition}
$score : S \times T \rightarrow [0,1]$ is a \emph{global similarity
score} function that aggregates local similarity scores computed by each matcher $m$. 
The simplest and effective way to combine local similarity scores from different
matchers is to define the function $score$ as a convex linear weighted function:
\begin{center}
$score(s, t) = \displaystyle\sum_{k = 1 \ldots L} w_i \cdot m_k(s, t)$, where $L$ is the number of matchers
\end{center}
\end{definition}
\begin{definition}\label{def:aligment}
The goal of Ontology Matching is to determine an \emph{alignment} $A'$ such that:
\begin{center}
$A' = \{ (s,t) | s \in S, t \in T, \argmax_{\bar{s} \in S, \bar{t} \in T} score(\bar{s}, \bar{t}) \} = (s,t)\}$
\end{center}
\end{definition}

We propose to integrate the concept of coupled similarity between matchers in a
framework for Ontology Matching. Matcher coupling can be seen by two different
prespectives: intra-coupling and inter-coupling.
\begin{definition}
	A matcher is \emph{Intra-coupled} when it compute similar local scores 
	for many different source concepts with respect to the same target concept;
\end{definition}
\begin{definition}
	Two matchers are \emph{Inter-coupled} when they compute similar scores for the 
	same pair of source and target concepts.
\end{definition}

Our hypothesis is that we can assess the coupled similarity between matchers by observing 
their ``behavior'', that is, looking at the matchings they compute in terms of occurrences 
and reciprocal dependencies. Moreover, we can consider a coupled dissimilarity to be the dual 
of coupled similarity. We propose to compute coupled similarity between each pair of matchers $(m_k,
m_{k'})$, in the same way that has been done for Coupled Clustering
Ensembles~\cite{CLUSTERING-ENSEMBLES} and integrate this measure into the
$score$ function. Thus we map the problem of computing the coupled similarity
between matchers to the problem of computing coupled similarity between
clustering methods by constructing a Matcher Information Table
$MIT$~\cite{CLUSTERING}: each row of $MIT$ is a source concept $s$, each column
is a matcher $m_k$, and each cell is a target concept $t$ such that $t =
\argmax_{\bar{t} \in T} m_k(s, \bar{t})$. We say that $t$ is \emph{matched to}
$s$. As an example, Table~\ref{tab:information} represents the $MIT$ for
matchers in Figure~\ref{fig:matchers}. We use $MIT$ to strictly map our problem
to the same problem tackled in~\cite{CLUSTERING}, in order to solve it whith the
same techinique.

\begin{table}[h!] 
	\centering
	\begin{tabular}{ |l|c|c|c| }
		\hline
		& $m_1$ & $m_2$ & $m_3$ \\
		\hline
		\texttt{Wheleed} & \texttt{Object} & \texttt{Object} & \texttt{Object} \\
		\hline
		\texttt{Locomotive} & \texttt{Automobile} & \texttt{Automobile} & \texttt{Train} \\
		\hline
		\texttt{Engine} & \texttt{Train} & \texttt{Train} & \texttt{Engine} \\
		\hline
		\texttt{Horsepower} & \texttt{Horsepower} & \texttt{Horsepower} & \texttt{Horsepower} \\
		\hline
	\end{tabular}
	\caption{Matcher Information Table for matchers in Figure~\ref{fig:matchers}}
	\label{tab:information}
\end{table}

Before introducing the proposed coupled similarity metrics we formalize several
utility concepts that will be used in the next sections, taken and adapted
from previous works on Coupled Similarity~\cite{CNS} and Coupled Clustering
Ensembles~\cite{CLUSTERING}. Given a $MIT$:
\begin{definition}
	The \emph{Matching Set} $G_k^t$ specifies the set of source concepts 
	$s \in S$ that are matched to $t$ by the matcher $m_k$
\end{definition}
\begin{definition}
	The \emph{Matched Set} $H_{k}$ specifies the set of target concepts that are 
	matched to at least one source concept by the matcher $m_k$
\end{definition}
\begin{definition}
	Given two matchers $m_k$ and $m_{k'}$ and a target concept $t$, the \emph{Inter-matching Set} 
	specifies the set of target concepts $\bar{t} \in H_{k'}$ that are matched by $m_{k'}$ to same 
	source concepts matched to $t$ by $m_k$:
	\begin{center}
	$M_{k \rightarrow k'}^t = \{\bar{t} \in H_{k'}~|~G_{k'}^{\bar{t}} \cap G_k^t \neq \emptyset \}$
	\end{center}
\end{definition}
\begin{definition}
	The \emph{Matching Conditional Probability} $P_{k | k'} (t|t')$ 
	characterize the percentage of source concepts matched to the target concept $t$ by matcher $m_k$ against
	the source concepts matched to $t'$ by matcher $m_{k'}$:
	\begin{center}
	$P_{k | k'} (t|t') = \cfrac{| G_k^t~\cap~G_{k'}^{t'}|}{|G_{k'}^{t'}|}$
	\end{center}
\end{definition}
Going back to the example from Table~\ref{tab:information}, $G_1^{\texttt{Train}} = \{\texttt{Engine}\}$, $H_1$ = 
\{\texttt{Object}, \texttt{Automobile}, \texttt{Train}, \texttt{Horsepower}\}, $M^{\texttt{Engine}}_{3 \rightarrow 2}$ = \{ \texttt{Train} \}
and $P_{2|3} (\texttt{Automobile}|\texttt{Train}) = 1/1= 1$.

\section{Matching using Dissimilar Matchers}\label{sec:matching}
\begin{figure}[h!] \centering
\includegraphics[width=.8\textwidth]{images/overall.pdf}
	\vspace{-4cm}
	\caption{Including coupled dissimilarity into the matching process.}
	\label{fig:overall}
\end{figure}
Our proposed approach is built on top of techique proposed for including
coupling information into Clustering Ensembles~\cite{CLUSTERING}. The overall
matching process that considers also the coupled dissimilarity between matchers
is sketched in Figure~\ref{fig:overall}. Given a source ontology $S$ and a
target ontology $T$ we compute all the local similarity scores between concepts
and we build the $MIT$. Then we compute the coupled similarity between matchers
considering both inter and intra coupling and we include it in a global coupled
dissimilarity measure for each matcher. We finally weight the local similarity
scores for concepts by means of the global coupled dissimilarity of matchers and
compute the final allignment.

In the following sections we define the intra and inter coupled similarities
between matchers, and how to integrate it in the global similarity score
function by computing a global coupled dissimilarity measure for matchers. The
proposed metrics aim at capturing the coupling between matcher by observing
their matching results (i.e. occurence and co-occurrence of top scored concept
pairs). All the following metrics are taken and adapted from previous work on
Coupled Clustering Ensambles~\cite{CLUSTERING}.

\subsection{Intra-Coupled Matchers}
A matcher is intra-coupled if is not able to discriminate between concepts from
source and/or target ontology. A strongly intra-coupled matcher will match many
source concepts to the same target concept. Given $MIT$ from
Table~\ref{tab:intra}, $t_1$ is the top scored concept by matcher $m_1$ for each
concept $s$, excepted for concept $s_3$: $m_1$ is strongly intra-coupled. More
formally, we capture the intra-coupled similarity of a matcher $m_k$ in terms of
coupling between the concepts that are matched by $m_k$:
\begin{definition}
	The \emph{Intra-coupled Matching Similarity for Matchers} (IaMSM) between two target 
	concepts $t, t' \in T$ matched by matcher $m_k$ is:
	\begin{center}
	$IaMSM_k (t, t') = \cfrac{|G_k^t|~\cdot~|G_k^{t'}|}{|G_k^t|~+~|G_k^{t'}|~+~|G_k^t|~\cdot~|G_k^{t'}|}$
	\end{center} 
\end{definition} 
For example, given the $MIT$ from Table~\ref{tab:intra}, $IaMSM_1(t_1, t_2) = 3/7$.

\begin{table}[h!] 
	\centering
	\begin{tabular}{ |l|c|c| }
		\hline
		& $m_1$ & $m_2$ \\
		\hline
		$s_1$ & $t_1$ & $t_5$ \\
		\hline
		$s_2$ & $t_1$ & $t_3$ \\
		\hline
		$s_3$ & $t_2$ & $t_9$ \\
		\hline
		$s_4$ & $t_1$ & $t_4$ \\
		\hline
	\end{tabular}
	\caption{An example of intra-coupled matcher ($m_1$)}
	\label{tab:intra}
\end{table}

\subsection{Inter-Coupled Matchers}
Two matchers are inter-coupled if they compute similar local scores for the same
pair of source and target concepts $(s,t)$. Two strongly inter-coupled matchers
will produce very similar columns in the $MIT$. For example given the $MIT$ from
Table~\ref{tab:inter}, the top scored pairs of concepts by $m_1$ are $(s_1,
t_1)$, $(s_2, t_2)$, $(s_3, t_1)$, $(s_4, t_6)$, $(s_5, t_2)$, very similar to
those evaluated by $m_2$: $m_1$ and $m_2$ are strongly inter-coupled.
Inter-coupled matchers can introduce a bias in the global scoring function.

\begin{table}[h!] 
	\centering
	\begin{tabular}{ |l|c|c|c| }
		\hline
		& $m_1$ & $m_2$ & $m_3$ \\
		\hline
		$s_1$ & $t_1$ & $t_1$ & $t_2$ \\
		\hline
		$s_2$ & $t_2$ & $t_1$ & $t_5$ \\
		\hline
		$s_3$ & $t_1$ & $t_6$ & $t_6$ \\
		\hline
		$s_4$ & $t_6$ & $t_6$ & $t_6$ \\
		\hline
		$s_5$ & $t_2$ & $t_6$ & $t_6$ \\
		\hline
	\end{tabular}
	\caption{An example of inter-coupled matchers ($m_1$ and $m_2$)}
	\label{tab:inter}
\end{table}

As for intra-coupled similarity, we compute inter-coupled similarity in terms of
co-occurrences of target concepts matched to source concepts by different
matchers. More formally:
\begin{definition}
The \emph{Inter-coupled Relative Similarity for Matchers} (IeRSM) between two matched target concepts 
$t$, $t'$ of matcher $m_{k}$ based on another matcher $m_{k'}$ is:
\begin{center}
$IeRSM_{k|k'}(t, t'~|~H_{k'})~=~\displaystyle\sum_{\bar{t} \in M_{k \rightarrow k'}^{t,t'}}~\min\{~{P_{k'|k}(\bar{t}~|~t),~P_{k'|k}(\bar{t}~|~t')}~\}$
\end{center}
where $M_{k \rightarrow k'}^{t,t'} = M_{k \rightarrow k'}^{t} \cap M_{k \rightarrow k'}^{t'}$
\end{definition}
\begin{definition}
The \emph{Inter-coupled Matching Similarity for Matchers} (IeMSM) between two matched target $t$, $t'$ concepts 
matched by a matcher $m_k$ is:
\begin{center}
$IeMSM_k(t, t'~|~\{H_{k'}\}_{k' \neq k})~=~\displaystyle\sum_{k' = 1, k' \neq k}^{L} \cfrac{IeRSM_{k|k'}(t, t'~|~H_{k'})}{L - 1}$
\end{center}
where $L$ is the total number of matchers, and $\{H_{k'}\}_{k' \neq k}$ contains all Matched Sets $H_{k'}$ for each matcher $m_{k'}$ other than $m_k$.
\end{definition}

For example, given the $MIT$ from Table~\ref{tab:inter}, we can compute the
inter-coupled similarity between two target concepts, namely $t_1$ and $t_6$,
according to matcher $m_2$. We aim at computing $IeMSM_2(t_1, t_6~|~\{H_1,
H_3\})$. Note that in this case $ \{ H_{k'}\}_{k' \neq 2} = \{ H_1, H_3 \}$.
Moreover $M_{2 \rightarrow 1}^{t_1, t_6} = \{t_1, t_2\}$, while $M_{2
\rightarrow 3}^{t_1,t_6} = \emptyset$, because $m_3$ never matches $t_1$ to any
source concept.
\begin{align*}
IeRSM_{2|1}(t_1, t_6 | H_1) &= \min\{P_{1|2}(t_1|t_1), P_{1|2}(t_1|t_6)\} + \min\{P_{1|2}(t_2|t_1), P_{1|2}(t_2|t_6)\}\\
&= \min\{\frac{1}{2}, \frac{1}{3}\} + \min\{\frac{1}{2}, \frac{1}{3}\}\\ &= \frac{2}{3}
\end{align*}
\begin{align*}
IeRSM_{2|3}(t_1, t_6 | H_3) = 0
\end{align*}
\begin{align*}
IeMSM_2(t_1, t_6~|~\{H_1, H_3\}) &= \cfrac{IeRSM_{2|1}(t_1, t_6 | H_1)}{2} + \cfrac{IeRSM_{2|3}(t_1, t_6 | H_3)}{2}\\
&= \frac{2}{3} \cdot \frac{1}{2} + 0 = \frac{1}{3}
\end{align*}

\subsection{Global Coupled Matchers}
$IaMSM$ captures the internal coupling of a matcher from the top scored target
concepts perspective by basically counting the frequency distribution of top
scored target concepts matched to source concepts. $IeMSM$ captures the coupling
of a matcher with other matchers from the same perspective (i.e. top scored
matched target concepts), basically by comparing the co-occurrence of target
concepts among different matchers. $IaMSM$ and $IeMSM$ are then combined together:
\begin{definition}
The \emph{Coupled Matching Similarity for Matchers} (CMSM) between target concepts 
$t$, $t'$ matched by matcher $m_k$ is:
\begin{center}
$CMSM_k(t, t'| \{H_{k'}\}_{k'=1}^L) = IaMSM_k(t,t') \cdot IeMSM_k(t,t'|\{H_{k'}\}_{k' \neq k})$
\end{center}
\end{definition}

$CMSM$ gives a combined view of coupled similarity between target concepts matched to
source concepts in terms of frequency and reciprocal dependency. Under the
hypothesis that coupled matchers will compute similar matchings we use $CMSM$ to
define a measure that aims at capturing the dissimilarity between matchers:
\begin{definition}
The \emph{Coupled Matcher Dissimilarity} (CMD) between two matchers $m_{k_1}$ and $m_{k_2}$ is:
\begin{align*}
MS_k(s_1, s_2) = CMSM_k(t^{k,s_1},  t^{k,s_2}|\{M_{k'}\}_{k' = 1}^{L})
\end{align*}
\begin{align*}
MD(m_{k_1}, m_{k_2}) = 1 - \displaystyle\sum_{s', s'' \in S} [ MS_{k_1}(s', s'') - MS_{k_2}(s', s'')]^2
\end{align*}
where $t^{k,s}$ is the top scored target concept matched to a source concept $s$ by the matcher $m_k$. 
\end{definition}

$CMD$ captures the dissimilarity between two matchers, and is used to compute to
capture the dissimilarity of a matcher between every other matcher:
\begin{definition}
The \emph{Global Coupled Matcher Dissimilarity} (GCMD) of a matcher $m_k$ is:
\begin{center}
$GCMD(m_k) = \displaystyle\sum_{\bar{k} = 1, \bar{k} \neq k}^{L} \cfrac{MD(m_k, m_{\bar{k}})}{L - 1}$
\end{center}
\end{definition}

\subsection{Computing the Final Alignment}
We compute the final alignment using the equation from
Definition~\ref{def:aligment} by integrating the global coupled matcher
dissimilarity into the global similarity score for concepts. We recall from
Section~\ref{sec:problem_statement} that the global similarity score aggregates
local similarity scores computed by each matcher $m$, by means of the convex
linear weighted function $score$. The weights in the function $score$ can be
computed using the global matcher dissimilarity concept defined before:
\begin{center}
$score(s, t) = \displaystyle\sum_{k = 1 \ldots L} GCMD(m_k) \cdot m_k(s, t)$
\end{center}

\section{Conclusion}
Matcher selection and weighting is a crucial task in Ontology Matching. Most of
state of art approaches match concepts from a source ontology to concepts from a
target ontolgy combining matchers by means of linear weighted functions.
However, the existing approaches do not consider coupling between matchers.
Hidden coupling between matchers can introduce biases in the process of matching
concepts from the source to the target ontology. This proposal is about
weighting matchers using a global coupled dissimilarity measure. This coupled
dissimilarity is computed considering matcher behavior, that is, considering the
matchings they produce. We propose to consider both the frequency of target
concepts matched to source concepts by a matcher (i.e. intra-coupling of
matchers) and the inter-dependency of target concepts matched by different
matchers (i.e. inter-coupling between matchers).

Our proposed approach looks promising from different perspectives. To the best
of our knowledge, there are no approaches to Ontology Matching that explicitely
consider matcher coupling. Moreover, we can easly evaluate the effectiveness of
the proposed approach using benchmarks from the Ontology Alignment Evaluation
Initiative, which have been widely used to evaluate every state of art techique
for Ontology Matching. Finally, the concept of coupled similarity can be applied
at different granularities: from matchers (as in this proposal) to concept
property values. The possible application of the proposed approach to different
granularities is the most promising perspective of this proposal.

\bibliographystyle{plain}
\bibliography{biblio}

\end{document}
