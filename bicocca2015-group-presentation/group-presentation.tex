\documentclass[xcolor={dvipsnames,table}]{beamer}
\usetheme{Unimib}
\usepackage{wrapfig}
\usepackage{floatflt}
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{array}
\usepackage{setspace}
\usepackage[percent]{overpic}
\usepackage{tabularx,booktabs}
\usepackage{multirow}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}

\author{Riccardo Porrini}
\institute{joint work with \textbf{Matteo Palmonari} and \textbf{Isabel F. Cruz}}
\title{Recovering the Role of Web Facets}
\runningtitle{Recovering the Role of Web Facets}
\runningauthors{Riccardo Porrini}
\date{}

% \setbeameroption{show only notes}
\begin{document}

\begin{frame}[plain]
	\includegraphics[width=11.99cm]{images/unimib.png}
\end{frame}

\begin{frame}[plain]
	\titlepage
\end{frame}

\begin{frame}{Facets on the Web}
	\begin{fancyblock}{}{1}
		\Blue{facet}: a clearly defined, mutually exclusive, and collectively
		exhaustive aspect, property, or characteristic of a class or specific subject~\cite{Ta04}
	\end{fancyblock}
	\begin{figure}
		\includegraphics[width=7.5cm]{images/facet.pdf}
	\end{figure}
	\begin{fancyblock}{}{1}
		a facet describes the \Blue{role} that facet \Blue{values} play in the characterization of entities from a specific \Blue{domain}
	\end{fancyblock}
\end{frame}

\begin{frame}{Facet Interpretation}
	\blue{facet extraction and integration}
	\begin{itemize}
	  \scriptsize
	  \item many works on facet extraction\\
	  \hfill \tiny (e.g., from web pages~\cite{Do11, Ko13}, taxonomies~\cite{CAISE}, \ldots)\scriptsize
	  \item focus on the extraction of facet values and their grouping\\
	  \hfill \tiny $\{2010, 2012, 2013, \ldots\}$, $\{USA, France, Italy, \ldots\}$  \scriptsize
	  \item semantics of the facet totally ignored, \Blue{no intepretation}\\
	  \hfill \tiny what does the facet $\{2010, 2012, 2013, \ldots\}$ represent? \scriptsize 
	\end{itemize}
	\vfill
	\blue{facet interpretation is needed}
	\begin{itemize}
	  \scriptsize
	  \item to improve the integration between different faceted classification schemes
	  \item to fully exploit the power of the classification schemes that they provide
	  \item web facets lack any explicit semantics, like web tables
	  \item facet interpretation by annotation with machine-readable semantics of the role it represents
	\end{itemize}
\end{frame}

\begin{frame}{Facet Annotation}
	\begin{figure}
		\includegraphics[width=9cm]{images/problem-statement.pdf}
	\end{figure}
	\vspace{1cm}
	\begin{overlayarea}{\textwidth}{0.6\textheight}
	\only<1>{
	\blue{intuitions \tiny{adapted from Web table annotation approaches}}
	\begin{itemize}
	  \scriptsize
	  \item \Blue{properties} from existing ontologies provide the semantics that we are looking for
	  \item consider the \emph{extensional semantics} of properties provided by a
	  \Blue{knowledge base}
	\end{itemize}
	}\only<2>{
	\blue{bonus points}
	\begin{itemize}
	  \scriptsize
	  \item machine readable semantics
	  \item facets published on the Web can be annotated with it (e.g., RDFa)
	\end{itemize}
	}\only<3-4>{
		\blue{challenge: ambiguity}
		\begin{columns}[c]
			\begin{column}{3cm}
				\textcolor{white}{foo}
			\end{column}
			\hspace{-2cm}
			\begin{column}{3cm}
				\only<3>{
					\Blue{wines}
				}\only<4>{
					\Blue{music albums}
				}
			\end{column}
			\hspace{-3cm}
			\begin{column}{5cm}
				\centering
				\only<2>{
					\textcolor{white}{any}
				}\only<3>{
					\Blue{vintage}
				}\only<4>{
					\Blue{release year}
				}\\
				2011\\
				2012\\
				2013\\
				\ldots
			\end{column}
		\end{columns}
	}
	\end{overlayarea}
\end{frame}

\begin{frame}{Problem Definition}
	\onslide<1-3>{
	\blue{knowledge base $\mathcal{KB}$}
	\begin{itemize}
	  \scriptsize
	  \item types $t$, categories $c$, entities $e$ and properties $p$
	  \item triples in the form $<s, p, o>$
	  \item $T(e)$ and $C(e)$ sets of types and categories of $e$
	  \item $L(t)$, $L(c)$, $L(e)$ sets of labels
	\end{itemize}
	\vfill
	}\onslide<2-3>{
	\blue{facet $\mathcal{F} = <d, r, F>$}
	\begin{itemize}
	  \scriptsize
	  \item $d$: domain (e.g., ``wines'')
	  \item $r$: role (e.g., ``vintage'') \hfill {\scriptsize * can be unknown or not meaningful}
	  \item $F$: values (e.g., $\{2010, 2011, 2012, \ldots\}$)
	\end{itemize}
	\vfill
	}\onslide<3>{
	\begin{cblock}{facet annotation problem}
		given a facet $\mathcal{F} = <d, r, F>$, find a ranked set of properties $P= \{ p_1, \ldots, p_n \}$ in $\mathcal{KB}$ such
		that their semantics is compatible with the semantics of the role $r$.
	\end{cblock}}
\end{frame}

\begin{frame}{Compatible Semantics}
	\begin{cblock}{main challenge}
		measuring the compatibility between the semantics of a property $p$\\ and the role $r$ of a facet $\mathcal{F} = <d, r, F>$ 
	\end{cblock}
	\vspace{0.5cm}
	\begin{overlayarea}{\textwidth}{0.8\textheight}
	\only<2>{
		\Blue{1. (weighted) frequency}\\
		\vspace{0.25cm}
		\scriptsize
		$p$ \Blue{frequently} holds between entities whose types and/or categories \Blue{are similar} to $d$ and facet values\\
		\begin{figure}
			\includegraphics[width=9cm]{images/weighted-frequency.pdf}
		\end{figure}
	}
	\only<3>{
		\Blue{2. coverage}\\
		\vspace{0.25cm}
		\scriptsize
		$p$ holds between entities whose types and/or categories matches $d$ and \Blue{many different} facet values (the more, the better)\\
		\begin{figure}
			\includegraphics[width=10cm]{images/value-coverage.pdf}
		\end{figure}
	}
	\only<4>{
		\Blue{3. specificity}\\
		\vspace{0.25cm}
		\scriptsize
		$p$ characterizes entities of types and categories that match $d$ \Blue{more} than entities of other types and categories\\
		\begin{figure}
			\includegraphics[width=9cm]{images/property-specificity.pdf}
		\end{figure}
	}
	\only<5>{
		\Blue{overall score}\\
		\vfill
		\begin{center}
			\blue{frequency} $\cdot$ \blue{coverage} $\cdot$ \blue{specificity}\\
			\begin{equation*}
				\begin{split}
					pfd(p, \mathcal{F}) = & \ln\Big(wfreq(p, \mathcal{F}) + 1 + \epsilon\Big)~\cdot\\
					& cov(p, \mathcal{F})~\cdot\\
					& \ln\Big(spec(p, \mathcal{F}) + 1 + \epsilon\Big)~\cdot\\
				\end{split}
			\end{equation*}
		\end{center}
	}
	\end{overlayarea}
\end{frame}

\begin{frame}{Knowledge Base Indexing}
	\includegraphics[width=11cm]{images/kb-indexing.pdf}
	\vfill
	\begin{itemize}
	  \scriptsize
	  \item selection of candidate properties via domain and facet value matching
	  \item ranking of the properties according to frequency, coverage and specificity
	\end{itemize}
\end{frame}

\begin{frame}{Sample Rankings}
	\begin{columns}[c]
		\begin{column}{2cm}
			\only<1>{
				\Blue{wines}
			}\only<2>{
				\Blue{books}
			}
		\end{column}
		\begin{column}{7cm}
			\only<1>{
				\scriptsize
				\url{http://dbpedia.org/property/year}\\
				\url{http://dbpedia.org/property/wineYears}\\
				\url{http://dbpedia.org/property/populationAsOf}\\
				\url{http://dbpedia.org/property/released}\\
				\url{http://dbpedia.org/ontology/releaseDate}\\
				\ldots
			}\only<2>{
				\scriptsize
				\url{http://dbpedia.org/property/releaseDate}\\
				\url{http://dbpedia.org/property/pubDate}\\
				\url{http://dbpedia.org/ontology/lcc}\\
				\url{http://dbpedia.org/property/congress}\\
				\url{http://dbpedia.org/ontology/publicationDate}\\
				\ldots
			}
		\end{column}
		\begin{column}{2cm}
			\centering
			\only<1>{
			2011\\
			2010\\
			2009\\
			2008\\
			2013\\
			2012\\
			2006\\
			2005\\
			2003\\
			2002\\
			\ldots
			}\only<2>{
			1754\\ 
			1859\\ 
			1869\\ 
			1877\\ 
			1881\\ 
			1887\\ 
			1892\\ 
			1896\\ 
			1902\\ 
			1908\\ 
			1932\\ 
			\ldots
			}
		\end{column}
	\end{columns}
\end{frame}

\newenvironment{smalltable}{
	\begin{table}[h!]
		\centering
		\scriptsize
}{
	\end{table}
}

\begin{frame}{Evaluation}
	\begin{cblock}{goal}
		\tiny
		show that we effectively measure the compatibility between the semantics of properties and facet roles, compared to state-of-the-art approaches for similar tasks (i.e., web table annotation)
	\end{cblock}
	\vspace{0.5cm}
	\begin{overlayarea}{\textwidth}{0.8\textheight}
	\only<2>{
	\Blue{knowledge bases}
	\begin{smalltable}
		\begin{tabularx}{\columnwidth}{@{}l *4{>{\centering\arraybackslash}X}@{}}
		\toprule
		& \textbf{types + categories} & \textbf{properties} & \textbf{triples} (millions) \\
		\midrule 
		\textbf{DBPedia*} & 753958 & 53195 & $\sim$ 96.8 \\
		\textbf{YAGO1} & 184512 & 89 & $\sim$ 5.5 \\
		\bottomrule
		\end{tabularx}
	\end{smalltable}
	{\scriptsize * \url{http://dbpedia.org/ontology} and \url{http://dbpedia.org/property} namespaces}
	}\only<3>{
	\Blue{gold standards}
	\begin{smalltable}
		\begin{tabularx}{\columnwidth}{@{}r *4{>{\centering\arraybackslash}X}@{}}
		\toprule
		& \multicolumn{2}{c}{\emph{facets}} & \multicolumn{2}{c}{\emph{properties}}\\ 
		& \textbf{\#} & \textbf{domains} & \textbf{correct} & \textbf{fair}\\
		\midrule
		\textbf{dbpedia-numbers} & 8 & 7 & $\sim$4 & $\sim$19\\
		\textbf{dbpedia-literals} & 31 & 13 & $\sim$7 & $\sim$7\\
		\textbf{dbpedia} & 39 & 13 & $\sim$3 & $\sim$9\\ 
		\midrule
		\textbf{yago-full}* & 83 & 17 & 1 & -\\
		\textbf{yago-abstract}* & 83 & 10 & 1 & -\\
		\bottomrule
		\end{tabularx}
	\end{smalltable}
	{\scriptsize * \textbf{yago-full} and \textbf{yago-abstract} adapted from \cite{Li10}}
	}\only<4>{
		\Blue{gold standards}
		\begin{smalltable}
			\begin{tabularx}{\columnwidth}{@{}r *3{>{\centering\arraybackslash}X}@{}}
			\toprule
			& \textbf{domain} & \textbf{values} & \textbf{properties}\\
			\midrule
			\textbf{dbpedia-numbers} & music albums & 1967, 1969, 1971, \ldots, 2000 & \emph{relYear}, \emph{year}, \emph{releaseDate} \ldots \\
			\textbf{dbpedia-entities} & wines & USA, Italy, \ldots, Slovenia & \emph{wineRegion}, \emph{origin}, \ldots\\
			\midrule
			\textbf{yago-full} & wordnet-actor-109765278 & 1776, California, \ldots, Wilson & \emph{actedIn}\\
			\textbf{yago-abstract} & countries & Abu Dhabi, Accra, \ldots, Zagreb & \emph{hasCapital}\\
			\bottomrule
			\end{tabularx}
		\end{smalltable}
	}\only<5>{
	\Blue{baselines}
	\vspace{0.5cm}
	\begin{itemize}
	  \scriptsize
	  \item majority (only frequency, boolean domain similarity)
	  \item maximum likelihood model from \cite{Ve11} (table annotation literature, most similar approach)
	\end{itemize}
	}\only<6>{
		\Blue{results} \\ 
		{\scriptsize mean average precision}
		\vspace{-0.4cm}
		\begin{smalltable}
			\scriptsize
			\begin{tabularx}{\columnwidth}{@{}l *3{>{\centering\arraybackslash}X}@{}} 
				\toprule
				& \textbf{dbpedia-numbers} & \textbf{dbpedia-entities} & \textbf{dbpedia} \\
				\midrule 
				\emph{majority} & 0.22 & 0.50 & 0.44 \\
				\emph{maximum likelihood} & 0.16 & 0.40 & 0.36 \\
				\emph{pfd} & \textbf{0.30*} & \textbf{0.52} & \textbf{0.47*} \\
				\bottomrule
			\end{tabularx}
		\end{smalltable}
		\vspace{-0.4cm}
		{\scriptsize mean reciprocal rank}
		\vspace{-0.4cm}
		\begin{smalltable}
			\scriptsize
			\begin{tabularx}{\columnwidth}{@{}l *2{>{\centering\arraybackslash}X}@{}} 
				\toprule
				& \textbf{yago-full} & \textbf{yago-abstract} \\
				\midrule 
				\emph{majority} & 0.73 & 0.83 \\
				\emph{maximum likelihood} & 0.82 & 0.86 \\
				\emph{pfd} & \textbf{0.87*} & \textbf{0.90*} \\
				\bottomrule
			\end{tabularx}
		\end{smalltable}
		\vspace{-0.4cm}
		{\scriptsize * statistically significant improvements (paired two-tailed t-test $p < 0.05$)}
	}\end{overlayarea}
\end{frame}

\begin{frame}{Open Points}
	\begin{itemize}[<+->]
	  \item improve the semantic compatibility measurement\\
	  \hfill{\scriptsize capture how ``consistent'' are the types of property values?}
	  \vspace{1cm}
	  \item more principled aggregation of the local scores\\
	  \hfill{\scriptsize why not seeing the problem as a \blue{rank aggregation} problem?}
	\end{itemize}
\end{frame}

\begin{frame}{Thank You}
	\begin{mdframed}[rightmargin=-1.2cm,
    				roundcorner=3pt,
    				align=right,
    				userdefinedwidth=6cm,
    				linecolor=lightgrey,
    				backgroundcolor=lightgrey,
    				innerbottommargin=10pt,
    				innertopmargin=10pt]
		{\Huge \textbf{\blue{Questions?}}} \hfill
	\end{mdframed}
\end{frame}

\begin{frame}{References}
	\begin{thebibliography}{}
		\smartbib{Taylor 2004}{Ta04}{A.G. Taylor}{Wynar’s introduction to cataloging and classification}{Libraries Unlimited}{2004}
		\smartbib{Carmel et al. 2009}{Ca09}{D. Carmel, H. Roitman and N. Zwerdling}{Enhancing Cluster Labelling Using Wikipedia}{SIGIR}{2009}
		\smartbib{Limaye et al. 2010}{Li10}{G. Limaye, S. Sarawagy and S. Chakrabarti}{Annotating and Searching Web Tables Using Entities, Types and Relationship}{VLDB}{2010}
		\smartbib{Venetis et al. 2011}{Ve11}{P. Venetis, A. Halevy, J. Madhavan, M. Pasca, W. Shen, F Wu, G. Miao and C. Wu}{Recovering Semantics of Tables on the Web}{VLDB}{2011}
		\smartbib{Dou et al. 2011}{Do11}{Z. Dou, S. Hu, Y. Luo, R. Song and J.R. Wen}{Finding dimensions for queries}{CIKM}{2011}
		\smartbib{Kong and Allan 2013}{Ko13}{W. Kong and J. Allan}{Extracting query facets from search results}{SIGIR}{2013}
		\smartbib{Porrini et al. 2014}{CAISE}{R. Porrini, M. Palmonari and C. Batini}{Extracting Facets from Lost Fine-Grained Classifications in Dataspaces}{CAiSE}{2014}
	\end{thebibliography}
\end{frame}

\begin{frame}
	\begin{mdframed}[rightmargin=-1.2cm,
    				roundcorner=3pt,
    				align=right,
    				userdefinedwidth=6cm,
    				linecolor=white,
    				backgroundcolor=white,
    				innerbottommargin=10pt,
    				innertopmargin=10pt]
		{\Huge \textbf{\blue{Backup}}} \hfill
	\end{mdframed}
\end{frame}

\begin{frame}{Related Work}
	\begin{table}
		\scriptsize
		\begin{tabular}{|l|l|l|l|}
		\hline
		HammerSky 2010 Red Handed & Red & \textbf{2010} & U.S.A. \\
		2009 Tignanello, Tuscany 750 mL & Red & \textbf{2009} & Italy \\
		2012 Paulinshof Urstueck 750 mL & White & \textbf{2012} & Germany \\
		2012 Sobremesa Vineyards VRM & White & \textbf{2012} & Argentina \\ 
		\hline
		\end{tabular}
	\end{table}
	\blue{property annotation in web tables}
	\begin{itemize}
	  \scriptsize
	  \item maximum likelihood model~\cite{Ve11}
	  \item custom, probabilistic knowledge bases~\cite{Wa12}
	  \item use of contextual, outside table information~\cite{Zh15}
	  \item holistic approaches (jointly annotate all table elements) based on joint inference~\cite{Li10, Mu13}
	\end{itemize}
	\blue{however}
	\begin{itemize}
	  \scriptsize
	  \item facets are less structured than tables
	  \item all approaches \Blue{all} rely on the quasi-relational structure of the table\\
	  \hfill{\scriptsize  (i.e., co-occurrences on the same row)}
	  \item still, we borrow solid principles from them\\
	  \hfill{\scriptsize (e.g, coverage)}
	  \item we investigate if established approaches can be applied to our problem
	\end{itemize}
\end{frame}

\begin{frame}{Additional References}
	\begin{thebibliography}{}
		\smartbib{Wang et al. 2012}{Wa12}{J. Wang, H. Wang, Z. Wang, and K. Q. Zhu}{Understanding tables on the web}{ER}{2012}
		\smartbib{Mulwad et al. 2013}{Mu13}{V. Mulwad, T. Finin, and A. Joshi}{Semantic message passing for generating linked data from tables}{ISWC}{2013}
		\smartbib{Zhang 2015}{Zh15}{Z. Zhang}{Start small, build complete: Effective and efficient semantic table interpretation using tableminer}{Sem. Web J. - under transparent review}{2015}
	\end{thebibliography}
\end{frame}

\end{document}