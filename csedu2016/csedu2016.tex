\documentclass[a4paper,twoside]{article}

\usepackage{subfigure}
\usepackage{calc}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{pslatex}
\usepackage{apalike}
\usepackage{tabularx,booktabs}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage{SCITEPRESS}     % Please add other packages that you may need BEFORE the SCITEPRESS.sty package.

\subfigtopskip=0pt
\subfigcapskip=0pt
\subfigbottomskip=0pt

\begin{document}

\title{Evaluation of Requirements Collection Strategies for a Constraint-based
Recommender System in a Social e-Learning Platform}

\author{
\authorname{Francesco Epifania\sup{1}\sup{2}\sup{3} and Riccardo Porrini\sup{3}}
\affiliation{\sup{1}Department of Computer Science, University of Milano, Via
Comelico 39, Milano, Italy}
\affiliation{\sup{2}Social Things s.r.l., Via De Rolandi 1, Milano, Italy}
\affiliation{\sup{3}Department of Informatics, Systems and Communication,
University of Milano-Bicocca, Viale Sarca 336, Milano, Italy}
\email{francesco.epifania@di.unimi.it, riccardo.porrini@disco.unimib.it}
}

\keywords{Recommender System, Learning Resources, Social Network, e-Learning,
User-centric Evaluation.}

\abstract{The NETT Recommender System (NETT-RS) is a constraint-based recommender system that recommends
learning resources to teachers who want to design courses. As for many state-of-the-art constraint-based
recommender systems, the NETT-RS bases its recommendation process on the collection of requirements to
which items must adhere in order to be recommended. In this paper we study the effects of two different
requirement collection strategies on the perceived overall recommendation quality of the NETT-RS. In the
first strategy users are not allowed to refine and change the requirements once chosen, while in the second
strategy the system allows the users to modify the requirements (we refer to this strategy as backtracking). We
run the study following the well established ResQue methodology for user-centric evaluation of RS. Our
experimental results indicate that backtracking has a strong positive impact on the perceived recommendation
quality of the NETT-RS.}

\onecolumn \maketitle \normalsize \vfill

\section{\uppercase{Introduction}}\label{sec:introduction}

Recommender Systems (RSs) are information filtering algorithms that generate
meaningful recommendations to a set of users over a collection of items that
might be of their interest~\cite{rs-introduction}. In its basic incarnation, a
RS takes in input a user profile and possibly some situational context and
computes a ranking over a collection of recommendable
items~\cite{AdomaviciusT05}. The user profile can possibly include explicit
information, such as feedback or ratings of items and/or implicit information,
such as items visited and time spent on them. RSs leverage this information to
predict the relevance score for a given, typically unseen, item.

RS have been adopted in many disparate fields, ranging from movies, music,
books, to financial services and live insurances~\cite{rs-introduction}. In the
e- Learning context, the NETT Recommender System (NETT-RS)~\cite{NETT} is a RS
that recommends learning resources (e.g., slides, tutorials, papers etc.) to
teachers who want to design a course. The NETT-RS is a component of the NETT
platform, one of the main outcomes of the NETT European project.

The NETT project aims at gathering a networked social community of teachers to
improve the entrepreneurship teaching in the European educational system. Among
the other things, the platform allows teachers to design courses. The NETT-RS
supports the teachers in the design of courses by recommending adequate and high
quality learning resources (resources, for brevity). In order to finalize the
design, teachers go through three sequential steps: they specify (1) a set of
rules and (2) keywords for a course (e.g., required skill = statistics) and (3)
the system recommends a set of resources such that they fit the rules and the
keywords specified by the teacher (e.g., no differential calculus for a basic
math course) and have an high rating.

The characteristics of a NETT-RS closely match the ones proper of
constraint-based RS~\cite{rs-handbook} as the teacher specifies a set of
requirements (in the form of rules and keywords) to which resources must adhere
in order to be recommended. The multi-phased process allows the teacher to
incrementally explore the resource space in order to find the most suitable ones
for her/his course, in the vein of conversational RS~\cite{PuFCZV11,ChenP12}.
However, this interaction with the user required by the NETT-RS entails several
challenges. The teacher must be put within an interactive loop with the system,
with the possibility to revise the rules and keywords previously specified. We
refer to this feature as backtracking.

In this paper, we study the effect of the backtracking feature on the NETT-RS.
We argue that providing a backtracking feature to the NETT-RS strongly
influences the perceived recommendation quality. In order to answer this
research question, we set up a user-centric evaluation of the NETT-RS following
the ResQue methodology~\cite{resque}. We compare two versions of the NETT-RS
(with and without backtracking) over many different user- centric quality
dimensions. Evidence gathered from this study substantiates our intuitions: the
presence of backtracking has a strong impact on many different quality measures,
such as control, perceived ease of use and overall satisfaction.

The reminder of the paper is organized as follows. In Section~\ref{sec:2} we
sketch the main components of the NETT-RS. In Section~\ref{sec:3} we describe
the user study that we conducted and discuss the results. We compare the NETT-RS
with related work in Section~\ref{sec:4} and end the paper with conclusions and
highlight future work in Section~\ref{sec:5}.

\section{\uppercase{The Recommender System}}\label{sec:2}

\begin{table*}[t]
\caption{An excerpt of metadata that characterize a resource.}
\label{table:metadata}
\scriptsize
\begin{tabularx}{\textwidth}{ l l X }
	\toprule
	\midrule
	\textbf{Metadata} & \textbf{Type} & \textbf{Values}\\
	\midrule
	\textit{Learning Resource Type} & qualitative & Diagram, Figure, Graph, Index,
	Slides, Table, Narrative Text, Lecture, Exercise, Simulation, Questionnaire, Exam, Experiment, Problem Statement, Self Assessment\\
	\midrule
	\textit{Format} & qualitative & Video, Images, Slide, Text, Audio\\
	\midrule
	\textit{Language} & qualitative & English, Italian, Bulgarian, Turkish\\
	\midrule
	\textit{Keywords} & qualitative & entrepreneurship, negotiation, \ldots\\
	\midrule
	\textit{Typical Learning Time} & quantitative & 30 minutes, 60 minutes, 90
	minutes, +120 minutes\\
	\midrule
	\bottomrule
\end{tabularx}
\end{table*}

The NETT-RS recommendation process consists of three sequential steps: rule
induction, keyword extraction and resource selection. In the rest of the Section
we describe how items (i.e., learning resources) are represented within the
NETT- RS, along with a sketch of the three phases. We also highlight one of the
main issues that underlies this multi-step process: the need for backtracking.

\subsection{Learning Resources}

Learning resource (resource for brevity), which are suggested by using a set of
metadata that adhere to the Learning Object Metadata standard (LOM).
These metadata characterize resources in terms of, for example, format (e.g.,
text, slide etc.) or language (e.g., Italian, English etc.). More formally,
resources are characterized by a fixed set of $n$ metadata $\mu_1, \ldots,
\mu_n$, which can be qualitative (nominal/ordinal) or quantitative
(continuous/discrete), where the latter are suitably normalized in $[0, 1]$.
Table~\ref{table:metadata} presents some example metadata used within the
NETT-RS. The resources are also characterized by a particular metadata: the
keywords. The keywords ideally describe the topics that a resource is about.
Each resource has a textual content $\pi$ (e.g., the text extracted from a
slide). Each resource is affected by a rating p, typically normalized in $[0,
1]$.

\begin{figure}[t]
	\centering
	\includegraphics[width=1\columnwidth]{images/rules.png}
	\caption{The rule selection step.}
	\label{fig:rules}
\end{figure}

\subsection{Rule Induction}

As a first step, the teacher is asked to select a set of constraints (i.e.,
\emph{rules}) over the learning metadata. Those rules are computed automatically
by the system leveraging a well known rule induction algorithm, as explained
later on in this Section. An example of rules selection is depicted in
Figure~\ref{fig:rules}. Rules, which should in principle accurately describe the
resources available in the system, are encoded as Horn clauses made of some
antecedents and one consequent. The consequent is fixed: ``$\pi$ is good''
(i.e., the content of a resource is good). The antecedents are Boolean
conditions $c_j$ (true/false) concerning sentences of two kinds: (1)
``$\mu_i~\frac{<}{>}~\theta$'', where $\theta$ stands for any symbolic (for
nominal metadata) or numeric constant (for quantitative variables) and (2)
``$\mu_i \in A$'', with $A$ a suitable set of constants associated with
qualitative metadata. A rule is hence formally defined as
$c_1,~\ldots,~c_k~\rightarrow~\pi~\text{is good}$.

We may obtain these rules starting from one of the many algorithms generating
decision trees dividing good from bad items, where the difference between the
various methods stands in the entropic criteria and the stopping rules adopted
to obtain a tree, and in the further pruning heuristics used to derive rules
that are limited in number, short in length (number of antecedents), and
efficient as for classification errors.
In articular we use RIPPERk, a variant of the Incremental Reduced Error Pruning
(IREP) proposed by Cohen~\cite{rule-induction} to reduce the error rate,
guaranteeing in the meanwhile a high efficiency on large samples, and in
particular its Java version JRip available in the WEKA environment~\cite{weka}.
This choice was mainly addressed by computational complexity reasons, as we move
from the cubic complexity in the number of items of the well known
C4.5~\cite{Quinlan93} to the linear complexity of JRip. Rather, the
distinguishing feature of our method is the use of these rules: not to exploit
the classification results, rather to be used as hyper- metadata of the
questioned items. In our favorite application field, the user, in search of
didactic material for assembling a course on a given topic, will face rules like
those reported in Table~\ref{table:rules}. Then, it is up to her/him to decide
which rules characterize the material s/he's searching for.

\begin{table}[t]
	\caption{A set of two candidate rules.}
	\label{table:rules}
	\scriptsize
	\begin{tabularx}{\columnwidth}{ l  X }
	\toprule
	\midrule
	\textbf{Id} & \textbf{Rule} \\
	\midrule
	$R_1$ & \textit{skill\_required} Communication Skill in Marketing Information
	Management = low \textbf{and} \textit{language} = Italian $\Rightarrow$
	\textit{good\_course} \\
	\midrule
	$R_2$ & \textit{skill\_acquired} Communication Skill in Marketing Information
	Management = medium-high \textbf{and} \textit{skill\_acquired} Communication
	Skill in Communications Basic = high \textbf{and} \textit{age} =
	teenager-adult $\Rightarrow$ \textit{good\_course} \\
	\midrule
	\bottomrule
	\end{tabularx}
\end{table}

\subsection{Keywords Extraction and Resource Selection}

As a second step, the system presents the teacher with a subset of the keywords
extracted from the metadata of resources that satisfy the rules selected during
the rule induction phase. An example of keywords is depicted in
Figure~\ref{fig:keywords}.

\begin{figure}[t]
	\centering
	\includegraphics[width=1\columnwidth]{images/keywords.png}
	\caption{The keywords selection step.}
	\label{fig:keywords}
\end{figure}

In fact, even after applying the filtering capability provided by the selected
rules, the number of resources that are to be suggested can still be very high.
Thus, a meaningful subset of the keywords is presented to the teacher. The
NETT-RS looks for the best subset of keywords in terms of the ones providing the
highest entropy partition of the resource set selected by the rules Figure 4.
With this strategy, the number of selected resources is guaranteed to reduce
uniformly at an exponential rate for whatever keyword subset chosen by the
teacher.

As the final step, the NETT-RS recommends a set the resources such that: (1)
they satisfy the rules and (2) they are annotated with the selected keywords.
The teacher then finalizes the design of the course by selecting the resources
considered suitable. An example of suggested resources is depicted in
Figure~\ref{fig:resources}.

\begin{figure}[t]
	\centering
	\includegraphics[width=1\columnwidth]{images/resources.png}
	\caption{The resource selection step.}
	\label{fig:resources}
\end{figure}

\section{\uppercase{User Experience Evaluation of the Recommender
System}}\label{sec:3}

\subsection{The Backtracking Feature}

The NETT-RS requires the teacher to go through all the three steps described
above in order to finalize the design of a course. During each step the system
provides the teacher with a set of automatically selected items, namely: rules,
keywords or resources. The strong assumption we make on such a process is that
the choices made by the teacher in one phase can potentially affect the result
of the subsequent phases. For this reason we argue that allowing the teacher to
go back and forth the phases, and possibly revising the selections, has a strong
impact on the perceived quality of the resource suggestion in the final step
(Figure~\ref{fig:resources}). The need of such a backtracking feature was
furthermore observed by alpha testers of the NETT-RS, which initially were not
equipped with such feature.

\subsection{Evaluating the Backtracking Feature}

From the user interaction point of view we argue that the backtracking feature
has a high impact on the overall perceived quality of the NETT-RS. We
substantiate this claim with empirical evidence gathered from a user-centric
evaluation of the NETT- RS. The remainder of this Section describes the
experiment we conducted, starting from the research question and hypotheses, the
experimental setting, and ending with the discussion of the experimental
results.

\subsection{Research Question and Hypotheses}

Our research question is rather simple and
pragmatic: Does providing a backtracking feature to
teachers affect the perceived quality of the
recommendation of the NETT System?

In order to provide an answer to this research question, we evaluate the NETT-RS
and formulate the two following hypotheses:
\begin{description}
\item[H1:] the possibility to revise the choices made during the course design
process increases the perceived user control over the NETT-RS.
\item[H2:] the possibility to revise the choices made during the course design
process increases the perceived overall quality of the NETT-RS.
\end{description}
The hypothesis \textbf{H1} focuses on a specific quality of
the NETT-RS (i.e., the user control over the recommendation process), which is
only one of the possible dimensions that contribute to the perceived
overall quality of system (\textbf{H2}).

\subsection{Experimental Design}

Two versions of the NETT-RS were evaluated:
the first one without the backtracking feature enabled (i.e., NETT-RS) and the
second one with backtracking (i.e., NETT-RS-b). As for testing our hypotheses,
we adopted the ResQue methodology~\cite{resque}, which is a well-established
technique for the user-centric evaluation of RSs. We selected 40 participants,
mainly university professors, and asked them to design a course on Probability
and Statistics, choosing from 1170 different learning resources. We selected
such resources from the MIT Open CourseWare website. The participants were
equally partitioned into two disjoint subsets (20 + 20). Participants from the
first subset were asked to design a course using NETT-RS, while participants
from the second subset used NETT-RS-b. Finally, participants were presented with
a questionnaire (Table~\ref{table:questions})

\begin{table*}[t] 
\centering 
\caption{The adapted version of the ResQue questionnaire used in our study.}
\label{table:questions}
\scriptsize
\begin{tabularx}{\textwidth}{l l X}
	\toprule
	\midrule
	& \textbf{Quality} & \textbf{Question} \\
	\midrule
	\textit{Q1} & \textit{recommendation accuracy} & The teaching material
	recommended to me match my interests \\\midrule
	\textit{Q2} & \textit{recommendation novelty} & The recommender
	system helped me discover new teaching material \\\midrule
	\textit{Q3} & \textit{recommendation diversity} & The items
	recommended to me show a great variety of options \\\midrule
	\textit{Q4} & \textit{interface adequacy} & The layout and labels of
	the recommender interface are adequate \\\midrule
	\textit{Q5} & \textit{explanation} & The recommender explains why the
	single teaching materials are recommended to me \\\midrule
	\textit{Q6} & \textit{information sufficiency} & The information
	provided for the recommended teaching material is sufficient for me to take
	a decision\\\midrule
	\textit{Q7} & \textit{interaction adequacy} & I found it easy to tell
	the system what I like/dislike \\\midrule
	\textit{Q8} & \textit{perceived ease of use} & I became familiar with
	the recommender system very quickly \\\midrule
	\textit{Q9} & \textit{control} & I feel in control of modifying
	my requests \\\midrule
	\textit{Q10} & \textit{transparency} & I understood why the learning material
	was recommended to me \\\midrule
	\textit{Q11} & \textit{perceived usefulness} & The recommender helped
	me find the ideal learning material \\\midrule
	\textit{Q12} & \textit{overall satisfaction} & Overall, I am satisfied
	with the recommender \\\midrule
	\textit{Q13} & \textit{confidence and trust} & The recommender can be
	trusted \\\midrule
	\textit{Q14} & \textit{use intentions} & I will use this recommender
	again \\\midrule
	\textit{Q15} & \textit{purchase intention} & I would adopt the learning
	materials recommended, given the opportunity \\\midrule
	\bottomrule
\end{tabularx}
\end{table*}

\subsection{The Adapted ResQue Questionnaire}

The ResQue questionnaire~\cite{resque} defines a wide set of user-centric
quality metrics to evaluate the perceived qualities of RSs and to predict users'
behavioral intentions as a result of these evaluations. The original version of
the questionnaire included 43 questions, evaluating 15 different qualities, such
as recommendation accuracy or control. Participants' responses to each question
are characterized by using a 5-point Likert scale from strongly disagree (1) to
strongly agree (5). Two versions of the questionnaire have been proposed (Pu, et
al., 2011): a longer version (43 questions) and a shorter version (15
questions). In our study we adopted the short version in order to reduce the
cognitive load required to participants. A modified version of the
questionnaire, tailored for a system that recommends learning resources, was
presented to the participants (Table~\ref{table:questions}).

\subsection{Experimental Results and Discussion}

Table~\ref{table:results} reports the mean grades for all the issued questions.
We got a Cronbach's $\alpha$~\cite{cronbach} equal to 0.919 and 0.887 for grades
given by participants who evaluated the NETT-RS and the NETT-RS-b, respectively.
Thus, we consider the questioned participants to be reliable. NETT-RS-b achieves
the most noticeable result on the control quality (Q9) showing that the presence
of the backtracking lifts the mean judgment up from 1.30 to 4.45 (342\% of
improvement). The difference is significant with a $p$-value $< 0.0001$,
providing strong experimental evidence for the hypothesis H1: the possibility to
revise the choices made during the course design process increases the perceived
user control over the NETT-RS. As far as the overall quality is concerned
(hypothesis H2) we observe strong significant improvements ($p < 0.0001$) in the
perceived ease of use, perceived usefulness, overall satisfaction, confidence
and trust, use intentions and purchase intention qualities. This evidence allows
us to correlate the presence of the backtracking feature with a higher perceived
overall quality of the NETT-RS in terms of the above features.

The presence of the backtracking feature does not lead to a significant
improvement of the recommendation accuracy. However, we observe significant
improvements ($p \approx 0.012$ and $p \approx 0.002$) on recommendation novelty
and recommendation diversity. Our interpretation is that enabling the users to
go back and forth the steps allows them to better explore the resource space,
thus leading to novel and diverse recommendations.

Finally, we observe that the presence of the backtracking feature has no
significant impact on the interface adequacy, explanation and transparency
qualities. We furthermore observe that participants assigned a relatively low
grade, especially for the interface adequacy. Such results may come from the
difficulty to understand the meaning of rules presented by the NETT-RS. We
consider it as a stimulus for a future improvement of the system.

\begin{table}[t] 
\centering 
\caption{Mean grades to questionnaire's questions.
$p$-values are computed by means of a two-tailed t-test. Statistically
significant improvements are marked in bold.} 
\scriptsize
\begin{tabularx}{\columnwidth}{@{}c c *3{>{\centering\arraybackslash}X}@{}}
	\toprule
	\midrule
	& \textbf{Quality} & \textbf{NETT-RS} & \textbf{NETT-RS-b} & $p$-value\\
	\midrule
	\textit{Q1} & \textit{recommendation accuracy} & 3.80 & 3.95 & 0.481\\
	\textit{Q2} & \textit{recommendation novelty} & 3.50 & \textbf{4.05} & 0.012 \\
	\textit{Q3} & \textit{recommendation diversity} & 3.50 & \textbf{4.10} & 0.002
	\\
	\textit{Q4} & \textit{interface adequacy} & 2.90 & 3.30 & 0.088 \\
	\textit{Q5} & \textit{explanation} & 3.40 & 3.60 & 0.162\\
	\textit{Q6} & \textit{information sufficiency} & 3.35 & \textbf{4.25} & $<$
	0.0006\\
	\textit{Q7} & \textit{interaction adequacy} & 3.10 & \textbf{3.60} & $<$
	0.002\\
	\textit{Q8} & \textit{perceived ease of use} & 3.45 & \textbf{4.60} & $<$
	0.0001\\
	\textit{Q9} & \textit{control} & 1.30 & \textbf{4.45} & $<$ 0.0001\\
	\textit{Q10} & \textit{transparency} &  3.45 & 3.75 & 0.110\\
	\textit{Q11} & \textit{perceived usefulness} & 3.00 & \textbf{4.00} & $<$
	0.0004\\
	\textit{Q12} & \textit{overall satisfaction} & 2.80 & \textbf{3.90} & $<$
	0.0001\\
	\textit{Q13} & \textit{confidence and trust} & 3.15 & \textbf{3.80} & $<$
	0.001\\
	\textit{Q14} & \textit{use intentions} & 2.70 & \textbf{3.70} & $<$ 0.0001\\
	\textit{Q15} & \textit{purchase intention} & 3.30 & \textbf{4.10} & $<$ 0.0001\\
	\midrule
	\bottomrule
\end{tabularx}
\label{table:results}
\end{table}

\section{\uppercase{Related Work}}\label{sec:4}

A widely accepted classification of RSs divides them into four main
families~\cite{rs-introduction}: \emph{content-based} (CB), \emph{collaborative
filtering} (CF), \emph{knowledge-based} (KB) and \emph{hybrid}. The basic idea
behind CB RSs is to recommend items that are similar to those that the user
liked in the past (see e.g., \cite{BalabanovicS97,PazzaniB97,Mooney2000}). CF RSs
recommend items based on the past ratings of all users collectively (see e.g.,
\cite{Resnick1994,Sarwar2001,LemireM05}). KB RSs suggest items based on
inferences about users' needs: domain knowledge is modeled and leveraged during
the recommendation process (see
e.g.,~\cite{Burke2000,Felfernig2008,FelfernigK05}). Hybrid RSs usually combine
two or more recommendation strategies together in order to leverage the
strengths of them in a principled way (see
e.g.,~\cite{deCampos2010,Shinde2012,RenHGXW08}).

The NETT-RS falls into the KB RSs family, and more precisely into the
\emph{constraint-based} category. For a more exhaustive and complete description
of constraint-based RSs we point the reader to~\cite{rs-handbook}. The typical
features of such RSs are: (1) the presence of a \emph{knowledge base} which
models both the items to be recommended and the explicit rules about how to
relate user requirements to items, (2) the collection of user requirements, (3)
the repairment of possibly inconsistent requirements, and (4) the explanation of
recommendation results.

We recall from Section~\ref{sec:2} that learning resources in the NETT-RS are
characterized by metadata. This characterization provides the basic building
block for the construction of a knowledge base (e.g., using Semantic Web
practices tailored for the education domain~\cite{Dietze2013}).
As for the collection of user requirements, the NETT-RS collects them during the
rule and keywords selection phases. The NETT-RS does not provide any kind of
repairment for inconsistent requirements (i.e., rules and keywords), in contrast
with most state-of-the-art constraint-based
RSs~\cite{FelfernigK05,Felfernig2008,FelfernigFISTJ09}. However, we notice that
the interaction that the NETT-RS requires to the teachers is different:
rules and keywords are not directly specified. Instead, teachers specify the
requirements by choosing from a suggested set of available rules and keywords,
ensuring the specification of consistent requirements only. Finally, the NETT-RS
currently does not provide any explanation of recommendation results. However,
as pointed out by our experiments in Section~\ref{sec:3}, the system would
benefit from the application of such explanation techniques~\cite{FriedrichZ11}.

In KB RSs literature, special attention has been devoted to requirements
collection, being it a mandatory prerequisite for recommendations to be
made~\cite{rs-handbook}. Requirements can be collected using different
strategies, each one leading to different interaction mechanisms with the user.
Such mechanisms can be relatively simple as \emph{static fill-out} forms filled
each time a user accesses the RS, but also more sophisticated
like the \emph{interactive conversational dialogs}, where the user specifies and
refines the requirements incrementally by interacting with the
system~\cite{PuFCZV11,ChenP12}. The backtracking feature added to the NETT-RS
goes exactly towards this direction.

\section{\uppercase{Conclusion and Future Work}}\label{sec:5}

We conducted a user-centric evaluation of the constraint-based NETT-RS, a RS
that recommends resources to teachers who want to design a course. Our goal was
to study the effect on the overall perceived recommendation quality of a
backtracking feature, that is to give the possibility to teachers to revise the
constraints (i.e., rules and keywords) over the resources specified within the
recommendation process. Our study reveals a strong correlation between the
presence of the backtracking feature and an higher perceived quality.

We foresee at least two main future lines of work. From the experimental point
of view, we would like to run the experiment on learning resources from
different domains and include more participants. From the point of view of the
NETT-RS itself, we plan to take advantage of the insights that we got from this
user study and include in the system also the explanation of the recommendation
results inspired by related work in this
area~\cite{FelfernigK05,Felfernig2008,FelfernigFISTJ09}.

\vfill
\bibliographystyle{apalike}
\small
\bibliography{biblio}
\vfill
\end{document}
