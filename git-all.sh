#!/bin/bash
set -e

relative_path=`dirname $0`
here=`cd $relative_path;pwd`

find $here -wholename '*.git' -exec dirname {} \; | sort | while read dir; do 
	cd $dir 
	echo $dir
	git $@
done

