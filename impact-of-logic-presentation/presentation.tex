\documentclass[xcolor={dvipsnames,table}]{beamer}
\usetheme{Unimib}
\usepackage{wrapfig}
\usepackage{floatflt}
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{array}
\usepackage{setspace}
\usepackage[percent]{overpic}
\usepackage{tabularx,booktabs}
\usepackage{multirow}

\author{Riccardo Porrini}
\institute{\textbf{DISCo, University of Milano-Bicocca}\\
		   \hfill \texttt{riccardo.porrini@disco.unimib.it}\\
		   \vfill \hfill \tiny{Assigment for the Course: The Impact of Logic: from Proof Systems to Databases}}
\title{ABSTAT\emph{Inf}: Inference of Knowledge Patterns for Linked Data Sets Summarization}
\date{}

%\setbeameroption{show only notes}
\begin{document}

\begin{frame}[plain]
	\titlepage
\end{frame}

\addtocounter{framenumber}{-1}

\begin{frame}{Linked Data Set Understanding}
\begin{center}
	\includegraphics[width=1\textwidth]{images/dataset-understanding.pdf}
\end{center}
\end{frame}

\begin{frame}{Linked Data Summarization}
\onslide<1-2>{
\Blue{ABSTAT} Linked Data set summarization framework \cite{ESWC}
\begin{itemize}
  \scriptsize
  \item compact and concise representation of a data set (i.e. \blue{summary})
  \item formal modeling of \blue{minimal type patterns} $(C, P, D)$ extracted from RDF data
  \item assertions $<a,P,b>$ where $C$ min. type of $a$ and $D$ min. type of $b$
  \item minimalization based on a \blue{type graph} that represent the ontology (schema)
  \item \Blue{occurrence} statistics computed for minimal type patterns  
\end{itemize}
}
\vfill
\begin{block}{{\small ABSTAT\emph{Inf}}}<2>
  \begin{itemize}
  \scriptsize
  \item implementation the ABSTAT summarization model in \blue{logic programming}
  \item adds \blue{pattern inference} from the minimal type patterns
  \item and the computation of the respective \blue{occurrence} statistics in the data set
  \end{itemize}  
\end{block}
\vfill
\begin{thebibliography}{}
		\smartbib{1}{ESWC}{M. Palmonari, A. Rula, R. Porrini, A. Maurino, B. Spahiu and V. Ferme}{ASBTAT: Linked Data Summaries with ABstraction and STATistics}{ESWC Posters and Demos}{2015}
	\end{thebibliography}
\end{frame}

\begin{frame}{ABSTAT Model}
\only<1>{\blue{Linked Data Set}}
\only<2>{\blue{Relational Assertions}: $P(x,y)$ from the triple $<x,P,y>$}
\only<3>{\blue{Typing Assertions}: $C(x)$ from the triple $<x,\texttt{rdf:type},C>$}
\only<4>{\blue{Type Graph}: $G = (\mathsf{N},\preceq^{G})$, $\preceq^{G}$ defines a partial order over $\mathsf{N}$}
\only<5>{\blue{Abstract Knowledge Patterns and their Occurrence}\\ 
$(C,P,D)$ s.t. $\exists x \exists y (C(x) \wedge D(y) \wedge P(x,y))$ - \Blue{existential} definition\\
$(C,P,D)$ \Blue{occurs} iff \ldots
}
\only<6>{\blue{Subpatterns}: $(C,P,D) \preceq^{G} (C', P, D')$, iff $C \preceq^{G} C'$ and $D \preceq^{G} D'$}
\only<7>{\blue{Minimal Type Pattern Base} \\
$(C,P,D)$ occurs as $P(a,b)$\\
not exist $C'$ s.t. $C'(a)$ and $C'\prec^{G}C$ or a $D'$ s.t $D'(b)$ and $D'\prec^{G}D$\\
\hfill ensures \Blue{compactness}
}
\only<8>{\blue{Linked Data Set Summary}}
\vfill
\begin{center}
	\only<1>{\includegraphics[width=1\textwidth]{images/example-2-complete.pdf}}
	\only<2>{\includegraphics[width=1\textwidth]{images/example-2-relational-assertion.pdf}}
	\only<3>{\includegraphics[width=1\textwidth]{images/example-2-typing-assertion.pdf}}
	\only<4>{\includegraphics[width=1\textwidth]{images/example-2-terminology-graph.pdf}}
	\only<5>{\includegraphics[width=1\textwidth]{images/example-2-pattern.pdf}}
	\only<6>{\includegraphics[width=1\textwidth]{images/example-2-subpattern.pdf}}
	\only<7>{\includegraphics[width=1\textwidth]{images/example-2-minimal-type-pattern-base.pdf}}
	\only<8>{\includegraphics[width=1\textwidth]{images/example-2-summary.pdf}}
\end{center}
\end{frame}

\begin{frame}{ABSTATInf}
\begin{block}{}<1>
  \begin{itemize}
  \scriptsize
  \item implementation the ABSTAT summarization model in \blue{Prolog}
  \vspace{0.5cm}
  \item implements \blue{pattern inference} from the minimal type patterns
  \vspace{0.5cm}
  \item and the computation of the respective \blue{occurrence} statistics in the data set
  \end{itemize}  
\end{block}
\end{frame}

\begin{frame}{Formal Properties}

of the minimal type pattern base
\vfill
\begin{cblock}{Relational Assertions Coverage}
given $(C, P, D)$, all instances of type $C$ and $D$ linked by the property $P$
can be retrieved by applying the existential definition of patterns as a
conjunctive query.
\end{cblock}
\vfill
\begin{cblock}{Completeness w.r.t. Pattern Inference}
the minimal type pattern base includes all and only the patterns from which all other patterns can
be inferred.
\end{cblock}

\end{frame}

\begin{frame}{Pattern Inference and Occurrence Statistics}
\begin{columns}[M]
		\begin{column}{7cm}
			\only<1>{\includegraphics[width=1\textwidth]{images/abstat-inf.pdf}}
			\only<2>{\includegraphics[width=1\textwidth]{images/abstat-inf-patterns.pdf}}
			\only<3>{\includegraphics[width=1\textwidth]{images/abstat-inf-inferred-patterns.pdf}}
			\only<4>{\includegraphics[width=1\textwidth]{images/abstat-inf-occurrences.pdf}}
		\end{column}
		\begin{column}{5cm}
			\only<2>{
			Instances of minimal type patterns are retrieved by applying their existential definition.\\(via the coverage property)
			}\only<3>{
			Instances of inferred patterns are aggregated from minimal type patterns.\\(via the completeness w.r.t. pattern inference property)
			}\only<4>{
			Occurrece statistics are trivially computed
			}
		\end{column}
\end{columns}

\end{frame}

\begin{frame}[Plain]
	\begin{mdframed}[rightmargin=-1.2cm,
    				roundcorner=3pt,
    				align=right,
    				userdefinedwidth=6cm,
    				linecolor=lightgrey,
    				backgroundcolor=lightgrey,
    				innerbottommargin=10pt,
    				innertopmargin=10pt]
		{\Huge \textbf{\blue{Demo}}} \hfill
	\end{mdframed}
\end{frame}

\begin{frame}{Conclusions and Future Work}
	\Blue{Conclusions}
	\begin{itemize}[<+->]
	  \small
	  \item ABSTATInf, a Prolog implementation of the ABSTAT model
	  \item pattern inference with occurrence statistics computation
	  \item built on ABSTAT model's formal properties 
	\end{itemize}
	\vfill
	\Blue{Future work}
	\begin{itemize}[<+->]
	  \small
	  \item Consider also subproperty relations
	  \item Experiments with large data sets
	\end{itemize}
\end{frame}

\begin{frame}{Thank You}
	\begin{mdframed}[rightmargin=-1.2cm,
    				roundcorner=3pt,
    				align=right,
    				userdefinedwidth=6cm,
    				linecolor=lightgrey,
    				backgroundcolor=lightgrey,
    				innerbottommargin=10pt,
    				innertopmargin=10pt]
		{\Huge \textbf{\blue{Questions?}}} \hfill
	\end{mdframed}
	\vfill
	\hfill
	\url{riccardo.porrini@disco.unimib.it}\\
	\hfill \url{http://rporrini.info}\\
\end{frame}

\end{document}