\documentclass{llncs}
\usepackage[a4paper,plainpages=true,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tabularx,booktabs}
\usepackage{multirow}
\usepackage{framed}

\begin{document}

\title{\textbf{ABSTAT\emph{Inf}: Inference of Knowledge Patterns for Linked Data
Sets Summarization}}
\author{Riccardo Porrini}
\institute{
	\vspace{-0.25cm}
	DISCo, University of Milano-Bicocca - \email{riccardo.porrini@disco.unimib.it}
}

\maketitle

\vspace{-0.5cm}

\begin{abstract}
This report presents ABSTAT\emph{Inf}: a logic programming prototype, which
infers data patterns and corresponding occurrence statistics in Linked Data
sets. ABSTAT\emph{Inf} is built on top of ABSTAT, a schema (i.e., ontology)
driven, minimization based linked data summarization framework. Experiments
conducted with a large real world data set show the feasibility of the
proposed pattern inference approach.
\end{abstract}

\section{Introduction}\label{sec:introduction}
\vspace{-0.25cm}
Understanding the content of large and complex data sets in the Linked Open Data
Cloud\footnote{\url{http://linkeddatacatalog.dws.informatik.uni-mannheim.de/state/}}
is challenging for data consumers~\cite{Palmonari2015,Loupe2015,PalmonariESWC}.
Linked Data summarization approaches aim at mitigating this problem by providing
a compact but complete representation of a Linked Data set (i.e.,
\emph{summary}). Among those approaches, the ABSTAT
framework~\cite{Palmonari2015,PalmonariESWC} implements an ontology-driven,
minimization based summarization model.
ABSTAT\footnote{\url{http://abstat.disco.unimib.it}} focuses on data sets that
are modeled using the RDF data model, and whose schema is provided by an
ontology. The principal components of a summary are Abstract Knowledge Patterns
(\emph{patterns}) that represent the usage of the ontology in the data similarly
to Ontology Design Patterns~\cite{staab2010handbook}, which have been used also
in related work~\cite{Loupe2015}.

The core feature of ABSTAT is a minimalization mechanism, based on
\textit{minimal type patterns}, to extract more compact summaries. Compactness
is crucial for a summary in order to significantly reduce the amount of
information given to users. A minimal type pattern is a triple $(C, P, D)$ that
represents the occurrences of assertions $<$\textit{a,P,b}$>$ in RDF data, such
that $C$ is a minimal type of the subject $a$ and $D$ is a minimal type of the
object $b$. Minimalization is based on a \textit{type graph} introduced to
represent the data ontology. By considering patterns that are based on minimal
types, ABSTAT is able to exclude several redundant patterns from the summary.
Summaries computed by ABSTAT have some important formal properties stating, for
example, that every pattern present the data can be inferred from the set
minimal type patterns.

A summary includes a set of statistics about the occurrence of patterns (i.e.,
how many times a pattern can be found in the data set). Occurrence statistics
are computed for minimal type patterns only, as they are the only ones that are
included in the summary. As a result, those statistics are not computed for
inferred patterns. This report presents ABSTAT\emph{Inf}, a logic programming
prototype that supports the inference of patterns along with the corresponding
occurrence statistics. ABSTAT\emph{Inf} extends the ABSTAT summarization model
and has been successfully experimented on real a world Linked Data set. The
report is organized as follows. Section~\ref{sec:model} introduces the ABSTAT
summarization model and its formal properties, while ABSTAT\emph{Inf} is
described in section~\ref{sec:inference}. Experiments are described in
section~\ref{sec:experiment}, and conclusions end the report.

\section{Linked Data Summarization}\label{sec:model}
\vspace{-0.25cm}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.8\textwidth]{img/example-2-complete.pdf}
	\caption{A small a Linked Data set and the corresponding
	patterns.}
	\label{fig:example}
	\vspace{-0.5cm}
\end{figure}

This section introduces the summarization model for RDF Linked Data sets, which
is the backbone of the ABSTAT framework~\cite{Palmonari2015,PalmonariESWC}, from
the definition of a Linked Data set, to the definition of a summary and its
formal properties.

\textbf{Linked Data Set.}
A Linked Data set (data set, from now on) consists of: a
\textit{terminology} defining the vocabulary (or ontology) of an application
domain, and a set of \textit{assertions} describing RDF resources in terms of
this vocabulary. More formally, a data set is a couple
$\Delta=(\mathcal{T},\mathcal{A})$, where $\mathcal{T}$ is a set of
terminological axioms, and $\mathcal{A}$ is a set of assertions. Assertions in
$\mathcal{A}$ are of two kinds: \textit{typing assertions} having the form
$C(a)$, and \textit{relational assertion}s having the form $P(a,b)$, where $a$
is a named individual and $b$ is either a named individual or a literal.

\textbf{Assertions.}
\textit{Typing assertions} occur in a data set as triples $<x,$
$\mathtt{rdf\mbox{:}type},$$C>$ where $x$ and $C$ are URIs (which, in the RDF
data model, univocally identify resources), or can be derived from triples
$<x,P,y\text{\^{}\^{}}C>$ where $y$ is a literal (in this case $y$ is a typed
literal, with $C$ being its datatype). $x$ is said to be an instance of a type
$C$ denoted by $C(x)$ if either $x$ is a named individual or $x$ is a typed
literal. A \textit{relational assertion} $P(x,y)$ is any triple $<x,P,y>$ such
that $P\neq Q*$, where $Q*$ is either \texttt{rdf:type}, or one of the
properties used to model a terminology (e.g. \texttt{rdfs:subClassOf}).

\textbf{Abstract Knowledge Patterns and Occurrences.}
\textit{Abstract Knowledge Patterns} (patterns) have a specific existential
interpretation defined by one or more formulas in a logical language, and are
called existential AKPs~\cite{Palmonari2015}. A pattern is a triple $(C,P,D)$
such that $C$ and $D$ are types and $P$ is a property, and it is interpreted in
FOL with the existential statement $\exists x \exists y (C(x) \wedge D(y) \wedge
P(x,y))$. A pattern states that there are instances of type $C$
that are linked to instances of a type $D$ by a property $P$. A pattern
$(C,P,D)$ \textit{occurs} in a set of assertions $\mathcal{A}$ iff there exist
some instances $x$ and $y$ such that $\{C(x),D(y),P(x,y)\}\subseteq
\mathcal{A}$.

\textbf{Type Graph.}
A \textit{type graph} is defined as $G = (\mathsf{N},\preceq^{G})$, where
$\mathsf{N}$ is a set of type names (either concept or datatype names) and
$\preceq^{G}$ is a subtype relation over $\mathsf{N}$. $\preceq^{G}$ is
reflexive, antisymmetric and transitive, and defines a partial order over
$\mathsf{N}$, which is therefore a poset. One type can be subtype of none, one
or more than one type. The type graph is extracted from typing assertions.

\textbf{Subpatterns and Inference.} A pattern $(C,P,D)$ is \textit{subpattern
of} a pattern $(C', P, D')$ wrt $G$, denoted by $(C,P,D) \preceq^{G} (C', P,
D')$, iff $C \preceq^{G} C'$ and $D \preceq^{G} D'$. The subpattern relation
defines pattern inference: a pattern $\rho$ is inferred from a set of patterns
$\Pi$ under a type graph $G$ iff $\pi \preceq^{G} \rho$ for some $\pi \in \Pi$.

\textbf{Minimal Type Pattern Base.}
Observe that, in principle, it is possible to infer a considerable amount of
patterns from an even small linked data set as the one depicted in
Figure~\ref{fig:example}. Observe also that most of the patterns are redudant,
as they can be inferred by leveraging the type graph. A summary that includes
\emph{all} redundant patterns will loose the required compactness and
informativeness. The intuition behind the ABSTAT summarization model is to focus
on patterns that are \emph{minimal} with respect to the type graph. A pattern
$(C,P,D)$ is a \textit{minimal type pattern} for a relational assertion
$P(a,b)\in\mathcal{A}$ and a type graph $G$ iff $(C,P,D)$ occurs in
$\mathcal{A}$ and there does not exist a type $C'$ such that
$C'(a)\in\mathcal{A}$ and $C' \prec^{G} C$ or a type $D'$ such that
$D'(b)\in\mathcal{A}$ and $D' \prec^{G} D$. A \textit{minimal type pattern base}
for a set of assertions $\mathcal{A}$ under a type graph $G$ is a set of
patterns $\Pi^{\mathcal{A},G}$ such that $\pi \in \Pi^{\mathcal{A},G}$ iff $\pi$
is a minimal type pattern for some assertion in $\mathcal{A}$.
$\Pi^{\mathcal{A},G}$ have two important formal properties: (1) it \emph{covers}
all the relational assertions in $\mathcal{A}$, and (2) it is \emph{complete}
with respect to pattern inference\cite{PalmonariESWC}. In other words: (1) for
all patterns $(C, P, D) \in \Pi^{\mathcal{A},G}$ it is possible to formulate a
conjunctive query over $\mathcal{A}$ to retrieve all instances of type $C$ and
$D$ linked by the property $P$, by applying the existential definition of
patterns; (2) $\Pi^{\mathcal{A},G}$ includes all and only the patterns from
which all other patterns can be inferred (see e.g., Figure~\ref{fig:example}).

\textbf{Summary.}
A \textit{summary} of a data set $\Delta=(\mathcal{A},\mathcal{T})$ is a triple
$\Sigma^{\mathcal{A,T}}=(G,\Pi^{\mathcal{A},G},S)$ such that: $G$ is the type
graph, $\Pi^{\mathcal{A},G}$ is a minimal type pattern base for $\mathcal{A}$
under $G$, and $S$ is a set of \textit{statistics} on the elements of $G$ and
$\Pi^{\mathcal{A},G}$. Statistics describe the occurrences of types and
patterns and show how many instances have $C$ as minimal type, and how many
instances having $C$ as minimal type are linked to instances having $D$ as
minimal type by a property $P$.
\vspace{-0.25cm}

\section{Abstract Knowledge Patterns Inference}\label{sec:inference}
\vspace{-0.25cm}
This report presents a logic programming approach that supports the computation
of occurrence statistics of inferred patterns. We implemented ABSTAT\emph{Inf}:
a SWI-Prolog program, which allows to compute the occurrence of an pattern
$\phi$ inferred from $\Pi^{\mathcal{A},G}$, given a data set $\Delta$. ABSTAT\emph{Inf}
implements the ABSTAT summarization model and adds pattern inference with the
computation of the corresponding occurrences on top of it. The intuition behind
ABSTAT\emph{Inf} is to leverage the formal properties of the ABSTAT
summarization model that ensure that (1) \emph{all} the patterns in the data set
can be inferred by leveraging the type graph (completeness of pattern
inference), and (2) that all the occurrences of inferred patterns can be
retrieved and thus counted (coverage).

% From a more practical point of view, given $\Delta$, we model assertions in
% $\mathcal{A}$ and the type graph $G$ using FOL predicates. Pattern
% inference rules are modelled as \emph{Horn Clauses}, along with the algorithm to
% compute the occurrence statistics.

% Computing the occurrence statistics for inferred patterns is a non trivial task,
% as illustrated by the following example from Figure~\ref{fig:example}. The
% pattern $(A, R, T)$ can be inferred from minimal type patterns $(E, R, T)$ and
% $(C, R, T)$, provided that $A$ is a supertype (transitively) of $E$ and $C$.
% Both those minimal type patterns have occurrence equal to 1, since they are
% extracted only from the triple $<c, R, ``s">$. The intuitive algorithm to
% compute the occurrence of $(A,R,T)$, is to sum up all the occurrences of its
% minimal type subpatterns. However, this leads to the computation of wrong
% occurrence statistics when at least one named individual is instance of more
% than one type (i.e., the individual $c$ in this example). One will conclude that
% the occurrence of the pattern $(A,R,T)$ is equal to 2, as the occurrence of both
% subpatterns $(E, R, T)$ and $(C, R, T)$ is 1. However, the correct occurrence of
% the pattern $(A,R,T)$ is 1, as the pattern can be extracted only from the triple
% $<c, R, ``s">$.

\textbf{Inputs.} The basic inputs of ABSTAT\emph{Inf} are: (1) assertions
$\mathcal{A}$ of a data set $\Delta$, (2) the type graph $G$, and (3) the
minimal types for all individuals of $\Delta$.

\textbf{Summary Representation.} Relational assertions in $\mathcal{A}$ are
represented using the basic predicate \texttt{rdf(Subject, Predicate, Object)},
provided by the \texttt{semweb} SWI-Prolog library\footnote{\scriptsize
\url{http://www.swi-prolog.org/pldoc/doc_for?object=section('packages/semweb.html')}}.
The type graph $G$ is represented by facts (i.e., graph edges) in the form of
\texttt{rdf(Type, skos:broader, Superconcept)}. Typing assertions are
represented by facts in the form of \texttt{rdf(Individual, rdf:type, Type)}. At
the moment, ABSTAT\emph{Inf} takes in input only typing assertions interpreted
under the minimal type semantics, that are such that \texttt{Type} is the
minimal type of \texttt{Individual}.
% The predicate \texttt{minimalType} models this semantics:
% \vspace{-0.25cm} \begin{framed} \vspace{-0.25cm} \noindent \small
% \texttt{minimalType(I,C) :- rdf(I,rdf:type,C)} \vspace{-0.25cm} \end{framed}
% \vspace{-0.25cm} \noindent
The representation of minimal type patterns follows from their existential
semantics, and captured by the predicate \texttt{mPattern}:
\begin{center}
\vspace{-0.20cm}
\small \texttt{mPattern(C, P, D) :- rdf(S,P,O), minimalType(S,C),
minimalType(O,D)}
\vspace{-0.20cm}
\end{center}

% \vspace{-0.25cm} \begin{framed} \vspace{-0.25cm} \noindent  \vspace{-0.25cm}
% \end{framed} \vspace{-0.25cm} \noindent
\textbf{Pattern Inference.} Patterns are inferred by leveraging the type graph
$G$ and the minimal type pattern base. The basic building block of the inference
algorithm implemented by ABSTAT\emph{Inf} is the predicate
\texttt{descendants(Type, Descendants)}, that is true if \texttt{Descendants}
contains the set of all the subtypes (transitively) of \texttt{Type}. Similarly
to minimal type patterns, inferred patterns are interpreted under an existential
semantics, with the difference that they consider the inferred types instead of
minimal types. A pattern $(C,P,D)$ is inferred if there exist some minimal type
subpattern such that the subject type is a descendant of $C$ and the object type
is a descendant of $D$. Completeness property (from section~\ref{sec:model}),
ensures that we do not miss any subpattern. Inferred patterns are modelled by
the predicate \texttt{iPattern}:
\begin{center}
	\vspace{-0.20cm}
	\small
	\texttt{iPattern(C, P, D) :- descendants(C,CDes), descendants(D,DDes),
	mPattern(SType,P,OType), member(SType,CDes), member(OType,DDes)}
	\vspace{-0.20cm}
\end{center}
% \vspace{-0.25cm}
% \begin{framed}
% 	\vspace{-0.25cm}
% 	\noindent
% 	\small
% 	\texttt{iPattern(C, P, D) :- descendants(C,CDes), descendants(D,DDes),
% 	mPattern(SType,P,OType), member(SType,CDes), member(OType,DDes)}
% 	\vspace{-0.25cm}
% \end{framed}
% \vspace{-0.25cm}
\noindent
where the predicate \texttt{member(E,List)} is true if \texttt{E} $\in$ \texttt{List}.

\textbf{Pattern Occurrence Computation.} Occurrence statistics of inferred
patterns are computed by leveraging the coverage property
(section~\ref{sec:model}). Given a pattern $(C,P,D)$, this property ensures that
we can always get all the pairs of individuals $a$ and $b$ such that $a$ is an
instance of $C$, $b$ is an instance of $D$ and $a$ and $b$ are connected by $P$.
The pair $\{a, b\}$ is called an \emph{instance} of the pattern. Pattern
instances are retrieved, again, by applying the existential definition of
patterns. Given an inferred pattern, all the instances of all its minimal type
subpatterns are retrieved and included in a set (i.e., with no duplicates). The
occurrence of the pattern is, then, simply the cardinality of the resulting set.

% Observe that this approach allows to compute the right occurrence statistics of
% patterns even in situations similar to the one described at the beginning of
% this section.

\section{Implementation and Experiments}\label{sec:experiment}
\vspace{-0.25cm}
ABSTAT\emph{Inf} has been implemented in SWI-Prolog and the source code is
publicily available\footnote{\url{http://github.com/rporrini/abstat-inf}}.
Experiments have been conducted with a real world data set, DBpedia (a general
knowledge data set), with the goal of evaluating the feasibility of the proposed
approach. DBpedia consists of about 1450 properties, 850 types, 29.7M
typing assertions and 40.5M relational assertions from which can be extracted
about 170K minimal type patterns. For the experiments, a smaller part of the
assertions of the data set was selected, including 1 single property
(\texttt{dbo:director}), 850 types, 176228 typing assertions, 96554 relational
assertions from which can be extracted 258 minimal type patterns\footnote{The
data set used for the experiments is available at \url{https://git.io/vzctn}}.
Overall, the data used for the experiment amounts to 275296 RDF triples. The
decision of experimenting ABSTAT\emph{Inf} on a smaller portion of the DBPedia
data set is due to the limited performance imposed by the hardware of the
machine available for the experiments (Intel Core Duo, 4GB Ram running Ubuntu
Linux). We measured the time required to compute the occurrence statistics for
several inferred patterns, described in Table~\ref{table:experiments}. Results
show that ABSTAt\emph{Inf} is capable to efficiently infer patterns and compute
their occurrence.

\begin{table}[t] 
	\centering 
	\caption{Timings required to compute the occurrence statistics of inferred patterns.} 
	\scriptsize
	\begin{tabularx}{\textwidth}{@{}l *2{>{\centering\arraybackslash}X}@{}}
		\toprule
		\textbf{Pattern} & \textbf{Occurrences} & \textbf{Time (sec)}\\
		\midrule
		(\texttt{Film} \texttt{director} \texttt{PrimeMinister}) & 2 & 0.605 \\
		(\texttt{Film} \texttt{director} \texttt{Politician}) & 37 & 0.468 \\
		(\texttt{Film} \texttt{director} \texttt{Person}) & 65352 & 1.988 \\
		(\texttt{Film} \texttt{director} \texttt{Agent}) & 65428 & 2.568 \\
		\midrule
		(\texttt{Work} \texttt{director} \texttt{PrimeMinister}) & 2 & 0.848 \\
		(\texttt{Work} \texttt{director} \texttt{Politician}) & 45 & 0.959 \\
		(\texttt{Work} \texttt{director} \texttt{Person}) & 81284 & 2.866 \\
		(\texttt{Work} \texttt{director} \texttt{Agent}) & 81402 & 3.697 \\
		\bottomrule
	\end{tabularx}
	\label{table:experiments}
\end{table}

\section{Conclusions and Future Work}\label{sec:conclusions}
\vspace{-0.30cm}
This report presented ABSTAT\emph{Inf}, which builds on the formal properties of
the ABSTAT summarization model to support the inference of knowledge patterns
and their relative occurrence statistics in Linked Data sets. Experiments
conducted with a real world data set show the feasibility of the proposed
approach in term of efficiency, while correctness is granted the ABSTAT model
formal properties. As a future work, we plan to conduct more intensive
experiments, in order to evaluate the performance of ABSTAT\emph{Inf} on larger
data sets.

\bibliographystyle{abbrv}
\bibliography{biblio}
\vspace{-0.5cm}
\end{document}