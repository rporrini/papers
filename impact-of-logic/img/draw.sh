#!/bin/bash

set -e

relative_path=`dirname $0`
current_dir=`cd $relative_path;pwd`

cd $current_dir
libreoffice --headless --convert-to pdf *.odp
for f in `ls *.pdf`; 
do
	pdfcrop $f $f
done
