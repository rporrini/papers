BEGIN { RS = "" ; FS = "\n" }
{
	gsub("dbo:", "" , $1)
	split($1, pattern, " ")
	split($2, logs, " ")
	
	print "(" "\\texttt{" pattern[1] "} " "\\texttt{" pattern[2] "} " "\\texttt{" pattern[3] "}" ") & " $3 " & " logs[7] " \\\\"
}