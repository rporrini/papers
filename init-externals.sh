#!/bin/bash
set -e

function clone {
	bbrepo=$1
	repo=$2
	user=$3
	rm -rf $repo
	sed -i.bak "/$repo/d" .gitignore
	git clone https://rporrini:matr071969@bitbucket.org/$user/$bbrepo $repo
	echo "$repo" >> .gitignore
	echo "[user]" >> $repo/.git/config
	echo "	name = Riccardo Porrini" >> $repo/.git/config
	echo "	email = riccardo.porrini@disco.unimib.it" >> $repo/.git/config
	cd $repo
	git checkout master
	cd ..
	rm -f .gitignore.bak
}

function checkout {
	cvsrepo=$1
	repo=$2
	rm -rf $repo
	sed -i.bak "/$repo/d" .gitignore
	echo "$repo" >> .gitignore
	export CVS_RSH="ssh" 
	export CVSROOT=:ext:rporrini@bert.cs.uic.edu:/home2/fac2/ifc/advis-repository/cvs
	if [ x$SSH_AUTH_SOCK = x ]
	then
		eval `ssh-agent`
		ssh-add ~/.ssh/uic-cs_rsa
	fi
	cvs checkout -d $repo $cvsrepo
	rm -f .gitignore.bak
}

function link {
	target=$1
	name=$2
	rm -rf $name
	ln -s $target $name
	sed -i.bak "/$name/d" .gitignore
	echo "$name" >> .gitignore
	rm -f .gitignore.bak
}

relative_path=`dirname $0`
here=`cd $relative_path;pwd`

cd $here

link ~/Dropbox/paper_trovaprezzi trovaprezzi-log-analysis
clone recsys-course-assignment.git rec-sys rporrini
clone CAiSE2014.git CAiSE2014 rporrini
clone CAiSE2014-Extended.git CAiSE2014-Extended rporrini
clone papers.git/wiki wiki rporrini
clone concept-matching.git concept-matching rporrini
clone demoeswc2015.git ESWC2015 disco_unimib
clone abstat-iswc2015.git ISWC2015 disco_unimib
clone abstat-iswc2016.git ISWC2016 rporrini
clone phd-thesis.git phd-thesis rporrini
checkout Projects/Papers/2015-WWW-Facets WWW2015
echo ". /usr/share/bash-completion/completions/cvs" >> ~/.bashrc
echo "complete -F _cvs scripts/repository.sh" >> ~/.bashrc
. ~/.bashrc

