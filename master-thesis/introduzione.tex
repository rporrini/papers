\chapter{Introduzione}
Nell'ambito dell'e-commerce un negozio elettronico (\textit{e-marketplace})
rappresenta una modalit� di accesso ad un negozio alternativa a quella fisica,
tipicamente via Web. Una delle funzioni degli e-marketplace � quella di
\textit{favorire l'incontro tra venditore e compratori}, fornendo un sistema che
permetta di reperire le offerte in vendita anche attraverso funzionalit� di
ricerca. Rispetto a questa funzione � estremamente importante per un
e-marketplace offrire agli utenti una funzionalit� di ricerca delle offerte in
corso che sia efficace, cio� che permetta facilmente all'utente di reperire le
offerte alle quali � interessato. Questo tipo di funzionalit� di ricerca �
differente dalle funzionalit� offerte dai motori di ricerca classici, sia in
ambito Web che in ambito offline (e.g. ricerca di file all'interno di
file-system). Le principali differenze sono due; in primo luogo l'unit�
informativa considerata non � un documento testuale o una pagina Web come nel
caso dei motori di ricerca classici, bens� un'offerta, la quale tipicamente
possiede solamente un nome (di solito derivato dal nome del prodotto associato
all'offerta), un prezzo ed un insieme di caratteristiche la cui descrizione non
� fornita in linguaggio naturale, ma bens� in forma semi-strutturata (e.g.
caratteristiche tecniche ed appartenenza a categorie merceologiche). La seconda
differenza risiede nell'intento con il quale l'utente effettua la ricerca. Nel
caso dei motori di ricerca classici il bisogno informativo dell'utente riguarda
il contenuto dei documenti o pagine Web, mentre in questo caso l'utente �
interessato a trovare le offerte che possiedono un determinato nome, oppure
possiedono determinate caratteristiche.

Un \textit{Price Comparison Engine} (PCE) � un particolare tipo di
e-market\-place il cui scopo � quello di integrare una grande quantit� di
offerte relative a diverse categorie di oggetti provenienti da \emapl diversi, al fine
di offrire funzionalit� di ricerca e di comparazione delle offerte rispetto al
prezzo o rispetto alle caratteristiche commerciali. Due esempi di PCE sono
TrovaPrezzi\footnote{\url{www.trovaprezzi.it}} e
ShoppyDoo\footnote{\url{www.shoppydoo.com}} i quali condividono la stessa base
informativa, che attualmente � costituita da pi� di 4 milioni di offerte. Al
fine di rendere possibile il reperimento ed il confronto delle offerte, su di
esse sono state definiti due tipi di categorizzazione:
\begin{description}
	\item[Categorie verticali]: categorie merceologiche, organizzate in strutture
	gerarchiche (e.g. ``Audio e Video'' $\rightarrow$ ``Audio'' $\rightarrow$
	``Lettori Mp3 ed Mp4'')
	\item[Categorie trasversali]: categorie arbitrarie, non necessariamente
	organizzate in strutture tassonomiche. Identificano le caratteristiche
	commerciali delle offerte (e.g. Sistema operativo Android, Connettivit� Wi-Fi).
\end{description}Nella base informativa � presente anche una misura di popolarit� associata ad
ogni offerta. Questa misura di popolarit� � determinata dal numero di utenti che
a partire da TrovaPrezzi o ShoppyDoo visitano la pagina dell'offerta
all'interno dell'\emapl di origine.

Il miglioramento dell'efficacia dei sistemi di retrieval nell'ambito degli
e-marketplace � un problema sentito, poich� la facilit� di reperimento delle
offerte � funzionale al loro core business. Tuttavia accade che le societ�
proprietarie degli e-marketplace non possiedano le risorse o le conoscenze
necessarie per implementare sistemi di retrieval avanzati. Questa situazione �
particolarmente frequente per e-marketplace di piccole e medie dimensioni, i
quali molto spesso offrono funzionalit� di retrieval dalla scarsa efficacia. Un modo
comune ed attuale di aumentare l'efficacia di un sistema di retrieval � quello
di integrare al suo interno un \textit{sistema di autocompletamento}.

In generale lo scopo di un sistema di autocompletamento � quello di ridurre il
carico cognitivo necessario all'utente nell'interazione con sistemi che
comportano la scrittura di una qualsiasi forma di testo. In particolare sono
utilizzati per aiutare l'utente nella scrittura di codice sorgente (e.g. l'IDE
Eclipse\footnote{\url{www.eclipse.org}} integra una funzionalit� di
autocompletamento di nomi di variabili, oggetti e metodi), nella scrittura di
comandi operativi (e.g. l'autocompletamento dei nomi di file o comandi nei
sistemi UNIX), ma anche in programmi di office automation utilizzati da utenti
non esperti (e.g. Office\footnote{\url{http://office.microsoft.com}}, in
particolare Excel, ed OpenOffice\footnote{\url{http://www.openoffice.org/}}).
Un altro ambito nel quale sono utilizzati sono i sistemi di Information
Retrieval, nei quali sono utilizzati per aiutare l'utente a formulare query
corrette (e.g. la funzionalit� di autocompletamento di
Amazon\footnote{\url{www.amazon.com}}) oppure per fornire un'anteprima dei
risultati restituiti dalla query che attualmente l'utente sta digitando (e.g.
il portale di giochi Flash Kongregate\footnote{\url{http://www.kongregate.com/}})

Nell'ambito degli e-marketplace di piccole e medie dimensioni, che possiedono
sistemi di retrieval dalla scarsa efficacia non � particolarmente utile
utilizzare un sistema di autocompletamento che aiuti l'utente nella formulazione
delle query, perch� � troppo alto il rischio di suggerire delle query (pur
ragionevoli, ma semplicemente fuori contesto) che potenzialmente non
porterebbero a nessun risultato. Inoltre la numerosit� delle offerte nel listino
dell'e-marketplace � bassa, e quindi � pi� facile suggerire direttamente delle
offerte rilevanti rispetto alle query (anche parziali) formulate dagli utenti.

In questa tesi viene descritto il lavoro effettuato presso il L.Int.Ar
(Laboratorio di Intelligenza Artificiale) e l'azienda 7Pixel
srl\footnote{\url{www.7pixel.it}} (proprietaria dei due PCE TrovaPrezzi e
ShoppyDoo), che ha riguardato la definizione di tecniche di supporto a sistemi
di autocompletamento nell'ambito degli e-marketplace, cio� considerando dati
semi-strutturati, classificati attraverso un sistema di categorie ortogonali, e
con vincoli di computazione molto stringenti (l'interazione con un sistema di
autocompletamento deve essere percepita come istantanea dall'utente). In
particolare sono state definite delle tecniche che permettono ad un sistema di
autocompletamento di suggerire le offerte pi� rilevanti secondo criteri
sintattici (i.e. rispetto al nome) oppure semantici (i.e. rispetto alle
categorie verticali e trasversali) rispetto alla query (anche parziale) che
l'utente ha digitato, considerando anche l'informazione sulla popolarit�. Ad
esempio, se l'utente digita ``nok'' in un box di ricerca il sistema suggerisce
alcune offerte all'interno delle quali compare il termine ``nokia''; oppure se
l'utente digita ``cellulare android'' (i cui termini non compaiono in nessun
nome di un'offerta) il sistema suggerisce alcune offerte appartenenti alla
categoria verticale ``cellulari'' ed appartenenti alla categoria trasversale
``Sistema Operativo Android''. Le tecniche definite in questa tesi sono state
utilizzate per lo sviluppo di un sistema autocompletamento da integrare
all'interno degli e-marketplace le cui offerte vengono aggregate dai PCE
TrovaPrezzi e ShoopyDoo.

La necessit� di coniugare tecniche sintattiche e semantiche nasce dalla
considerazione che esistono due tipi di query, che identificano bisogni
informativi diversi, che vengono sottoposte dagli utenti ai sistemi di retrieval
degli e-marketplace. Queste due tipologie di query sono state individuate
attraverso l'analisi dei log delle query sottoposte dagli utenti ai sistemi di
retrieval di un PCE (ShoppyDoo) e di un e-marketplace di piccole-medie
dimensioni:
\begin{enumerate}
  \item \textbf{Query Mirate}: l'utente intende ricercare un'offerta
  particolare, attraverso una query che tipicamente contiene uno o pi� termini
  del nome dell'offerta (e.g. query:``ipad'', probabili obiettivi: tutte le
  offerte che contengono ``ipad'' nel nome, come ad esempio ``ipad apple
  16GB'');
  \item \textbf{Query Esplorative}: l'utente non ha in mente un'offerta
  precisa, bens� un insieme di offerte che possiedono determinate
  caratteristiche; tipicamente le query esplorative contengono dei nomi di
  categorie merceologiche eventualmente abbinati a nomi di caratteristiche
  (e.g. query: ``cellulare android'', probabili obiettivi: tutti i cellulari
  con sistema operativo android);
\end{enumerate}

Rispetto alle considerazioni appena esplicitate � possibile definire il problema
affrontato in questa tesi nel modo seguente:
\begin{center}
\fboxsep5mm 
\fbox{\vbox{\hsize=10cm\noindent 
    Definire delle tecniche che permettano ad un Sistema di Autocompletamento di
    elaborare query mirate e query esplorative su collezioni di dati
    semi-strutturati annotati secondo due diverse categorizzazioni ortogonali,
    in un tempo limitato.}}
\end{center}
In questa tesi questo problema � stato affrontato come un problema di matching,
il quale deve avvenire in maniera interattiva, tra le query parziali formulatedall'utente e le offerte vendute da un \emapl.

In questa tesi viene presentato \comma (COMposite Match Autocompletion),
algoritmo che permette ad un sistema di autocompletamento di elaborare query
mirate e query esplorative su collezioni di dati semi-strutturati annotati
secondo due diverse categorizzazioni ortogonali, in un tempo limitato.
L'algoritmo di autocompletamento proposto in questa tesi � strutturato in questo
modo: data una query di autocompletamento $q$ ed un insieme di offerte $O$
vengono utilizzate diverse strategie di matching sintattico e semantico per
selezionare un sottoinsieme $R \subset O$ di offerte rilevanti rispetto a $q$.
In questa fase, la componente semantica dell'algoritmo ha lo scopo di allargare
l'insieme delle offerte considerate, in particolare modo quando l'utente sta
effettuando una query di tipo esplorativo. In questo caso, infatti, la query non
potrebbe avere un match sintattico con i nomi delle offerte. Successivamente
viene applicata una funzione che assegna uno score globale agli elementi di $R$ vista
come composizione di score locali i quali forniscono rispettivamente una misura
di rilevanza sintattica, semantica e della popolarit� assoluta delle offerte.
L'algoritmo restituisce le prime $k$ offerte pi� rilevanti.

Lo sviluppo delle tecniche proposte � stato guidato dalle considerazioni che,
data una query di autocompletamento $q$ digitata dall'utente nel box di ricerca
su un e-marketplace:
\begin{enumerate}
  \item ad ogni offerta $o$ � possibile associare una misura di
  \textit{rilevanza sintattica} relativa alla query $q$, che esprime il grado di
  similarit� della query $q$ rispetto al nome dell'offerta;
  \item ad ogni offerta $o$ � possibile associare una misura di
  \textit{rilevanza semantica} relativa alla query $q$, che fornisce una stima
  di quanto i termini appartenenti a $q$ identificano un insieme di
  categorie verticali o trasversali rilevanti rispetto a $o$.
\end{enumerate}

Nell'approccio proposto sono state utilizzate alcune tecniche di matching
sintattico presenti in letteratura, utilizzate per identificare l'insieme delle
offerte sintatticamente rilevanti a partire da query mirate. Le tecniche di
matching semantico vengono invece utilizzate in tutte e due le fasi
dell'algoritmo. Come detto in precedenza durante la prima fase viene allargato
l'insieme delle possibili offerte rilevanti rispetto alle dimensioni epistemiche
identificate dalle categorie verticali ed orizzontali. Invece durante la seconda
fase si utilizzano le relazioni che intercorrono tra categorie verticali ed
orizzontali per discriminare le offerte pi� rilevanti. Il principale contributo
innovativo dell'approccio consiste proprio nello sfruttare questo tipo di
relazioni tra categorizzazioni ortogonali per discriminare le offerte rilevanti
anche nel caso in cui la query sottoposta dall'utente � particolarmente ambigua
(i.e. riconducibile ad un numero molto elevato di categorie verticali e/o
trasversali). In questo caso, si premiano le offerte che presentano combinazioni
particolarmente frequenti di categorie verticali e trasversali identificate
all'interno della query formulata dall'utente. Un altro contributo innovativo
dell'algoritmo proposto in questa tesi risiede nelle caratteristiche della base
informativa sulla quale sono state applicate le tecniche semantiche. In
particolare, a differenza delle tecniche che costituiscono lo stato dell'arte
dell'autocompletamento semantico, la base informativa utilizzata � costituita da
semi-strutturati e non da dati strutturati attraverso ontologie. La tecnica
proposta � innovativa rispetto allo stato dell'arte dell'autocompletamento
semantico anche perch� � stata definita una funzione di ordinamento dei
risultati, la quale � in grado di valorizzare le offerte che possiedono alta
rilevanza, sia sintattica che semantica in modo trasparente rispetto alle
diverse tipologie di query mirate ed esplorative.

% In letteratura il problema dell'autocompletamento � stato affrontato in molti
% contesti, attraverso diversi approcci e tecniche. Uno degli ambiti rilevanti
% rispetto al di questa tesi pu� essere definito come {\it \sba}. In questo
% tipo di approcci il problema dell'autocompletamento � visto come un problema di
% {\it \sm}, ed in particolare di {\it \apma}, tra i termini contenuti
% in una query e stringhe che identificano dei dati di qualche tipo. Queste
% tecniche sono tecniche standard, e sono state utilizzate in questa tesi per
% permettere al sistema di autocompletamento di rispondere alle query mirate,
% considerando anche errori di battitura (e.g. ``noka'' al posto di ``nokia'').
% Tuttavia queste tecniche da sole non permettono ad un sistema di
% autocompletamento di rispondere alle query esplorative. Inoltre non sfruttano
% eventuali annotazioni dei dati.
% 
% Nell'ambito delle problematiche della {\it \sems} sono state sviluppate delle
% tecniche che vanno sotto il nome di {\it \sema}. In questo contesto il
% problema dell'autocompletamento � visto come un problema di matching tra un
% termine di una query digitata dall'utente e dei metadati associati a dati di un
% dataset, oppure categorie attraverso le quali sono organizzati i dati del
% dataset. L'obiettivo di queste tecniche � quello di fornire un supporto ai
% sistemi di \sems. Le tecniche di \sema sono molto attinenti le tecniche proposte
% in questa tesi il cui obiettivo � permettere al sistema di autocompletamento di
% rispondere a query esplorative. Nel contesto di questa tesi infatti non esiste
% una rappresentazione ontologica delle offerte di un e-marketplace, ma le
% categorie verticali e trasversali possono essere utilizzate per identificare un
% insieme di offerte semanticamente rilevanti rispetto alla query digitata
% dall'utente. Le tecniche appartenenti allo stato dell'arte tuttavia non
% considerano il caso in cui la query dell'utente denota un insieme di categorie
% verticali o trasversali. Inoltre le tecniche proposte si basano sempre su una
% base di conoscenza molto strutturata (i.e. Ontologie) e non sono particolarmente
% efficienti (i.e. la computazione avviene spesso con un ritardo percepibile
% dall'utente). La tecnica di autocompletamento semantico proposta in questa tesi
% si basa sullo stesso principio delle tecniche appartenenti allo stato dell'arte
% (i.e. cio� quello di effettuare matching tra query e categorie), ma propone un
% approccio nuovo che permette di identificare insiemi di offerte semanticamente
% rilevanti rispetto ad un insieme di categorie, anche nel caso in cui queste
% identifichino insiemi di offerte disgiunti (e.g. ``cellulari microsoft'') e, a
% differenza delle tecniche proposte nello stato dell'arte, propone inoltre un
% modo per ordinare i risultati dell'autocompletamento semantico.
% 
% Nell'ambito dell'Information Retrieval sono state definite delle tecniche che
% possono essere classificate con il nome di {\it \qa}. L'obiettivo di queste
% tecniche � quello di suggerire delle query o termini di query (e non documenti)
% che siano frequenti e/o significativi rispetto ad un corpus di documenti. In
% generale queste tecniche non sono applicabili nel contesto di questa tesi,
% poich� in tutte le tecniche proposte vengono suggerite query. Tuttavia il punto
% di contatto � rappresentato dalla considerazione che viene fatta nei lavori pi�
% recenti, e cio� che una tecnica di query autocompletion che si basi solamente su
% informazioni di frequenza e popolarit� dei termini ricercati sia poco �
% efficace. L'approccio proposto in questa tesi, seppure poco attinente con le
% tecniche di query autocompletion, segue la stessa considerazione.
% 
% Esempi di aggregazione di tecniche di matching sintattico e semantico possono
% essere trovati nell'ambito dei \textit{Recommender Systems}, in particolare
% nell'ambito dei \textit{Content Based Recommender Systems}. L'obiettivo di un
% Content Based Recommender System � quello di suggerire all'utente un insieme di
% oggetti (che possono anche essere documenti) che siano attinenti ai suoi
% interessi. L'insieme degli oggetti attinenti viene ottenuto considerando le
% caratteristiche degli oggetti, e la loro attinenza rispetto ai ``gusti''
% dell'utente definiti da un profilo. Tuttavia l'ambito dei Recommender Systems �
% distante dall'ambito descritto in questa tesi per il concetto di profilo utente
% e perch� il procedimento di suggerimento degli oggetti non � interattivo.

Le tecniche definite sono state implementate ed integrate all'interno di un
sistema di autocompletamento. Successivamente il sistema � stato valutato
attraverso due tipi di sperimentazione: una in condizioni controllate ed una in
un contesto di utilizzo reale. L'obiettivo della prima sperimentazione � stato
quello di valutare l'algoritmo di autocompletamento sia dal punto di vista
dell'efficienza che dal punto di vista dell'efficacia. L'obiettivo della seconda
sperimentazione invece � stato quello di dimostrare che l'integrazione di un
sistema di autocompletamento all'interno di un e-marketplace porta dei benefici
dal punto di vista commerciale.

Per quanto riguarda la sperimentazione in condizioni controllate � stato
utilizzato il prototipo finale del sistema di autocompletamento, nel quale sono
state integrate tutte e due le tecniche di autocompletamento sintattico e
semantico. L'algoritmo sviluppato � stato valutato in termini di efficacia.
Utilizzando tecniche standard appartenti allo stato dell'arte sono state
calcolate diverse curve di precision-recall. In particolare la sperimentazione
ha dimostrato che l'integrazione di tecniche di autocompletamento semantico
aumenta considerevolmente l'efficacia del sistema di autocompletamento rispetto
al solo uso di tecniche sintattiche in presenza di query di tipo esplorativo.

L'algoritmo sviluppato � stato valutato in termini di efficienza. In
particolare sono stati misurati i tempi medi di esecuzione su un numero
rilevante di query rispetto a due dimensioni: la lunghezza in termini di
caratteri della query e la numerosit� delle offerte. La sperimentazione
ha dimostrato che i tempi di esecuzione seguono una crescita lineare rispetto
alla lunghezza della query, mentre non � cos� per quanto riguarda la numerosit�
delle offerte (in particolare la sperimentazione suggerisce che plausibilmente
il costo dell'algoritmo cresca in ragione pi� che lineare rispetto alla
dimensione del listino).

Per quanto riguarda la sperimentazione in un contesto di utilizzo reale, � stato
utilizzato il primo prototipo del sistema di autocompletamento, che integrava
solamente le tecniche sintattiche, e che quindi poteva elaborare solamente query
mirate. Il sistema � stato installato su un e-marketplace di piccole-medie
dimensioni. Utilizzando Google
Analytics\footnote{\url{http://www.google.com/analytics/}} � stata misurata la
percentuale di utenti che ha messo nel carrello un'offerta dopo essere passata
dalla ricerca. Successivamente � stato attivato il sistema di autocompletamento
ed � stata misurata la percentuale di utenti che ha messo nel carrello
un'offerta restituita dalla ricerca oppure suggerita dal sistema di
autocompletamento. Dal confronto tra le due percentuali � emerso che
l'integrazione del sistema di autocompletamento ha portato ad un aumento del
numero degli utenti che mettono nel carrello un'offerta dopo aver utilizzato la
funzionalit� di retrieval.

La tesi � articolata come segue. Nel capitolo \ref{cap:contesto} � presente una
descrizione approfondita del contesto applicativo e delle motivazioni che hanno
portato alla necessit� di definire le tecniche proposte in questa tesi. Nel
capitolo \ref{cap:approcci} viene esplicitato il contesto rispetto allo stato
dell'arte. In particolare verranno descritte le tecniche pi� rilevanti rispetto
al problema affrontato in questa tesi, le similarit� con l'approccio proposto ed
eventualmente motivi per i quali non si possono applicare in questo contesto.
Nel capitolo \ref{cap:tecniche} le tecniche di autocompletamento sintattico e
semantico sono descritte approfonditamente, contestualmente ad una
formalizzazione rigorosa di alcuni aspetti del dominio applicativo, funzionali
alla trattazione. Nel capitolo \ref{cap:sperimentazione} vengono descritte le
due sperimentazione effettuate e discussi i relativi risultati. Successivamente
vengono esplicitate le conclusioni e gli sviluppi futuri. Nell'appendice
\ref{cap:implementazione} vengono descritti l'architettura generale del sistema
implementato e l'architettura di deployment. Infine l'appendice \ref{app:metodo}
descrive in maniera pi� approfondita una parte del setting utilizzato per la
sperimentazione in condizioni controllate.