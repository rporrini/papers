\documentclass[xcolor={dvipsnames,table}]{beamer}
\usepackage{pgfpages}
\usetheme{Unimib}
\usepackage{wrapfig}
\usepackage{floatflt}
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{array}
\usepackage{setspace}
\usepackage{tabularx,booktabs}
\usepackage[percent]{overpic}
\usepackage{multirow}
\usepackage{amsmath}

\author{Riccardo Porrini}
\tutor{Prof. Messina} 
\supervisor{Dott. Palmonari}
\institute{Ph.D. in Computer Science Course - XXVIII Series}
\title{Construction and Maintenance\\ of Domain Specific Knowledge Graphs\\ for
Web Data Integration}
\date{}

%\pgfpagesuselayout{2 on 1}[a4paper,border shrink=5mm]
%\setbeameroption{show notes}

\begin{document}

\begin{frame}[plain,noframenumbering]
	\includegraphics[width=11.99cm]{images/unimib.png}
\end{frame}

\begin{frame}[plain,noframenumbering]
	\titlepage
\end{frame}

\begin{frame}{Outline}
	\begin{enumerate}
	  \item \Blue{Data Intensive Applications and Dataspaces}
	  \item Motivation and Contributions
	  \item Knowledge Graphs, Mappings, Facets
	  \item Facet Extraction
	  \item Facet Interpretation
	  \item Knowledge Graph Profiling
	  \item Conclusion and Future Work
	\end{enumerate}
\end{frame}

\begin{frame}[noframenumbering]{Data Intensive Web Applications}
	\includegraphics[width=11cm]{images/trovaprezzi.png}
\end{frame}

\begin{frame}{Dataspace and its Lifecycle}
	\only<1>{
		\begin{center}
			\includegraphics[width=11cm]{images/kg-bootstrap.pdf}
		\end{center}
	}
	\only<2>{
		{\hfill\Blue{Modeling}}
		\begin{center}
			\includegraphics[width=11cm]{images/kg-bootstrap-model.pdf}
		\end{center}
	}
	\only<3>{
		{\hfill\Blue{Mapping}}
		\begin{center}
			\includegraphics[width=11cm]{images/kg-bootstrap-map.pdf}
		\end{center}
	}
	\only<4>{
		{\hfill\Blue{Materialization}}
		\begin{center}
			\includegraphics[width=11cm]{images/kg-bootstrap-materialize.pdf}
		\end{center}
	}
\end{frame}

\begin{frame}{End-User Oriented Representation}
	\only<1>{
	KG types and relations as backbone for advanced browse features
	\vfill
	}
	\only<2>{
	\begin{block}{Facet}
		Specialized \blue{relations} aimed at model the \blue{salient} characteristics
		of entities from specific \blue{domains}, from an \blue{end-user} perspective
	\end{block}}
	\begin{center}
		\includegraphics[width=10cm]{images/characterization.pdf}
	\end{center}
\end{frame}

\begin{frame}{An eCommerce Dataspace}
	\hfill\Blue{trovaprezzi.it}\\
	\onslide<1-3>{
	\Blue{Dataspace}
	\begin{itemize}
	  \item \blue{$\sim$ 3 K} sources (eMarketplaces)
	  \item \blue{several millions} of mappings
	\end{itemize}}
	\vfill
	\onslide<2-3>{
	\Blue{Knowledge Graph}
	\begin{itemize}
	  \item \blue{$\sim$ 12 M} offers + \blue{$\sim$ 350 K} ``master'' products
	  \item \blue{$\sim$ 350} types (product categories)
	  \item \blue{$\sim$ 10 K} relations (product technical specifications)
	  \item \blue{$\sim$ 100} facets (for front-end presentation)
	\end{itemize}}
	\vfill
	\onslide<3>{
	\Blue{Staff}
	\begin{itemize}
	  \item \blue{12} domain experts
	  \item \blue{9} software engineers (\blue{5} full-time dedicated)
	\end{itemize}}
\end{frame}

\begin{frame}{Outline}
	\begin{enumerate}
	  \item Data Intensive Applications and Dataspaces
	  \item \Blue{Motivation and Contributions}
	  \item Knowledge Graphs, Mappings, Facets
	  \item Facet Extraction
	  \item Facet Interpretation
	  \item Knowledge Graph Profiling
	  \item Conclusion and Future Work
	\end{enumerate}
\end{frame}

\begin{frame}{Evolution of a Dataspace}
	\begin{center}
		\includegraphics[width=9cm]{images/extraction-interpretation.pdf}
	\end{center}
	\begin{center}
	\includegraphics[width=7cm]{images/trends.png}
	\end{center}
\end{frame}

\begin{frame}[noframenumbering]{Data Management Challenges}
	\onslide<1-2>{
	\Blue{KG and mapping maintenance is hard}
	\begin{itemize}
	  \item need for extensive knowledge of disparate domains\\ \hfill \blue{wines}
	  and \blue{clothes}
	  \item need for domain experts supervision\\ \hfill quality issues
	\end{itemize}}
	\vfill
	\onslide<2>{
	\Blue{Crucial for}
	\begin{itemize}
	  \item inclusion of new data sources covering different domains
	  \item domain specific characterization of data\\ \hfill from
	  \blue{wines}
	  \\
	  \hfill to
	  \blue{italian, cabernet wine bottles, from 2011}
	\end{itemize}}
\end{frame}

\begin{frame}{Generalist vs Domain Specific Characterization}
	\begin{center}
			\includegraphics[width=10cm]{images/characterizations.pdf}
		\end{center}
\end{frame}

\begin{frame}{Goal and Contributions}
	\begin{block}{}
	\blue{Enrich} the schema a KG with \blue{domain specific} facets (relations)
	extracted from a vast amount of structured sources, providing interactive
	methods to domain experts
	\end{block}
	\onslide<2-5>{
	\only<1-2>{
		\begin{center}
			\includegraphics[width=10cm]{images/contributions-example.pdf}
		\end{center}
	}
	\only<3>{
		\begin{center}
			\includegraphics[width=10cm]{images/contributions-extraction.pdf}
		\end{center}
	}
	\only<4>{
		\begin{center}
			\includegraphics[width=10cm]{images/contributions-interpretation.pdf}
		\end{center}
	}
	\only<5>{
		\begin{center}
			\includegraphics[width=10cm]{images/contributions-summarization.pdf}
		\end{center}
	}}
\end{frame}

\begin{frame}{Research Challenges}
	  \onslide<1-3>{\Blue{KG enrichment}
	  \begin{itemize}
	    \item extraction of types, relations and facets\\\hfill
	    \smartcite{Po11,Me13,Ko13}\ldots
	  \end{itemize}}
	  \vfill
	  \onslide<2-3>{\Blue{Mapping Discovery}
	  \begin{itemize}
	    \item schema and ontology matching\\\hfill \smartcite{BE11,SH13} \ldots
	    \item web table annotation\\\hfill \smartcite{Li10,Ve11} \ldots
	  \end{itemize}}
	  \vfill
	  \onslide<3>{\Blue{KG profiling}
	  \begin{itemize}
	     \item type and relation summarization \\ \hfill{\tiny to support domain
	     experts in data management tasks}\\
	     \hfill
	     \smartcite{Pr11,Jarrar}
	     \ldots
	  \end{itemize}}
\end{frame}

\begin{frame}{Limitations}
	\begin{itemize}
	  \item<1-2> \Blue{Evolution of the KG schema}
	  \begin{itemize}
	    \item focus on enrichment with types and relations
	    \item limited focus on domain specific relations, upon which are built
	    facets
	    \item domain specificity not considered
	  \end{itemize}
	  \vfill
	  \item<2> \Blue{Insights on the status of a KG}
	  \begin{itemize}
	    \item limited profiling of the usage of types and relations
	    \item especially on large, domain specific KG
	    \item difficult support to domain experts
	  \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Outline}
	\begin{enumerate}
	  \item Data Intensive Applications and Dataspaces
	  \item Motivation and Contributions
	  \item \Blue{Knowledge Graphs, Mappings, Facets}
	  \item Facet Extraction
	  \item Facet Interpretation
	  \item Knowledge Graph Profiling
	  \item Conclusion and Future Work
	\end{enumerate}
\end{frame}

\begin{frame}[noframenumbering]{Knowledge Graph}
	Formalized in First Order Logic
	\begin{columns}[T]
    	\begin{column}{6cm}
    	\vspace{0.5cm}
    	\begin{itemize}
    	  \scriptsize
    	  \item<2-6> \blue{entities} / \blue{literals} \\ \hfill constants:
    	  $iphone6$
    	  \item<3-6> hierarchy of \blue{types} \\ \hfill unary predicates: $SmartPhone$
    	  \item<4-6> \blue{relations} \\ \hfill binary predicates: $brand$
    	  \item<5-6> \blue{assertions} \\ \hfill typing: $SmartPhone(iphone6)$ \\
    	  \hfill relational: $brand(iphone6,apple)$
    	  \item<6> \blue{terminological axioms} \\ 
    	  \hfill $\forall x~\big(SmartPhone(x) \rightarrow
    	  Product(x)\big)$
    	  \\
    	  \hfill $\forall x \forall y~\big(brand(x,y) \rightarrow
    	  SmartPhone(x)\big)$
    	  \\
    	  \hfill $\forall x \forall y~\big(brand(x,y) \rightarrow
    	  Company(y)\big)$
    	\end{itemize}
    	\end{column}
    	\begin{column}{6.5cm}
    		\begin{center}
				\only<1-4>{\includegraphics[width=6.5cm]{images/knowledge-graph.pdf}}
				\only<5>{\includegraphics[width=6.5cm]{images/knowledge-graph-assertions.pdf}}
				\only<6>{\includegraphics[width=6.5cm]{images/knowledge-graph-axioms.pdf}}
			\end{center}
    	\end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Facets}
	\begin{block}{}
		\blue{Mutually exclusive}, and \blue{collectively
    exhaustive} aspect, property, or characteristic of a class or
    \blue{specific subject}~\cite{Ta04}
    \end{block}
	\vfill
	\begin{columns}[T]
    	\begin{column}{6cm}
    	\begin{center}\includegraphics[width=4cm]{images/facet.pdf}\end{center}
    	\begin{itemize}
    	  	\scriptsize
	    	\item particular \blue{relations} specified by terminological axioms \\
	    	\vspace{0.2cm} 
	    	{\tiny
	    	$\forall x~\big(Wine(x) \rightarrow \exists y~origin(x,y)\big)$\\
			$\exists x~\big(Wine(x)~\wedge~origin(x,tuscany)\big)$\\
			\ldots\\
			$\exists x~\big(Wine(x)~\wedge~F(x,lombardy)\big)$
			\\$\forall x~\big(Wine(x) \rightarrow \forall z~\big(origin(x,y)~\wedge~origin(x,z) \rightarrow y=z\big)\big)$
			}
    	\end{itemize}
    	\end{column}
    	\begin{column}{6cm}
    		\begin{center}
				\includegraphics[width=6cm]{images/facets.pdf}
			\end{center}
    	\end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Mappings to Data Sources}
	Modeled as \blue{Horn Clauses}
	\vfill
	\begin{columns}[T]
    	\begin{column}{5cm}
    	\vspace{0.5cm}
    	\begin{itemize}
    	  \item<2-4> \blue{type-to-type} {\tiny(t-to-t)}\\ \hfill {\tiny $SmartPhone(x) \leftarrow Product(x)$}
    	  \item<3-4> \blue{relation-to-relation} {\tiny(r-to-r)}\\ \hfill {\tiny $displaySize(x) \leftarrow display(x)$}
    	  \item<4> \blue{instance-to-instance} {\tiny(i-to-i)}\\ \hfill {\tiny $sameAs(samsungGalaxyA7,galaxyA7)$}
    	\end{itemize}
    	\end{column}
    	\begin{column}{7cm}
    		\begin{center}
				\includegraphics[width=7cm]{images/mappings.pdf}
			\end{center}
    	\end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Outline}
	\begin{enumerate}
	  \item Data Intensive Applications and Dataspaces
	  \item Motivation and Contributions
	  \item Knowledge Graphs, Mappings, Facets
	  \item \Blue{Facet Extraction}
	  \item Facet Interpretation
	  \item Knowledge Graph Profiling
	  \item Conclusion and Future Work
	\end{enumerate}
\end{frame}

\begin{frame}[noframenumbering]{Facet Extraction}
	\begin{block}{Goal}
		Extract domain specific facets for a KG type $C$
	\end{block}
	\vfill
	\only<1>{
	\begin{center}
		\includegraphics[width=7.4cm]{images/guided-searches.png}
	\end{center}
	\vspace{-0.3cm}
	{
	\hfill \tiny deployed in production on trovaprezzi.it since mid. 2014}
	}
	\only<2>{
	\Blue{Observations about source types}
	\begin{itemize}
	  \item source types often come from specialized sources\\\hfill{\scriptsize(e.g., emarketplaces selling only wine bottles)}
	  \item t-to-t mappings typically map specialized to generalist types \\\hfill{\scriptsize $Wines(x) \leftarrow Barolo(x)$}
	\end{itemize}
	}
	\only<3>{
	\Blue{Idea:} \blue{leverage t-to-t mappings present in the dataspace}
	\begin{enumerate}
	  \scriptsize
	  \item form the set $\mathcal{V}$ of facet values by picking all the source types $\tilde{C}$ such that $C(x) \leftarrow \tilde{C}(x)$
	  \item cluster $\mathcal{V}$ into group of \Blue{homogeneous} facet ranges
	  $V_{1}, \ldots, V_{n}$ \hfill DBSCAN
	  \item output facets $F_i = < C,~V_i>$
	  %\item and rules for materialization $F_i(x, v) \leftarrow \tilde{C}(x), \tilde{C} \in \text{\emph{de-normalized}}(v), ~v \in V_i$
	\end{enumerate}
	}
	\vfill
	\begin{thebibliography}{}
		\scriptsize
		\smartbib{Porrini et al. CAiSE 2014}{CAISE}{R. Porrini, M. Palmonari and C. Batini}{Extracting Facets from Lost Fine-Grained Classifications in Dataspaces}{CAiSE}{2014}
	\end{thebibliography}
\end{frame}

\begin{frame}{Source Type Mutual Exclusivity Principle}
	\begin{block}{}
		The more two source types are mutually exclusive, the more they should be
		clustered together into the same facet range
	\end{block}
	\begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[width=\textwidth]{images/taxonomy-layer-distance.pdf}};
     \begin{scope}[x={(image.south east)},y={(image.north west)}]
     	\draw[red,ultra thick,rounded corners] (0.005,0.34) rectangle (0.49,0.54);
     \end{scope}
	\end{tikzpicture}
	\begin{block}{}
		Given two source types $\tilde{C_1}$ and $\tilde{C_2}$, their occurrence as
		siblings indicates that $\tilde{C_1}$ and $\tilde{C_2}$ are mutually exclusive
	\end{block}
\end{frame}

\begin{frame}{Taxonomy Layer Distance}
	\begin{block}{}
	{\normalsize $\text{TLD}(\tilde{C_1}, \tilde{C_2}) = 1 - \cfrac{|L_{\tilde{C_1}} \cap L_{\tilde{C_2}}|}{|L_{\tilde{C_1}} \cup L_{\tilde{C_2}}|}$}\\
	{\tiny \vspace{0.2cm} Jaccard distance between the two sets of taxonomy layers where two types
	$\tilde{C_1}$ and $\tilde{C_2}$ occur}
	\end{block}
	\vfill
	\begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[width=\textwidth]{images/taxonomy-layer-distance.pdf}};
     \begin{scope}[x={(image.south east)},y={(image.north west)}]
     	\draw[red,ultra thick,rounded corners] (0.005,0.34) rectangle (0.49,0.54);
     	\draw[red,ultra thick,rounded corners] (0.5,0.36) rectangle (1,0.56);
     	\draw[red,ultra thick,rounded corners] (0.17,0) rectangle (0.57,0.27);
 		\draw[green,ultra thick,rounded corners] (0.01,0.355) rectangle (0.485,0.52);
		\draw[green,ultra thick,rounded corners] (0.506,0.378) rectangle (0.995,0.54);
     \end{scope}
	\end{tikzpicture}
	\vfill
	{\color{red} \emph{cabernet}}
	{\color{green} \emph{chianti}}
	$\text{TLD}(cabernet, chianti) = 1 - \cfrac{|L_{cabernet} \cap L_{chianti}|}{|L_{cabernet} \cup L_{chianti}|} = 1 - \cfrac{2}{3} = \cfrac{1}{3}$
\end{frame}

\begin{frame}{Evaluation}
	\begin{table} 
		\centering
		\scriptsize
		\begin{tabularx}{\textwidth}{@{}l *2{>{\centering\arraybackslash}X}@{}}
		\toprule
		& \textbf{\#Taxonomies} & \textbf{\#Mappings}\\ \midrule
		Wines & 184 & 8967 \\
		Musical Instruments & 128 & 1306 \\
		Grappe, Liquors, Aperitives & 115 & 1254 \\
		Beers & 58 & 156 \\
		DVD Movies & 164 & 2042 \\
		Blu-Ray Movies & 55 & 395 \\
		Dogs and Cats Food & 80 & 5592 \\
		Rings & 138 & 936 \\
		Ski and Snowboards & 55 & 790 \\
		Necklaces & 148 & 1156 \\ \midrule
		\textbf{Overall} & \textbf{688} & \textbf{22594} \\ \bottomrule
		\end{tabularx}
	\end{table}
	Evaluation using real-world data from the Italian CSE trovaprezzi.it\\
	\begin{itemize}
	  \small
	  \item facet values manually grouped by domain experts
	  \item comparison with state of the art distances between concepts
	  \\ \hfill \smartcite{LC98}
	  \\ \hfill \smartcite{Wu94}
	\end{itemize}
\end{frame}

\begin{frame}{Quantitative Evaluation}
	\only<1-3>{
	\begin{table}
		\centering
		\scriptsize
		\begin{tabularx}{\textwidth}{@{}l *8{>{\centering\arraybackslash}X}@{}}
		\toprule
		& \multicolumn{3}{c}{\emph{Value Effectiveness}} & \multicolumn{4}{c}{\emph{Clustering Effectiveness}} & \emph{Quality} \\\midrule
	 	& $P$ & $R$ & $F_1$ & $F^*$ & $NMI^*$ & Purity & $E^*$ & $PRF^*$\\\midrule
		LC & 0.394 & 0.953 & 0.537 & 0.666 & 0.709 & 0.220 & 0.685 & 0.531 \\
		WP & 0.377 & \textbf{0.984} & 0.525 & 0.682 & 0.714 & 0.210 & 0.744 & 0.520 \\
		TLD & \textbf{0.416} & 0.901 & \textbf{0.541} & \textbf{0.719} & \textbf{0.746} & \textbf{0.286} & \textbf{0.416} & \textbf{0.558} \\
		\bottomrule
		\end{tabularx}
	\end{table}
	\begin{itemize}[<+->]
	  \pause
	  \item TLD more effective in finding relevant facet values and discarding noisy ones (high $F_1$)
	  \item TLD more effective in clustering homogeneous values (high \emph{clustering effectiveness})
	\end{itemize}
	}
\end{frame}

\begin{frame}{Outline}
	\begin{enumerate}
	  \item Data Intensive Applications and Dataspaces
	  \item Motivation and Contributions
	  \item Knowledge Graphs, Mappings, Facets
	  \item Facet Extraction
	  \item \Blue{Facet Interpretation}
	  \item Knowledge Graph Profiling
	  \item Conclusion and Future Work
	\end{enumerate}
\end{frame}

\begin{frame}[noframenumbering]{Facet Interpretation}
	\begin{block}{Goal}
	Provide a domain specific interpretation of a facet by annotating it with a
	relation $P$ of a KG such as the semantics of $P$ captures the semantics of the
	facet
	\end{block}
	\vfill
	\begin{figure}
	\centering
		\includegraphics[width=7cm]{images/problem-statement.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Challenges}
\begin{itemize}
	\item more than one target relation suitable for mapping
	\\{ \scriptsize \hfill e.g., \blue{$\sim$50000} relations from the DBPedia KG
	\vspace{0.25cm}}
	\item how to resolve ambiguities? \\{\scriptsize \hfill $\{2010, 2011, 2012, \ldots\}$ \\\hfill $releaseYear$ for music albums \\\hfill $vintage$ for wines}
	\vspace{0.20cm}
	\item how to capture semantic similarity?
\end{itemize}
\end{frame}

\begin{frame}{Approach Overview}
	\begin{figure}
	\centering
		\includegraphics[width=11cm]{images/approach-books-refined.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Semantic Similarity - Basic Principles}
	\onslide<1-2>{
		\vfill
		\Blue{Weighted frequency}
		\includegraphics[width=11cm]{images/weighted-frequency.pdf}
	}
	\onslide<2>{
		\Blue{Coverage}
		\includegraphics[width=11cm]{images/value-coverage.pdf}
	}
\end{frame} 

\begin{frame}{Semantic Similarity - Specificity}
	\only<1>{
		\begin{center}
		\includegraphics[width=10cm]{images/example-interpretation-creator.pdf}
		\end{center}
	}
	\only<2>{
		\begin{center}
		\includegraphics[width=10cm]{images/example-interpretation-author.pdf}
		\end{center}
	}
\end{frame}

\begin{frame}{Evaluation}
	\begin{table}
	\scriptsize
	\begin{tabularx}{\columnwidth}{@{}r *5{>{\centering\arraybackslash}X}@{}}
	\toprule
	& \multicolumn{2}{c}{\emph{source relations}} &
	\multicolumn{3}{c}{\emph{target relations}}\\
	\midrule
	& \textbf{\#} & \textbf{types} & \textbf{\#} & \textbf{relevant} &
	\textbf{fair}\\
	\midrule
	\textbf{dbpedia-numbers} & 8 & 7 & 53195 & $\sim$4 & $\sim$19\\
	\textbf{dbpedia-entities} & 31 & 13 & 53195 & $\sim$7 & $\sim$7\\
	\textbf{dbpedia} & 39 & 13 & 53195 & $\sim$3 & $\sim$9\\ 
	\midrule
	\textbf{yago-full} & 83 & 17 & 89 & 1 & -\\
	\textbf{yago-abstract} & 83 & 10 & 89 & 1 & -\\
	\bottomrule
	\end{tabularx}
	\end{table}
	\begin{itemize}
	  \item DBPedia and YAGO as target KGs
	  \item DBPedia based gold standard created through a questionnaire
	  \item YAGO based state-of-the-art gold standard \\ \hfill \smartcite{Li10, Ve11}
	\end{itemize}
\end{frame}

\begin{frame}{Evaluation Results}
	\begin{itemize}
	  \item comparison with the majority voting scheme and 
	  \item maximum likelihood based approach \smartcite{Ve11}
	\end{itemize}
	\vfill
	\hfill {Mean average precision on DBpedia}
	\begin{table}
		\scriptsize
		\begin{tabularx}{\columnwidth}{@{}l *3{>{\centering\arraybackslash}X}@{}} 
			\toprule
			& \textbf{dbpedia-numbers} & \textbf{dbpedia-entities} & \textbf{dbpedia} \\
			\midrule 
			majority & 0.22 & 0.50 & 0.44 \\
			maximum likelihood & 0.16 & 0.39 & 0.34 \\
			specificity-based & \textbf{0.25} & \textbf{0.55*} & \textbf{0.49*} \\
			\bottomrule
		\end{tabularx}
	\end{table}
	\hfill {Mean reciprocal rank on YAGO}
	\begin{table}
		\scriptsize
		\begin{tabularx}{\columnwidth}{@{}l *2{>{\centering\arraybackslash}X}@{}}
		\toprule
		& \textbf{yago-full} & \textbf{yago-abstract} \\
		\midrule 
		majority & 0.76 & 0.86 \\
		maximum likelihood & 0.81 & 0.85 \\
		specificity-based & \textbf{0.88*} & \textbf{0.90*} \\
		\bottomrule
		\end{tabularx}
	\end{table}
	{\tiny \hfill * $p < 0.05$}\\
\end{frame}

\begin{frame}{Research Prototype: STAN}
	\begin{center}
	\includegraphics[width=10cm]{images/ui-suggestions.png}
	\end{center}
	{\hfill \scriptsize demo @ \url{http://stan.disco.unimib.it}}\\
	{\hfill \scriptsize source code @ \url{https://github.com/brando91/STAN}}\\
	{\hfill \scriptsize credits to Brando Preda (MSc student) for the web app development}\\
\end{frame}

\begin{frame}{Outline}
	\begin{enumerate}
	  \item Data Intensive Applications and Dataspaces
	  \item Motivation and Contributions
	  \item Knowledge Graphs, Mappings, Facets
	  \item Facet Extraction
	  \item Facet Interpretation
	  \item \Blue{Knowledge Graph Profiling}
	  \item Conclusion and Future Work
	\end{enumerate}
\end{frame}

\begin{frame}[noframenumbering]{Knowledge Graph Profiling}
	\begin{block}{Goal}
		Support domain experts in maintenance tasks by profiling type and relation
		usage in a KG
	\end{block}
	\vfill
	\begin{itemize}
	   	\item what types are described in the KG? 
		\item what relations are used to characterize the entities of certain types (domain specificity)? 
		\item how frequent is the use of a given relation for certain types?
	\end{itemize}
	\vfill
	\begin{thebibliography}{}
		\scriptsize
		\smartbib{Palmonari et al. ESWC 2015}{ESWC}{M. Palmonari, A. Rula, R. Porrini, A. Maurino, B. Spahiu and V. Ferme}{\\ ASBTAT: Linked Data Summaries with ABstraction and STATistics}{ESWC Posters and Demos}{2015}
		\vfill
		\smartbib{Spahiu et al. SumPre 2016}{sumpre}{B. Spahiu, R. Porrini, M. Palmonari, A. Rula, A. Maurino}{\\ ABSTAT: Ontology-driven Linked Data Summaries with Pattern Minimalization}{SumPre @ ISWC}{2016 \textbf{Best Paper Award}}
	\end{thebibliography}
\end{frame}

\begin{frame}{Summarization Model}
	Extraction of \blue{patterns} $(C,~P,~D)$\\
	\only<1>{
	\begin{center}
		\includegraphics[width=10cm]{images/abstat-example.pdf}
	\end{center}}
	\only<2-3>{
	\only<2>{
	\begin{center}
		\includegraphics[width=10cm]{images/abstat-example-pattern.pdf}
	\end{center}}
	\only<3>{
	\begin{center}
		\includegraphics[width=10cm]{images/abstat-example-summary.pdf}
	\end{center}}}
	\onslide<3>{
	\Blue{Minimality based summarization}
	\begin{itemize}
	  \item provides \blue{compactness} to the summary, preserving
	  \blue{coverage}
	  \item abstract view of the KG
	  \item minimalization captures the \blue{domain specificity} of relations
	\end{itemize}
	}
\end{frame}

\begin{frame}{Research Prototype: ABSTAT}
	\begin{center}
	\includegraphics[width=11cm]{images/abstat.pdf}
	\end{center}
	{\hfill \scriptsize demo @ \url{http://abstat.disco.unimib.it}}\\
	{\hfill \scriptsize source code @ \url{http://github.com/rporrini/abstat}}
\end{frame}

\begin{frame}{Outline}
	\begin{enumerate}
	  \item Data Intensive Applications and Dataspaces
	  \item Motivation and Contributions
	  \item Knowledge Graphs, Mappings, Facets
	  \item Facet Extraction
	  \item Facet Interpretation
	  \item Knowledge Graph Profiling
	  \item \Blue{Conclusion and Future Work}
	\end{enumerate}
\end{frame}

\begin{frame}[noframenumbering]{Conclusions}
	\begin{block}{General contribution}
	Granular characterization of entities of a KG by enriching its
	schema with \blue{domain specific facets} extracted from structured sources,
	providing interactive methods to domain experts to ensure quality
	\end{block}
	\only<1>{
		\begin{center}
			\includegraphics[width=10cm]{images/contributions-example.pdf}
		\end{center}
	}
	\onslide<2-4>{
	\vspace{0.6cm}
	\blue{Data}-driven approach, captures and models \Blue{domain
	specificity}
	\begin{itemize}
	  \vfill
	  \item<2-4> \blue{extraction} of granular, domain specific facets \\ 
	  {\hfill \scriptsize consider already defined t-to-t mappings towards the KG}
	  \vfill
	  \item<3-4> \blue{interpretation} of extracted facets with KG relations \\
	  {\hfill \scriptsize specificity-based semantic similarity}
	  \vfill
	  \item<4> Knowledge Graph \blue{profiling}\\
	  {\hfill \scriptsize abstract overview of how much a relation is domain specific}
	\end{itemize}
	}
\end{frame}

\begin{frame}{Future Work}
	\blue{Usage}-driven maintenance of the Knowledge Graph
	\begin{itemize}
	  \vfill
	  \item<+-> explicit feedback from domain experts \\
	  {\hfill \scriptsize inspired by work on interactive Ontology Matching}
	  \vfill
	  \item<+-> implicit feedback from end-users \\
	  {\hfill \scriptsize evaluating the ``salience'' of extracted facets from their usage}
	  \vfill
	  \item<+-> user behavior analysis \\
	   {\hfill \scriptsize mining (stagional) regularities from queriyng and access
	   logs}
	\end{itemize}
\end{frame}

\begin{frame}{Publications}
	\begin{thebibliography}{}
		\smartbibinc{1}{8}{R. Porrini, M. Palmonari, I.F. Cruz}{Facet Annotation}{To be submitted}{}
		\smartbib{2}{1}{B. Spahiu, R. Porrini, M. Palmonari, A. Rula, A. Maurino}{ABSTAT: Ontology-driven Linked Data Summaries with Pattern Minimalization}{SumPre @ ISWC}{2016 - \textbf{Best Paper Award}}
		\smartbib{3}{2}{M. Palmonari, A. Rula, R. Porrini,
		A. Maurino, B. Spahiu and V. Ferme}{ASBTAT: Linked Data Summaries with ABstraction and STATistics}{ESWC}{2015}
		\smartbib{4}{3}{R. Porrini, M. Palmonari and C.
		Batini}{Extracting Facets from Lost Fine-Grained Classifications in Dataspaces}{CAiSE}{2014} 
		\smartbib{5}{4}{R. Porrini, M. Palmonari and G. Vizzari}{Composite
		Match Autocompletion (COMMA): a Semantic Result-Oriented Autocompletion Technique for e-Marketplaces}{Web Intelligence and Agent Systems Journal}{2014}
		\smartbib{6}{5}{M. Palmonari, G. Vizzari, R. Porrini, A. Broglia, N.
		Lamberti}{Comma: A Result-Oriented Composite Autocompletion Method for e-Marketplaces}{Web Intelligence}{2012}
		\vspace{0.5cm}
		\smartbib{7}{6}{F. Epifania, R. Porrini}{Evaluation of Requirements Collection Strategies for a Constraint-based Recommender System in a Social e-Learning Platform}{CSEDU}{2016}
		\smartbib{8}{7}{L. Canova, M. Nicolini, R. Porrini}{Shopping Online: The Consumer's Odissey between Mobile and Desk Devices}{Journal of Retailing}{2016 - submitted}
	\end{thebibliography}
\end{frame}

\begin{frame}{Thank You}
	\begin{mdframed}[rightmargin=-1.2cm,
    				roundcorner=3pt,
    				align=right,
    				userdefinedwidth=6cm,
    				linecolor=lightgrey,
    				backgroundcolor=lightgrey,
    				innerbottommargin=10pt,
    				innertopmargin=10pt]
		{\Huge \textbf{\blue{Questions?}}} \hfill
	\end{mdframed}
	\vfill
	\hfill
\end{frame}

\begin{frame}{Related Work - Domain Specific Facet Extraction}
	\begin{itemize}
	  \item \emph{document corpora}\\{\hfill \scriptsize focus on relation hierarchies
	  - specific for unstructured data}\\{\hfill \scriptsize\smartcite{Da08,We13,Me13}}
	  \item search engines' \emph{query logs} and \emph{documents}\\{\hfill \scriptsize user search queries as a primary source of information}\\{\hfill\scriptsize\smartcite{Li09,Pa09,Po11}}
	  \item search engines' \emph{query results}\\{\hfill \scriptsize integrate and rank properties already present in web documents}\\{\hfill\scriptsize\smartcite{Ya10,Do11,Ka12,Ko13}}
	\end{itemize}
	\vfill
	\Blue{Similarity-Relatedness between taxonomic concepts}
	\begin{itemize}
	  \item Leacock and Chodorow similarity~{\scriptsize\smartcite{LC98}}
	  \item Wu and Palmer similarity~{\scriptsize\smartcite{Wu94}}
	  \item \ldots \\ {\hfill\scriptsize not designed for heterogeneous taxonomies}
	\end{itemize}
\end{frame}

\begin{frame}{Related Work - Specificity-based Facet Interpretation}
	\Blue{ontologies}
	\begin{itemize}
	  \item intensional matchers \\ {\hfill \scriptsize focus on schema, neglect instances} \\ \hfill \smartcite{Ch14} \ldots
	  \item extensional matchers \\ {\hfill \scriptsize focus on the instances, neglect the schema} \\ \hfill \smartcite{Zh15} \ldots
	\end{itemize}
	\vfill
	\Blue{tabular data}
	\begin{itemize}
	  \item custom knowlege graphs \\ {\hfill \scriptsize knowledge graphs with special features} \\\hfill \smartcite{Ve11,Wa12} \ldots
	  \item holistic approaches \\ {\hfill \scriptsize annotate the table as a whole} \\\hfill \smartcite{Li10,Mu13,Zh15b} \ldots
	\end{itemize}
\end{frame}

\begin{frame}{Related Work - Knowledge Graph Summarization}
	\begin{itemize}
	  \item identification of relevant \blue{subsets} of the knowledge graph \\
	  {\hfill \scriptsize extraction of partial summaries} \\ 
	  {\hfill \scriptsize \smartcite{Zhang:2007,Troullinou15}}
	  \item extraction of patterns from a knowledge graph \\
	  {\hfill \scriptsize based on \blue{triple patterns} \\ \hfill \smartcite{Loupe}} \\
	  {\hfill \scriptsize used to write ``schema-less'' queries over a knowledge
	  graph \\ \hfill \smartcite{Jarrar}}
	  \item computation of statistics on the usage of types and relations \\
	  {\hfill \scriptsize information theory-based measures \\
	  \hfill\smartcite{KonrathGSS12}} \\
	  {\hfill \scriptsize at large scale \\ \hfill \smartcite{LangeggerW09,auer2012}}
	\end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{References}
	\begin{thebibliography}{}
		\smartbib{Abu Helou et al. 2016}{HelouPJ16}{M. Abu Helou, M. Palmonari, M. Jarrar}{Effectiveness of Automatic Translations for Cross-Lingual Ontology Mapping}{J. Artif. Intell. Res.}{2016}
		\smartbib{Auer et al. 2012}{auer2012}{S. Auer, J. Demter, M. Martin and J. Lehmann}{{LODStats} - {A}n Extensible Framework for High-Performance Dataset Analytics}{EKAW}{2012}
		\smartbib{Bernstein et al. 2011}{BE11}{P. A. Bernstein, J. Madhavan and E. Rahm}{Generic Schema Matching, Ten Years Later}{PVLDB}{2011}
		\smartbib{Cheatham and Hitzler 2014}{Ch14}{M. Cheatham and P. Hitzler}{The properties of property alignment}{ISWC}{2014}
		\smartbib{Cruz et al. 2014}{CruzLPST14}{I. F. Cruz, F. Loprete, M. Palmonari, C. Stroe and A. Taheri}{Pay-As-You-Go Multi-user Feedback Model for Ontology Matching}{EKAW}{2014}
		\smartbib{Dakka and Ipeirotis 2008}{Da08}{W. Dakka and P.G. Ipeirotis}{Automatic extraction of useful facet hierarchies from text databases}{ICDE}{2008}
		\smartbib{Dou et al. 2011}{Do11}{Z. Dou, S. Hu, Y. Luo, R. Song and J.R. Wen}{Finding dimensions for queries}{CIKM}{2011}
		\smartbib{Franklin et al. 2005}{FR05}{M. Franklin, A. Halevy and D. Maier}{From Databases to Dataspaces: A New Abstraction for Information Management}{SIGMOD Record}{2005}
		\smartbib{Jarrar and Dikaiakos 2012}{Jarrar}{M. Jarrar and M. D. Dikaiakos}{A Query Formulation Language for the Data Web}{{IEEE} Trans. Knowl. Data Eng.}{2012}
		\smartbib{Kawano et al. 2012}{Ka12}{Y. Kawano, H. Ohshima and K. Tanaka}{On-the-fly generation of facets as navigation signs for web objects}{DASFAA}{2012}
		\smartbib{Kong and Allan 2013}{Ko13}{W. Kong and J. Allan}{Extracting query facets from search results}{SIGIR}{2013}
		\smartbib{Konrath et al. 2012}{KonrathGSS12}{M. Konrath, T. Gottron, S. Staab and A. Scherp}{{SchemEX} - {E}fficient construction of a data catalogue by stream-based indexing of linked data}{J. Web Sem.}{2012}
		\smartbib{Langegger and W{\"{o}}{\ss} 2009}{LangeggerW09}{A. Langegger and W. W{\"{o}}{\ss}}{{RDFStats} - An Extensible {RDF} Statistics Generator and Library}{DEXA}{2009}
		\smartbib{Leacock and Chodorow 1998}{LC98}{C. Leacock and M. Chodorow}{Combining local context and wordnet similarity for word sense identification}{MIT Press}{1998}
		\smartbib{Li et al. 2009}{Li09}{X. Li, Y.Y. Wang, A. Acero}{Extracting structured information from user queries with semi-supervised conditional random fields}{SIGIR}{2009}
		\smartbib{Limaye et al. 2010}{Li10}{G. Limaye, S. Sarawagy and S. Chakrabarti}{Annotating and Searching Web Tables Using Entities, Types and Relationship}{VLDB}{2010}
		\smartbib{Medelyan et al. 2013}{Me13}{O. Medelyan, S. Manion, J. Broekstra, A. Divoli, A.L. Huang and I.H. Witten}{Constructing a focused taxonomy from a document collection}{ESWC}{2013}
		\smartbib{Mihindukulasooriya et al. 2015}{Loupe}{N. Mihindukulasooriya, M. Poveda Villalon and R. Garcia-Castro, A. Gomez-Perez}{RDF Digest: Efficient Summarization of RDF/S KBs}{ISWC (Posters \& Demo)}{2015}
		\smartbib{Mulwad et al. 2013}{Mu13}{V. Mulwad, T. Finin, and A. Joshi}{Semantic message passing for generating linked data from tables}{ISWC}{2013}
		\smartbib{Pasca and Alfonseca 2009}{Pa09}{M. Pasca and E. Alfonseca}{Web-derived resources for web information retrieval: from conceptual hierarchies to attribute hierarchies}{SIGIR}{2009}
		\smartbib{Pound et al. 2011}{Po11}{J. Pound, S. Paparizos and P. Tsaparas}{Facet discovery for structured web search: a query-log mining approach}{SIGMOD}{2011}
		\smartbib{Presutti et al. 2011}{Pr11}{V. Presutti, L. Aroyo, A. Adamou, B. A. C. Schopman, A. Gangemi and G. Schreiber}{Extracting Core Knowledge from Linked Data}{COLD}{2011}
		\smartbib{Shvaiko and Euzenat 2013}{SH13}{Pavel Shvaiko and J{\'{e}}r{\^{o}}me Euzenat}{Ontology Matching: State of the Art and Future Challenges}{{IEEE} Trans. Knowl. Data Eng.}{2013}	
		\smartbib{Stoica et al. 2007}{St07}{E. Stoica, M.A. Hearst and M. Richardson}{Automating creation of hierarchical faceted metadata structures}{HLT-NAACL}{2007}
		\smartbib{Taylor 2004}{Ta04}{A. G. Taylor}{Wynar’s introduction to cataloging and classification}{Libraries Unlimited}{2004}
		\smartbib{Troullinou et al. 2015}{Troullinou15}{G. Troullinou, H. Kondylakis, E. Daskalaki and D. Plexousakis}{RDF Digest: Efficient Summarization of RDF/S KBs}{ESWC}{2015}
		\smartbib{Venetis et al. 2011}{Ve11}{P. Venetis, A. Halevy, J. Madhavan, M. Pasca, W. Shen, F Wu, G. Miao and C. Wu}{Recovering Semantics of Tables on the Web}{VLDB}{2011}
		\smartbib{Wang et al. 2012}{Wa12}{J. Wang, H. Wang, Z. Wang, and K. Q. Zhu}{Understanding tables on the web}{ER}{2012}
		\smartbib{Wei et al. 2013}{We13}{B. Wei, J. Liu, J. Ma, Q. Zheng, W. Zhang and B. Feng}{Dft-extractor: a system to extract domain-specific faceted taxonomies from wikipedia}{WWW}{2013}
		\smartbib{Wu and Palmer 1994}{Wu94}{Z. Wu and M. Palmer}{Verb semantics and lexical selection}{ACL}{1994}
		\smartbib{Yan et al. 2010}{Ya10}{N. Yan, C. Li, S.B. Roy, R. Ramegowda and G. Das}{Facetedpedia: enabling query-dependent faceted search for wikipedia}{CIKM}{2010}
		\smartbib{Zhang 2015}{Zh15b}{Z. Zhang}{Effective and Efficient Semantic Table Interpretation using TableMiner+}{Sem. Web}{2015}
		\smartbib{Zhang et al. 2007}{Zhang:2007}{X. Zhang, G. Cheng and Y. Qu}{Ontology summarization based on rdf sentence graph}{WWW}{2007}
		\smartbib{Zhang et al. 2015}{Zh15}{Z. Zhang, A. L. Gentile, I. Augenstein, E. Blomqvist, and F. Ciravegna}{An Unsupervised Data-driven Method to Discover Equivalent Relations in Large Linked Datasets}{Sem. Web}{2015}
	\end{thebibliography}
\end{frame}

\end{document}