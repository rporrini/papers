\documentclass{article}
\usepackage{multirow}
\usepackage{longtable}
\usepackage{placeins}
\usepackage{array}
\usepackage{wrapfig}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{amsthm}
\usepackage[fleqn]{amsmath}
\usepackage[margin=2.6cm,bottom=2.2cm,top=2.2cm]{geometry}
\usepackage{framed}
\renewcommand*\rmdefault{ppl}\normalfont\upshape

\begin{document}

\hyphenation{Trova-Prezzi}

\date{}
\title{\vspace{-1.5cm}
	   \includegraphics[height=2cm]{img/logo.jpeg}
	   \begin{minipage}[c]{11cm}
	   	\vspace{-2cm}
	   	\centering
	   	\large
	   	UNIVERSITA' DEGLI STUDI DI MILANO-BICOCCA\\
	   	\textbf{Ph.D. in Computer Science}
	   \end{minipage}
	   \includegraphics[height=2cm]{img/disco.jpg}}
\author{
	\normalsize
	Research Doctorate in Computer Science, Series XXVIII\\
	\Large
	\textbf{Final Doctoral Thesis Submission}\\
	Doctoral Candidate: Riccardo Porrini
}
\maketitle

\vspace{1cm}
\noindent Tutor, Prof. \textbf{Enza Messina}
\vspace{1cm}
\begin{center}
\LARGE\textbf{Quality-driven Information Integration Evolution}
\end{center}

\section{Motivation}\label{sec:1}
The transition towards the \textit{Web of Data} opened many new research
directions~\cite{Bizer-ds11}. One of the main goals in this field is to find
hidden relations that hold between semantically similar information (data),
to represent and to leverage them in order to provide a unique access point to
integrated information~\cite{Halevy-sigmodr05}. Classic approaches to
information integration (i.e. Data Management) are based on the concept of
\emph{matching}, both at information structure level (i.e.
schema) and content level (i.e. instances)~\cite{Lenzerini-pods02}. Classic Data
Management approaches have been proven to be effective when dealing with static
information that is, information that does not change over time. However, Web
information is dynamic: it gets frequently updated, deleted etc.  Thus classic
Data Management approaches need to be adapted to consider information
dynamicity~\cite{Halevy-queue05}.

On the Web, dynamicity characterize not only information but also end users'
information needs in accessing integrated information. One example of users'
information needs dynamicity can be found in the eCommerce domain.
Consider the product category ``Tablets'': the first tablet available on the
market was classified as a particular ``Netbook''. When users searched for a
tablet they looked for them within the category ``Netbooks''. However, as time
went by, the more tablets were sold the more they gained popularity. Thus end
users started searching for and browsing to ``Tablets'' and not ``a particular
Netbook type''. This example shows how dynamic end users' information need on
the Web can be: users search for and browse to the same target, but with
different strategies that change over time.

DataSpace Management Systems (DSMS)~\cite{Halevy-sigmodr05} have been proposed
to address dynamicity issues in Web data integration. DSMS are based on the
concept of \emph{data co-existence}: they gather and integrate information
coming from different sources into a \emph{dataspace}, by constantly improving
and refining the integration over time in a \emph{pay-as-you-go} fashion. DSMS
are characterized by~\cite{ROOMBA}:
\vspace{-0.2cm}
\begin{itemize}[itemsep=0mm]
  	  \item Large amount of heterogeneous information sources, changing over time (at schema and instances level);
  	  \item A set of \emph{mappings} between sources schemas/instances and target schema/instances defined by domain experts or found by (semi) automatic algorithms;
  	  \item End users who search for and browse to integrated information and whose information needs change over time.
\end{itemize}\vspace{-0.2cm}
The core component of DSMS are the mappings between sources and the target schema/instances. 
These mappings affect the presentation of integrated information to end users and thus the effectiveness of the DSMS in fulfilling end users' information needs.
In other words, mapping quality affects the overall quality of a DSMS. Moreover, since both information and information needs are changing over time, 
mappings should \emph{evolve}: they should be constantly maintained and updated so that they can enable a DSMS to best fulfill end users' information needs. 
An example of DSMS from the eCommerce domain are Price Comparison Engines. Price Comparison Engines
integrate a huge amount of product offers from many electronic marketplaces. Offers from different eMarketplaces 
are categorized using specific, proprietary taxonomies. In this context source taxonomies are reconciled
with the Price Comparison Engine target taxonomy. End users browse through target taxonomy to find product offers
that they are looking for. Recalling the previous example on end users' information need dynamicity, the not up to date 
mapping ``Tablets'' $\rightarrow$ ``Netbooks'' can potentially prevent end users from actually finding 
offers that fulfill their information need.

Evolving schema and instance mappings of dynamic sources and adapt them to end
users' dynamic information needs is crucial to support end users in effectively
accessing the integrated information provided by DSMS.
\emph{Explicit user feedback} (i.e. from domain experts
\cite{Belhajjame-infsyst13}) and \emph{implicit user feedback} (i.e. inferred
from end-user' observable behavior \cite{cao-tkde12}) represent a valuable aid
to mapping evolution and can be used as an indirect measure of the overall
quality of DSMS at a given time~\cite{belhajjame-cidr11}. The main goal of this
research is to model and implement a mapping and feedback management system that
supports information integration evolution for DSMS. We aim at modelling and
implementing a joint mapping and feedback management system, where mapping
evolution is driven by an indirect measure of the quality of the overall DSMS
defined in terms of fitness to end users' information needs and conformance to
domain experts knowledge.

\section{Related Work}
The research on DSMS is very active as testified by the large body work
published in conferences and journal from different areas, from Artificial
Intelligence (e.g. AAAI, JAIR) to Data Management (e.g. SIGMOD, ICDE, ESWC,
ISWC). Besides many academical research groups (e.g. ``Information Management''
group - University of
Manchester~\footnote{\url{http://img.cs.manchester.ac.uk/}}, ``Penn Database''
group - University of
Pennsylvania~\footnote{\url{https://dbappserv.cis.upenn.edu/home/}}, ``ADVIS
Lab'' - University of Illinois~\footnote{\url{http://www.cs.uic.edu/Advis}}),
there are also many industry research groups that work on DSMS research (e.g.
``Structured Data'' group -
Google~\footnote{\url{http://research.google.com/pubs/DataManagement.html}},
``DMX'' group -
Microsoft~\footnote{\url{http://research.microsoft.com/en-us/groups/dmx/}}). The
concept of Dataspaces has been formally introduced by Franklin et al. in
2005~\cite{Halevy-sigmodr05}. In their seminal paper authors identified some
research challenges that have to be addressed in order to enable effective
management of Dataspaces. One of the challenges identified by Franklin et al.
is ``Human Attention Reuse'' that is, storing and leveraging feedback from users
actions and behavior in order to enable integration evolution within Dataspaces
in a ``Pay-as-You-Go'' way. The problem of DSMS evolution has been further
detailed into two different, separate sub-problems:
\emph{maintenance} (i.e. react to changes in sources) and \emph{improvement}
(i.e. react to feedback)~\cite{Belhajjame-sc10}.

Ideally, DSMS should be able to cope with changes in sources, both at schema and
instances level as well as changes in target schema and instances. Moreover,
they should require little or no manual effort in responding to these changes. A
DSMS should provide some (semi-)automatic methods to derive and/or update the
mappings as changes happen. In the literature, this task is accomplished by
reusing previously defined mappings~\cite{SEMEX, PAYGO, CIMPLE, COPYCAT,
ROOMBA}. All the proposed approaches use syntactic or structural metrics to
derive new mappings or update the existing ones as changes happen in sources
and/or target. The mappings produced during the maintenance phase are
then refined during the improvement phase. Both implicit feedback~\cite{PAYGO,
ROOMBA, MaskatPE12} and explicit feedback~\cite{Q, COPYCAT, OCTOPUS, CIMPLE,
HedelerBMGALPFE12, HoweMRR08} have been incorporated into DSMS to improve
integration. More precisely, query logs~\cite{MaskatPE12} and query answers
click-through~\cite{PAYGO, ROOMBA} are used as a source of implicit end user
feedback to rank and validate automatically discovered mappings. On the other
side explicit feedback is gathered by asking domain experts to validate the
mappings~\cite{HoweMRR08, CIMPLE, COPYCAT, OCTOPUS} or to validate query
answers~\cite{Q, HedelerBMGALPFE12}.

Despite being crucial to mapping maintenance and
evolution~\cite{belhajjame-cidr11}, user feedback is far from being extensively
applied to DSMS, specially when considering end user implicit feedback: there
are only few approaches that incorporate it within a DSMS. More precisely,
existing approaches use only punctual information, that is query
logs~\cite{MaskatPE12} and query answers click-through~\cite{PAYGO, ROOMBA}, as
implicit feedback source. However, query logs and query answers click-through
capture only a small fraction of end user behavior in consulting integrated
information~\cite{KellyT03}. Moreover, the usage of feedback within DSMS has
focused mainly on integration improvement and less on integration evolution.
However, when considering a Web data integration environment, integration should
evolve in order to fit end users' information need dynamicity. For example, one
mapping that is effective at a given time in terms of fitness to end users'
information needs (e.g. the ``Tablets'' $\rightarrow$ ``Netbooks'' mapping from
example in Section~\ref{sec:1}), may not be effective one month later. Lastly,
there are no approaches from the literature that model both implicit and
explicit user feedback within a DSMS in a principled, combined way and study
how they can be leveraged for mapping evolution. The aim of this research is to
face these previously uncovered challenges in Web data management.

\section{Goals}\label{sec:3}
We aim at modelling and implementing a joint mapping and feedback management
system that supports mapping evolution within a DSMS. Our approach relies on the
following hypothesis:
\begin{center}
	Quality defined in terms of \emph{fitness} to end users' information 
	needs and \emph{conformance} to domain experts knowledge can be used 
	to evolve integration over time.
\end{center}
Based on this hypothesis, the goals of this research are:
\begin{description}[itemsep=0mm]
	\item [Goal 1.] Model the quality of a DSMS considering end user behavior;
	\item [Goal 2.] Model mappings evolution considering continuous quality evaluation.
\end{description}

We plan to address the challenges above by starting from several case studies
from the eCommerce domain provided by the italian company
7Pixel~\footnote{\url{www.7pixel.it}}. 7Pixel is the owner of
TrovaPrezzi~\footnote{\url{www.trovaprezzi.it}} and
Shoppydoo~\footnote{\url{www.shoppydoo.it}}, two italian Price Comparison
Engines. They integrate about 8 million product offers from about 2000 italian
eMarketplaces every day, providing end users with full text search, faceted
search and category browsing features. TrovaPrezzi and Shoopydoo are two examples
of domain specific (i.e. eCommerce) DSMS (eDSMS, for brevity). Product offers
listings coming from many different, heterogeneous sources (i.e.
eMarketplaces) are mapped both at schema and instance level (i.e. product offers and
also product categories) to a target schema and instances repository (i.e.
target products and target categories). Mappings are manually specified by
domain experts. The collaboration with 7Pixel is extremely valuable, because not
only they grant access to TrovaPrezzi and Shoppydoo data (i.e. website traffic
information, mappings and source listings), but they share also domain experts'
expertise and experience. Moreover the collaboration with 7Pixel allows us to
deploy our approach to a real world scenario and to conduct both in vivo and in
vitro experiments to validate our work.

Case studies provided by 7Pixel mainly focus on evolution of the mappings
between source product taxonomies (i.e. heterogeneous taxonomies used by source
eMarketplaces to categorize their product offers) and the eDSMS target taxonomy.
These mappings are used to categorize product offers provided by sources
eMarketplaces using the target taxonomy, so that end users can browse through it
as they look for integrated product offers. The definition of the mappings is
the first step performed by domain experts in order to integrate an eMarketplace
as an information source. We plan to model end user behavior by considering
TrovaPrezzi and Shoppydoo traffic information (i.e. from full text search query
logs to category browsing paths) in order to assess the quality of the eDSMS at
a given time, thus pursuing \textbf{Goal 1}. We plan to use the assessed quality
of the eDSMS to provide domain experts with a semi-automatic tool for mapping
evolution. More precisely, we want to be proactive and suggest to domain experts
how they should evolve both the mappings (e.g. update an existing mapping,
define a new mapping), and target taxonomy (e.g. create, delete, merge
categories) in order to better fulfill end users' information needs. Moreover we
plan to enrich our model for mapping evolution with explicit feedback from
domain experts, thus pursuing \textbf{Goal 2}.

\newpage
\section{Preliminary Results and Ongoing Works}
As a preliminary work we defined and implemented a scalable, lightweight
matching algorithm that can be used for many instance mapping tasks within
DSMS~\cite{PalmonariVBLP12}. More precisely, we proposed and algorithm that can
be used to match source instances described by short strings (e.g. product offer
names from the eCommerce domain) to target instances defined in terms of short,
semi-structured documents, classified using a taxonomy and annotated with
metadata (e.g. product offers features such as ``Operating System:
Android''). The main contribution of our work is a new semi-structured documents
ranking function that considers also documents' attribute values occurrences
within the whole documents collection and leverages them in order to calculate
the similarity between the input string and documents. We implemented, deployed
and evaluated our approach in a real world scenario as part of an Autocompletion
System for eMarketplaces. The extended version of our work will be published on
the Web Intelligence and Agent Systems (WIAS) Journal.
 
\begin{wrapfigure}{r}{0.60\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.60\textwidth]{img/facets.pdf}
  \end{center}
  \vspace{-20pt}
  \caption{Adaptive Mappings and Facets Management.}
  \label{fig:1}
  \vspace{-10pt}
\end{wrapfigure}

Currently we are tackling the problem of leveraging mappings to infer new
knowledge about the underlying data and schemas. The main contribution with
respect to state of art of DSMS is about the evolution of new inferred knowledge
over time considering its fitness to end users' information needs and
conformance to domain experts knowledge. We are considering a case study from
the Price Comparison Engine TrovaPrezzi: Adaptive Mappings and Facets
Management. We recall from Section~\ref{sec:3} that product offers coming from
many heterogeneous source eMarketplaces are integrated into TrovaPrezzi using
manually defined mappings between source product categories and the target
taxonomy. Source taxonomies can be very heterogeneous: they can be more general
(i.e. coming from generalistic eMarketplaces selling offers from many different
categories, such as Amazon), or they can be very specific (i.e.
coming from specialized eMarketplaces, selling offers from a specific category
such as wines). A Price Comparison Engine integrates a huge amount of
heterogeneous eMarketplaces, thus the target taxonomy in this case is very
generalistic. When integrating a specialized eMarketplace, usually domain
experts map source categories to target categories at the same specificity
degree. One example of specialized source taxonomy is depicted in
Figure~\ref{fig:1}.

As a result of the mapping process, fine-gained classifications are
``forgotten'', because they are too specific with respect to target taxonomy.
Lost, fine-grained specific source categories are a useful source from where
extract facets. In the context of classification theory facets are ``a clearly
defined, mutually exclusive, and collectively exhaustive aspect, property, or
characteristic of a class or specific subject''~\cite{taylor2004wynar}.
Extracted facets can be used to annotate product offers in order to provide end
users with a faceted search interface. The main idea is to extract facets from
fine-gained source categories and then to use existing mappings from the closest
source category ancestors to map them. An example of this idea is depicted in
Figure~\ref{fig:1}. The source category ``Red Wines'' is mapped to the target
category ``Wines''. We extract the facet ``Grape: Bordeaux'' from the source
category ``Bordeaux''. Then we use the mapping between ``Red Wines'' and
``Wines'' to infer the mapping between ``Bordeaux'' and the extracted facet.
After that all the source product offers belonging to source category
``Bordeaux'' can be annotated with the extracted facet.

This case study is strictly related to goals identified in Section~\ref{sec:3}.
Extracted facets and inferred mappings should be validated by domain experts and
should fit end users' information needs. In other words, extraction and
evolution of these mappings should be driven also by quality, inferred from
implicit end explicit user feedback. In order to integrate feedback into
mappings and facets management, we implemented the previously described approach
and partially deployed it to a real world scenario that is, the TrovaPrezzi
Price Comparison Engine. By partially, we mean that we ran our approach
considering only on a small subset target categories. Currently we are
collecting end users usage statistics about extracted facets (i.e.
click-through) and in the nearest future we plan to perform a qualitative
analysis on them. The result of this analysis will be the input for the next
stage of our work: evaluate the quality of the inferred mappings by considering
end user behavior.

\small
\bibliographystyle{unsrt}
\bibliography{biblio}
\normalsize

\newpage
\begin{center}\section*{\LARGE{Temporary Model of the Final Thesis}}\end{center}
\setcounter{section}{0}
\vspace{2cm}
\section{Motivation and Background}
\subsection{Web Information Integration}
\subsection{Mapping and Taxonomy Management}
\subsection{Problem Definition}
\section{Related Work}
\subsection{Explicit Feedback}
\subsection{Implicit Feedback}
\subsection{DataSpace Management Systems}
\section{Quality driven Feedback and Mapping Management}
\subsection{Representing Mapping Dynamicity}
\subsection{Mapping Management Actions}
\subsection{Inferring Mapping Quality from Implicit Feedback}
\subsection{Quality Driven Mapping Management}
\section{Evaluation}
\subsection{Case Study 1: Adaptive Mappings and Facets Management}
\subsection{Case Study 2: Adaptive Mappings and Taxonomy Refinement}
\newpage

\begin{center}\section*{\LARGE{Schedule for Next Year Work}}\end{center}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\setcounter{section}{0}

\section*{Research Activities} 
\begin{table}[h!] \centering
	\begin{tabular}{ |L{6.0cm}|C{1.65cm}|C{3.5cm}|C{3.5cm}| }
		\hline
		\textbf{Activity} & \textbf{Estimated Duration (Days)} & \textbf{Input} & \textbf{Output} \\
		\hline 
		\hline
		\multicolumn{4}{|c|}{\textit{Case Study 1: Adaptive Mappings and Facets Management}} \\
		\hline
		\hline
		Qualitative analysis of traffic data & 10 & Extracted facets usage log in a real world scenario & Insight about improvements to the approach \\
		\hline
		Improvement of the approach considering traffic data & 15 &  Extracted facets usage log & Improved approach\\
		\hline
		Experiments & 15 & Deployment on a real world scenario & Validation of the approach \\
		\hline
		Paper writing & 10 & Technique formalization from previous activities, Experimental results & Paper \\
		\hline
		\hline
		\multicolumn{4}{|c|}{\textit{Case Study 2: Adaptive Mappings and Taxonomy Refinement}} \\
		\hline
		\hline
		Modelling of mappings and taxonomy management operations & 15 & State of art review, current mappings from 7Pixel, interviews with domain 7Pixel experts & A model implementation for representing mappings and taxonomy management operations \\
		\hline
		Include temporal information on mapping definition & 5 & State of art review, current mappings from 7Pixel & Enhanced model implementation with temporal information \\
		\hline
		Model user behavior & 20 & State of art review, websites traffic information from 7Pixel & A model implementation to represent user behavior in consulting integrated information categorized using a taxonomy \\
		\hline
		Link mappings and taxonomy management model to user behavior model & 30 & All model implementations from previous activities & Implementation of semi-automatic mapping and taxonomy management operations \\
		\hline
		Experiments & 30 & Deployment of techniques from previous activities in a real world scenario & Experimental results \\
		\hline
		Paper writing & 10 & Technique formalization from previous activities, Experimental results & Paper \\
		\hline
	\end{tabular}
\end{table}

\section*{Conferences on Interest Deadlines\footnote{This is a rough schedule, since deadlines for submission have not been published yet.}}
\begin{description}[itemsep=0cm]
\item[Extended Semantic Web Conference (ESWC) : ] Early December 2013
\item[International Conference on Knowledge Engineering and Knowledge Management (EKAW) : ] Late April 2014
\item[International Semantic Web Conference (ISWC) : ] Early May 2014
\item[International Conference on Information and Knowledge Management (CIKM) : ] Early May 2014
\item[International Conference on Data Engineering (ICDE) : ] Late July 2014
\item[International Conference on Management of Data (SIGMOD) : ] Early September 2014
\item[International World Wide Web Conference (WWW) : ] Early October 2014
\item[International Conference on Extending Database Technology (EDBT) : ] Early October 2014
\item[International Conference on Advanced Information Systems Engineering (CAISE) : ] Late November 2014
\end{description}

\section*{Educational Activities}
\subsection*{PhD Courses}
\begin{table}[h!] 
	\centering
	\begin{tabular}{ |l|c| }
		\hline
		\textbf{Activity} & \textbf{Estimated Duration (Hours)} \\
		\hline \hline
		PhD courses attendance & 72 (3 courses $\times$ 24 hours each) \\
		\hline
		PhD courses final exams (study and/or practical works) & 45 (3 courses $\times$ 15 hours each) \\
		\hline
	\end{tabular}
\end{table}
\subsection*{Visiting Scholarships}
\begin{itemize}[itemsep=0cm]
  \item 3 or 6 months to spend abroad. Most likely institution: ADVIS Lab - University of Illinois~\footnote{\url{http://www.cs.uic.edu/Advis}}.
\end{itemize}

\end{document}