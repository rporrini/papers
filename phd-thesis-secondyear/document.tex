\documentclass{article}
\usepackage{multirow}
\usepackage{longtable}
\usepackage{placeins}
\usepackage{array}
\usepackage{wrapfig}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{amsthm}
\usepackage[fleqn]{amsmath}
\usepackage[margin=2.6cm,bottom=2.2cm,top=2.2cm]{geometry}
\usepackage{framed}
\usepackage{titlesec}
\usepackage{tikz}
\renewcommand*\rmdefault{ppl}\normalfont\upshape

\newcommand{\fancysection}[1]{%
        \begin{flushleft}\tikz[baseline=-0.4ex] \node
        [fill=black,text=white]{\begin{minipage}[c][0.4cm][c]{\textwidth}\textbf{\Large #1}\end{minipage}};\end{flushleft}}%

\begin{document}

\hyphenation{Trova-Prezzi}

\date{}
\title{\vspace{-1.5cm}
	   \includegraphics[height=2cm]{img/logo.jpeg}
	   \begin{minipage}[c]{12cm}
	   	\vspace{-2cm}
	   	\centering
	   	\large
	   	UNIVERSITA' DEGLI STUDI DI MILANO-BICOCCA\\
	   	\textbf{Ph.D. in Computer Science}
	   \end{minipage}
	   \includegraphics[height=2cm]{img/disco.jpg}}
\author{
	\normalsize
	Research Doctorate in Computer Science, Series XXVIII\\
	\Large
	\textbf{Final Doctoral Thesis, Academic Year 2013-2014}\\
	Doctoral Candidate: Riccardo Porrini
}
\maketitle

\vspace{1cm}
\noindent Tutor: Prof. \textbf{Enza Messina} \hfill Supervisor: Dott.
\textbf{Matteo Palmonari}
\vspace{1cm}

\fancysection{Final Doctoral Thesis: work in progress}

\section{Quality-driven Evolution in Information
Integration Systems \\ {\small Research Summary}}\label{sec:summary}

DataSpace Management Systems~\cite{halevy-sigmodr05} (DSMS) are information
integration architectures that deal with large heterogeneous data, which are
partially unstructured, possibly sparse and characterized by high
dimensionality. Differently from traditional data integration architectures,
where data from local sources are consistently integrated in a global view after
their schemas are aligned, pay-as-you-go data integration is needed in DSMS:
data are more and more integrated along time within a \emph{dataspace}, as more
effective data access features are required~\cite{PAYGO}.

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{img/web-information-integration.pdf}
  \end{center}
  \caption{An overview of a DSMS.}
  \label{fig:dataspaces}
\end{figure}

This research focuses on DSMS where information is classified using different
kinds of classifications. These kind of DSMS are very frequent, especially on
the web (e.g., Price Comparison Engines or online libraries).
Figure~\ref{fig:dataspaces} provides an overview of a DSMS with different
categorizations. Generally, those categorizations are based on
\emph{taxonomies}, i.e., hierarchies of product categories such as ``Mobile
Phones'' or ``Wines'', and \emph{facets}, i.e., sets of mutually exclusive
coordinate terms that belong to a same concept (e.g., ``Grape: Barolo, \ldots,
Cabernet'', \ldots, ``Type: Red Wine, \ldots, White
Wine'')~\cite{taylor2004wynar,FacetedSearch}. DSMS with categorizations are
characterized by:
\vspace{-0.2cm}
\begin{enumerate}
\item Presence of \textbf{heterogeneous information} both at classification and
instance level.
\vspace{-0.2cm}
\item A set of \textbf{mappings} between local classifications/instances and
global classifications/instances defined by domain experts or found by (semi) automatic algorithms.
\vspace{-0.2cm}
\item \textbf{End-users} who search for and browse to integrated information.
\end{enumerate}

The \textit{global taxonomy} is used to annotate all the instances in the
dataspace with a coarse-grained categorization, which helps end-users to rapidly
recall the ``family'' of instances they are interested in. From the other side,
\emph{facets} can be used to annotate instances in the dataspace with a
fine-grained categorization, which helps end-user to rapidly recall instances
with specific characteristics (e.g., ``Grape: Barolo'', ``Type: Red Wine''). In
DSMS, the maintenance of effective global classifications is a prerequisite to
provide end-users with effective search and browsing features over the
integrated information. This is true especially for facets as they provide the
finer-grained classification.

Another important scenario refers to classifications exchange and reuse, both at
global and local level. In fact, in these last years the number of websites that
provide machine readable data increased dramatically, mainly due to initiatives
like Schema.org\footnote{\url{http://schema.org}}. Data on the web is published
using models and representations that belong to the Semantic Web community
(e.g., RDF~\cite{rdf}). Thus, for DSMS is crucial to be able to interpret and
integrate these kind of data. Moreover, once the data is integrated within the
dataspace, it is extremely valuable to link and reconcile it with external
sources, in order to enable the reuse and the exchange of dataspace
instances and classifications with external services and/or other DSMS.

However, the most difficult challenge that DSMS must face in maintaining
and exchanging effective classifications is \emph{dynamicity}. In particular,
local classifications and instances change over time (e.g., new products are released
and new categories are created to represent them such as ``Tablets'') as well as
user information needs in accessing the integrated information.
Figure~\ref{fig:challenges} highlights these challenges. For these reasons is
crucial for a DSMS to be able to cope both with changes in the sources and in
the end-users information needs. In particular, there is the need of equipping
DSMS with algorithms and techniques that enable the \emph{adaptation} of
classification systems used to classify dataspace
instances~\cite{Belhajjame-sc10}. The DSMS should be equipped also with
automatic tools and algorithms that enable the \emph{timely} alignment between
local classifications, global classifications and mappings, also with respect to
a set of shared Semantic Web vocabularies that allow information exchange and
reuse with other DSMS and services.

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{img/world-changes.pdf}
  \end{center}
  \caption{Dynamicity related challenges for DSMS.}
  \label{fig:challenges}
\end{figure}

The goal of this research is to address the above mentioned challenges. More
specifically, I aim at enabling the adaptation of the DSMS classifications by an
indirect assessment of the quality of the overall DSMS defined in terms of (1)
coverage of the global classifications with respect to local ones and (2)
fitness to end-users’ information needs. The proposed solutions to the above
mentioned challenges have been and will be evaluated (also with respect to
state-of-the-art ones) using both state-of-the-art datasets and \emph{real
world} case studies. Real world case are referred to the eCommerce domain and
are provided by the Italian company 7Pixel~\footnote{\url{www.7pixel.it}}.
7Pixel is the owner of TrovaPrezzi~\footnote{\url{www.trovaprezzi.it}} and
Shoppydoo~\footnote{\url{www.shoppydoo.it}}, two Italian Price Comparison
Engines. They integrate about 8 million product offers from about 2000 Italian
eMarketplaces every day, providing end users with full text search, faceted
search and category browsing features. TrovaPrezzi and Shoopydoo are two
application front-ends of a domain specific (i.e., eCommerce) DSMS.

\section{Facet Extraction \\ {\small Work Started Last Year and Finalized
this Year}}\label{sec:extraction}

During the past year I tackled the problem of adapting the global faceted
classification of a DSMS considering its coverage with respect to the local
classifications~\cite{porriniPB14}. The work has been published in the
Proceedings of the 26th International Conference on Advanced Information Systems
Engineering (CAiSE). Facet creation and maintenance is an extremely time and
effort consuming task in DSMS, which is often left to manual work of domain
experts. The definition of meaningful facets at large scale requires a deep
understanding of the salient characteristics of dataspace instances (e.g., wines
are characterized by grape, type, provenance, and so on). Moreover, using the
manual approach, is impossible to deal with the dynamicity related challenges
highlighted in Section~\ref{sec:summary}.

I proposed an approach to automatic facet extraction in dataspaces, which is
aimed to support domain experts in creating and maintaining significant facets
associated with global categories. The proposed approach leverages the
information already present in the dataspace, namely (i) taxonomies used to
classify instances in the data sources and (ii) mappings established from source
taxonomies to the global taxonomy of the dataspace, to suggest meaningful facets
for a given global category. Unlike the global taxonomy, which has to cover
instances from very diverse domains, source taxonomies are often specialized in
certain domains (e.g., ``Wines''). Domain experts map specific categories in
source taxonomies (e.g., ``Barolo'') to generic categories in the global
taxonomy (e.g., ``Wines''). The idea behind my approach consists in reusing the
fine-grained categories that occur in several source taxonomies mapped to the
global taxonomy (e.g., ``Barolo'',``Cabernet''), to extract a set of relevant
facets for a given global category (e.g., ``Grape: Barolo, \ldots, Cabernet'',
\ldots, ``Type: Red Wine, \ldots, White Wine'').

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{img/approach-detailed-grey-final.pdf}
  \end{center}
  \caption{Semi-automatic facet extraction.}
  \label{fig:facet-extraction}
\end{figure}

The proposed approach is sketched in Figure~\ref{fig:facet-extraction} and
incorporates an automatic facet extraction algorithm that consists of three steps:
\textit{extraction} of potential facet values (e.g., ``Cabernet'');
\textit{clustering} of facet values into sets of mutually exclusive terms (e.g.,
``Bordeaux'', ``Cabernet'', ``Chianti''); \textit{labeling} of clusters with
meaningful labels (e.g., ``Grape''). The algorithm is based on structural
analysis of source taxonomies and on \textit{Taxonomy Layer Distance}, a novel
metric introduced to evaluate the distance between mutually exclusive categories
in different taxonomies. Experiments conducted to evaluate the approach show
that the proposed approach is able to extract meaningful facets that can then be
refined by domain experts.
\vspace{-0.2cm}
\section{Facet Linking \\ {\small Work in Progress}}\label{sec:linking}

Once published the work on Facet Extraction, I started tackling one of the weak
parts of the facet extraction approach: \emph{facet labelling}. More
specifically, I studied more deeply the problem of attaching a label to an
extracted facet (i.e., attaching the label ``Country of Origin'' to the facet
$\{$ ``Italy'', ``Spain'', ``France'' \ldots$\}$ for the category ``Wines''). I
focused on the problem of reconciling extracted facets to shared ontologies,
that is the \emph{facet linking problem}. The goal of this work is to go beyond
the simple labelling of the facets by \emph{linking} each facet to its semantic
representation provided by an existing ontology. This will enable facet reuse
and exchange, also outside the DSMS from which the facets have been extracted.
The proposed approach to facet linking relies on the following hypothesis,
motivated by previous work in this area
(e.g.,~\cite{limayeVLDB2010,venetisVLDB2011,querciniEDBT2013,mulwadISWC2013,munozWSDM2014}):
\begin{enumerate}
  \item Facets can be linked with \emph{predicates} from shared ontologies.
  In other words, ontology predicates provide a suitable semantics for facets.
  \item Right predicates to be linked to a facet can be inferred by analyzing
  their usage within large knowledge bases (e.g., DBPedia~\cite{dbpedia},
  YAGO~\cite{yago} or Freebase~\cite{freebase}). In other words, approaches from the literature leverage
  \emph{evidence} provided by a knowledge base on the semantics of certain predicates.
\end{enumerate}

So far, I adapted and implemented one of the most promising state-of-the-art
approaches~\cite{venetisVLDB2011} and identified its weaknesses. Currently I am
working on overcoming these weaknesses. Moreover, I also created the
experimental setting needed to evaluate the effectiveness of the proposed
approach, especially in comparison with the other state-of-the-art approach.
\vspace{-0.2cm}
\section{Econometric Analysis on TrovaPrezzi Traffic \\ {\small Side
Activity}}\label{sec:economics}

I am collaborating with a group of researchers in Ecomonics from Universit\'a di
Pavia and Scuola Enrico Mattei (ENI). The goal of this interdisciplinary
collaboration is to study the correlation between end-user dynamic information
needs in information integration systems with focus on the eCommerce domain and
several external factors, such as different economic indicators and weather
conditions. My role in this collaboration is to provide suitable data for the
analysis. I retrieved the access logs for the TrovaPrezzi Price Comparison
Engine, spanning approximately one year (i.e., about 3 billions HTTP requests).
I wrote the code to perform an efficient analysis of the access logs in terms of
geolocalization of the requests and their distribution among different
merceological categories. The resulting dataset is currently being analyzed.
\vspace{-0.2cm}
\section{Future Work}

As future work I plan to:
\begin{enumerate}
  \item Finalize the work on Facet Linking. This will be done while visiting the
  ADVIS Lab~\footnote{\url{http://www.cs.uic.edu/Advis}} at the University of
  Illinois at Chicago, under the supervision of Prof. Isabel Cruz.
  \item Tackle the problem of adaptation of DSMS classifications with respect to
  the other perspective highlighted in Section~\ref{sec:summary}: fitness to
  end-users information needs. In particular I plan to use information extracted
  from the DSMS end-user access log to further refine the techniques for facet
  extraction and linking described in Sections~\ref{sec:extraction}
  and~\ref{sec:linking}. In this direction, the side activity described in
  Section~\ref{sec:economics} is considered to be propedeutical, because
  allowed me to gain technical confidence and practice on how to analyze and
  process in an efficient way the huge amount of information provided by the
  access logs of TrovaPrezzi.
\end{enumerate}

\vspace{0.5cm}
\noindent \textbf{Tutor's signature} \hfill \textbf{Supervisor's signature}
\vspace{0.9cm}

\small
\bibliographystyle{unsrt}
\bibliography{biblio}
\normalsize

\end{document}